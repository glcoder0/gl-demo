//
// Created by walter on 02/02/20.
//

#ifndef MYNATIVEACTIVITYGLES3_ANDROIDGLESMAIN_H
#define MYNATIVEACTIVITYGLES3_ANDROIDGLESMAIN_H

#include <initializer_list>
#include <memory>
#include <cstdlib>
#include <cstring>
#include <jni.h>
#include <errno.h>
#include <cassert>

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <GLES3/gl31.h>

#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>
#include <android/asset_manager.h>

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "native-activity", __VA_ARGS__))
#define ALOGW LOGW
#define ALOGV LOGI

#include <math.h>
#include <string.h>
#define PI M_PI

#include <vector>
#include <array>
extern void ShaderTest_Init();
extern void ShaderTest_Render();
extern void ShaderTest_Resize(int w,int h);
extern void AndroidShaderTest_Init();
extern void AndroidShaderTest_Render();
extern GLuint loadTgaTextureGL(const char* f);
extern GLuint loadTgaTextureGLFromMem(const void* data);

extern AAssetManager *g_assetMgr;

#endif //MYNATIVEACTIVITYGLES3_ANDROIDGLESMAIN_H
