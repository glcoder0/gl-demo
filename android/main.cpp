/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//BEGIN_INCLUDE(all)
#include "androidglesmain.h"
extern "C" GLboolean gl3stubInit();

AAssetManager *g_assetMgr;
/**
 * Our saved state data.
 */
struct saved_state {
    float angle;
    int32_t x;
    int32_t y;
};

void walkAssetDir(){
    AAssetDir *assetDir = AAssetManager_openDir(g_assetMgr, "");
    const char *filename = (const char *) NULL;
    while ((filename = AAssetDir_getNextFileName(assetDir)) != NULL) {
                ALOGV("asset %s\n", filename);
    }
    AAssetDir_close(assetDir);
}
uint8_t* loadAndAllocResourceFile(const char* f,size_t* fileSize){
    if (!g_assetMgr){
                ALOGV("NO_ASSET_MANAGER , Cant load %s",f);
        return nullptr;
    }
    AAsset *asset = AAssetManager_open(g_assetMgr, f, AASSET_MODE_STREAMING);
    if (!asset) {
                ALOGV("could not open %s\n", f);
        return nullptr;
    }

    size_t alen=AAsset_getLength(asset);
    void* buffer=malloc(alen);
    auto totSize=AAsset_read(asset,buffer,alen);
            ALOGV("LOADED asset chunk %z/%z\n", totSize,alen);
    if (fileSize) *fileSize=totSize;
    AAsset_close(asset);
    return (uint8_t*)buffer;
}

#define TX_LOGI ALOGV
#define TX_TRACE(X)




/**
 * Shared state for our app.
 */
struct engine {
    struct android_app* app;

    ASensorManager* sensorManager;
    const ASensor* accelerometerSensor;
    ASensorEventQueue* sensorEventQueue;

    int animating;
    EGLDisplay display;
    EGLSurface surface;
    EGLContext context;
    int32_t width;
    int32_t height;
    struct saved_state state;
};
int g_width,g_height;
void process_virtual_joypad();

/**
 * Initialize an EGL context for the current display.
 */
static int engine_init_display(struct engine* engine) {
    // initialize OpenGL ES and EGL

    /*
     * Here specify the attributes of the desired configuration.
     * Below, we select an EGLConfig with at least 8 bits per color
     * component compatible with on-screen windows
     */
    const EGLint attribs[] = {
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES3_BIT_KHR,
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_BLUE_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_RED_SIZE, 8,
            EGL_NONE
    };
    EGLint w, h, format;
    EGLint numConfigs;
    EGLConfig config;
    EGLSurface surface;
    EGLContext context;

    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

    eglInitialize(display, 0, 0);

    /* Here, the application chooses the configuration it desires.
     * find the best match if possible, otherwise use the very first one
     */
    eglChooseConfig(display, attribs, nullptr,0, &numConfigs);
    std::unique_ptr<EGLConfig[]> supportedConfigs(new EGLConfig[numConfigs]);
    assert(supportedConfigs);
    eglChooseConfig(display, attribs, supportedConfigs.get(), numConfigs, &numConfigs);
    assert(numConfigs);
    auto i = 0;
    for (; i < numConfigs; i++) {
        auto& cfg = supportedConfigs[i];
        EGLint r, g, b, d;
        EGLint version;

        eglGetConfigAttrib(display,cfg,EGL_CONTEXT_CLIENT_VERSION,&version);
        eglGetConfigAttrib(display, cfg, EGL_RED_SIZE, &r);
        eglGetConfigAttrib(display, cfg, EGL_GREEN_SIZE, &g);
        eglGetConfigAttrib(display, cfg, EGL_BLUE_SIZE, &b);
        eglGetConfigAttrib(display, cfg, EGL_DEPTH_SIZE, &d);
        ALOGW("EGL CONFIG %d/%d:  Version %d  rgbd=%d %d %d %d", i, numConfigs, version,
                      r, g, b, d);
        if (r==8 && g==8 && b==8 && d==24) {
            config = supportedConfigs[i];
            ALOGW("choosing [%d]",i);
            break;
        }

    }
    if (i == numConfigs) {
        config = supportedConfigs[0];
    }
    //exit(0);

    /* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
     * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
     * As soon as we picked a EGLConfig, we can safely reconfigure the
     * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */

    EGLint AttribList[] =
            {
                    EGL_CONTEXT_CLIENT_VERSION, 3,
                    EGL_NONE
            };

    eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);
    surface = eglCreateWindowSurface(display, config, engine->app->window, NULL);
    context = eglCreateContext(display, config, NULL, AttribList);

    if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE) {
        LOGW("Unable to eglMakeCurrent");
        return -1;
    }

    eglQuerySurface(display, surface, EGL_WIDTH, &w);
    eglQuerySurface(display, surface, EGL_HEIGHT, &h);


    const char* versionStr = (const char*)glGetString(GL_VERSION);
    ALOGV("!!!OPENGL VERSION = %s\n",versionStr);

    engine->display = display;
    engine->context = context;
    engine->surface = surface;
    engine->width = w;
    engine->height = h;
    engine->state.angle = 0;
    ALOGV("VIEWPORT SIZE = %d %d",w,h);
    glViewport(0, 0, w, h);
    ShaderTest_Resize(w,h);
    g_width=w;
    g_height=h;

    // Check openGL on the system
    auto opengl_info = {GL_VENDOR, GL_RENDERER, GL_VERSION, GL_EXTENSIONS};
    for (auto name : opengl_info) {
        auto info = glGetString(name);
        LOGI("OpenGL Info: %s", info);
    }
    // Initialize GL state.
    //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);
    glEnable(GL_CULL_FACE);
    //glShadeModel(GL_SMOOTH);
    //glDisable(GL_DEPTH_TEST);
    ShaderTest_Init();

    return 0;
}

/**
 * Just the current frame in the display.
 */
int my_frame=0;
static void engine_draw_frame(struct engine* engine) {
    if (engine->display == NULL) {
        // No display.
        return;
    }
    my_frame++;
    // Just fill the screen with a color.
    glClearColor(((float)engine->state.x)/engine->width, (1.0f/31.f)*(float)(my_frame&31),
                 ((float)engine->state.y)/engine->height, 1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    //process_virtual_joypad();

    ShaderTest_Input(nullptr);
    ShaderTest_Render();

    eglSwapBuffers(engine->display, engine->surface);
}

/**
 * Tear down the EGL context currently associated with the display.
 */
static void engine_term_display(struct engine* engine) {
    if (engine->display != EGL_NO_DISPLAY) {
        eglMakeCurrent(engine->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (engine->context != EGL_NO_CONTEXT) {
            eglDestroyContext(engine->display, engine->context);
        }
        if (engine->surface != EGL_NO_SURFACE) {
            eglDestroySurface(engine->display, engine->surface);
        }
        eglTerminate(engine->display);
    }
    engine->animating = 0;
    engine->display = EGL_NO_DISPLAY;
    engine->context = EGL_NO_CONTEXT;
    engine->surface = EGL_NO_SURFACE;
}

/**
 * Process the next input event.
 */
#define MAX_TOUCH 10
struct TouchInput{
    int numTouch;
    int x[MAX_TOUCH];
    int y[MAX_TOUCH];
    int startx[MAX_TOUCH];
    int starty[MAX_TOUCH];
    int endx[MAX_TOUCH];
    int endy[MAX_TOUCH];
    int active[MAX_TOUCH];
    int old_active[MAX_TOUCH];
};

struct VirtualJoypad {
    float dx0,dy0;
    float dx1,dy1;
    int buttons;
};
VirtualJoypad g_Joypad;

template<typename T> inline T myclamp(T v,T lo,T hi){return v<lo?lo:v>hi?hi:v;}
TouchInput g_Touch;
static int32_t engine_handle_input(struct android_app* app, AInputEvent* event) {
    struct engine* engine = (struct engine*)app->userData;
    auto action = AMotionEvent_getAction(event);
    ALOGV("inp %d %d %d %d",AInputEvent_getType(event),AMotionEvent_getAction(event), AMotionEvent_getX(event, 0),AMotionEvent_getX(event, 0));

    float jpsize=engine->width/8.0f;
    g_Joypad.dx0=0;
    g_Joypad.dy1=0;
    g_Joypad.dx0=0;
    g_Joypad.dy1=0;

    engine->animating=1;
    int count = AMotionEvent_getPointerCount(event);
    g_Touch.numTouch=count;
    TouchInput* touch=&g_Touch;
    for (int i=0; i<MAX_TOUCH; i++){touch->old_active[i]=touch->active[i];touch->active[i]=0;}
    for(int c = 0; c < count; c++) {
        int id = AMotionEvent_getPointerId(event, c);
        float x = AMotionEvent_getX(event, c);
        float y = AMotionEvent_getY(event, c);
        if (id<0 || id>MAX_TOUCH) {continue;} //its an error really.
        touch->x[id]=x; touch->y[id]=y;
        ALOGV("touch %d %.3f %.3f",id,x,y);
        if (action==AKEY_EVENT_ACTION_DOWN){
            ALOGV("press %d %.3f %.3f",id,x,y);
            touch->startx[id]=x;
            touch->starty[id]=y;
            touch->active[id]=1;
        } else if (action==AKEY_EVENT_ACTION_UP) {
            touch->endx[id]=x;
            touch->endy[id]=y;
            touch->active[id]=0;
        } else {
               touch->active[id]=1;
        }
        if (touch->x[id] < engine->width/2){
            float f=touch->active[id]?1.0f:0.0f;
            g_Joypad.dx0 = f*myclamp((float)(touch->x[id]-touch->startx[id])/jpsize,-1.0f,1.0f);
            g_Joypad.dy0 = f*myclamp((float)(touch->y[id]-touch->starty[id])/jpsize,-1.0f,1.0f);
        } else {
            float f=touch->active[id]?1.0f:0.0f;
            g_Joypad.dx1 = f*myclamp(((float)touch->x[id]-touch->startx[id])/jpsize,-1.0f,1.0f);
            g_Joypad.dy1 = f*myclamp((float)(touch->y[id]-touch->starty[id])/jpsize,-1.0f,1.0f);
        }
    }
    ALOGV("active = %d %d %d %d", touch->active[0],touch->active[1],touch->old_active[0],touch->old_active[1]);
    for (int i=0; i<MAX_TOUCH; i++){
        if (touch->active[i]){
            if (touch->x[i] < engine->width/2){
                g_Joypad.dx0 = (touch->x[i]-touch->startx[i])/jpsize;
                g_Joypad.dy0 = (touch->y[i]-touch->starty[i])/jpsize;
            } else {
                g_Joypad.dx1 = (touch->x[i]-touch->startx[i])/jpsize;
                g_Joypad.dy1 = (touch->y[i]-touch->starty[i])/jpsize;
            }
        }
    }

    ALOGV("virtual joypad: %.3f %.3f\n",g_Joypad.dx0,g_Joypad.dy0);
    if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION) {
        engine->animating = 1;
        engine->state.x = AMotionEvent_getX(event, 0);
        engine->state.y = AMotionEvent_getY(event, 0);
        return 1;
    }
    return 0;
}

/**
 * Process the next main command.
 */
static void engine_handle_cmd(struct android_app* app, int32_t cmd) {
    struct engine* engine = (struct engine*)app->userData;
    switch (cmd) {
        case APP_CMD_SAVE_STATE:
            // The system has asked us to save our current state.  Do so.
            engine->app->savedState = malloc(sizeof(struct saved_state));
            *((struct saved_state*)engine->app->savedState) = engine->state;
            engine->app->savedStateSize = sizeof(struct saved_state);
            break;
        case APP_CMD_INIT_WINDOW:
            // The window is being shown, get it ready.
            if (engine->app->window != NULL) {
                engine_init_display(engine);
                engine_draw_frame(engine);
            }
            break;
        case APP_CMD_WINDOW_RESIZED:
            // todo -how do we get the new size?
            //ShaderTest_Resize()
            break;
        case APP_CMD_TERM_WINDOW:
            // The window is being hidden or closed, clean it up.
            engine_term_display(engine);
            break;
        case APP_CMD_GAINED_FOCUS:
            // When our app gains focus, we start monitoring the accelerometer.
            if (engine->accelerometerSensor != NULL) {
                ASensorEventQueue_enableSensor(engine->sensorEventQueue,
                                               engine->accelerometerSensor);
                // We'd like to get 60 events per second (in us).
                ASensorEventQueue_setEventRate(engine->sensorEventQueue,
                                               engine->accelerometerSensor,
                                               (1000L/60)*1000);
            }
            break;
        case APP_CMD_LOST_FOCUS:
            // When our app loses focus, we stop monitoring the accelerometer.
            // This is to avoid consuming battery while not being used.
            if (engine->accelerometerSensor != NULL) {
                ASensorEventQueue_disableSensor(engine->sensorEventQueue,
                                                engine->accelerometerSensor);
            }
            // Also stop animating.
            //engine->animating = 0;
            engine_draw_frame(engine);
            break;
    }
}

/*
 * AcquireASensorManagerInstance(void)
 *    Workaround ASensorManager_getInstance() deprecation false alarm
 *    for Android-N and before, when compiling with NDK-r15
 */
#include <dlfcn.h>
ASensorManager* AcquireASensorManagerInstance(android_app* app) {

  if(!app)
    return nullptr;

  typedef ASensorManager *(*PF_GETINSTANCEFORPACKAGE)(const char *name);
  void* androidHandle = dlopen("libandroid.so", RTLD_NOW);
  PF_GETINSTANCEFORPACKAGE getInstanceForPackageFunc = (PF_GETINSTANCEFORPACKAGE)
      dlsym(androidHandle, "ASensorManager_getInstanceForPackage");
  if (getInstanceForPackageFunc) {
    JNIEnv* env = nullptr;
    app->activity->vm->AttachCurrentThread(&env, NULL);

    jclass android_content_Context = env->GetObjectClass(app->activity->clazz);
    jmethodID midGetPackageName = env->GetMethodID(android_content_Context,
                                                   "getPackageName",
                                                   "()Ljava/lang/String;");
    jstring packageName= (jstring)env->CallObjectMethod(app->activity->clazz,
                                                        midGetPackageName);

    const char *nativePackageName = env->GetStringUTFChars(packageName, 0);
    ASensorManager* mgr = getInstanceForPackageFunc(nativePackageName);
    env->ReleaseStringUTFChars(packageName, nativePackageName);
    app->activity->vm->DetachCurrentThread();
    if (mgr) {
      dlclose(androidHandle);
      return mgr;
    }
  }

  typedef ASensorManager *(*PF_GETINSTANCE)();
  PF_GETINSTANCE getInstanceFunc = (PF_GETINSTANCE)
      dlsym(androidHandle, "ASensorManager_getInstance");
  // by all means at this point, ASensorManager_getInstance should be available
  assert(getInstanceFunc);
  dlclose(androidHandle);

  return getInstanceFunc();
}


/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */


void android_main(struct android_app* state) {
    struct engine engine;

    memset(&engine, 0, sizeof(engine));
    state->userData = &engine;
    state->onAppCmd = engine_handle_cmd;
    state->onInputEvent = engine_handle_input;
    engine.app = state;
    g_assetMgr= engine.app->activity->assetManager;

    // Prepare to monitor accelerometer
    engine.sensorManager = AcquireASensorManagerInstance(state);
    engine.accelerometerSensor = ASensorManager_getDefaultSensor(
                                        engine.sensorManager,
                                        ASENSOR_TYPE_ACCELEROMETER);
    engine.sensorEventQueue = ASensorManager_createEventQueue(
                                    engine.sensorManager,
                                    state->looper, LOOPER_ID_USER,
                                    NULL, NULL);

    if (state->savedState != NULL) {
        // We are starting with a previous saved state; restore from it.
        engine.state = *(struct saved_state*)state->savedState;
    }

    // loop waiting for stuff to do.
    engine.animating=1;

    while (1) {
        // Read all pending events.
        int ident;
        int events;
        struct android_poll_source* source;

        // If not animating, we will block forever waiting for events.
        // If animating, we loop until all events are read, then continue
        // to draw the next frame of animation.
        while ((ident=ALooper_pollAll(engine.animating ? 0 : -1, NULL, &events,
                                      (void**)&source)) >= 0) {

            // Process this event.
            if (source != NULL) {
                source->process(state, source);
            }

            // If a sensor has data, process it now.
            if (ident == LOOPER_ID_USER) {
                if (engine.accelerometerSensor != NULL) {
                    ASensorEvent event;
                    while (ASensorEventQueue_getEvents(engine.sensorEventQueue,
                                                       &event, 1) > 0) {
//                        LOGI("accelerometer: x=%f y=%f z=%f",
//                             event.acceleration.x, event.acceleration.y,
//                             event.acceleration.z);
                    }
                }
            }

            // Check if we are exiting.
            if (state->destroyRequested != 0) {
                engine_term_display(&engine);
                return;
            }
        }

        if (engine.animating) {
            // Done with events; draw next animation frame.
            engine.state.angle += .01f;
            if (engine.state.angle > 1) {
                engine.state.angle = 0;
            }

            // Drawing is throttled to the screen update rate, so there
            // is no need to do timing here.
            engine_draw_frame(&engine);
        }
    }
}
//END_INCLUDE(all)
