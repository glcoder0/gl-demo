#line 1
vec4 texTriplanarArray4(sampler2DArray texarr,vec3 indices,vec3 indices2,float microTex,vec3 norm,vec3 texcoords,float macroScale){
	vec3 factors=triplanarFactorsFrom(norm);
	vec4 acc=vec4(0.f,0.f,0.f,0.f);
	vec3 macroUV=texcoords*macroScale;
	vec3 microUV=texcoords*0.2f;
	
	if (factors.x>0.01f)
		acc+=texture(texarr, vec3(-texcoords.yz,indices.x))
			*texture(texarr, vec3(-macroUV.yz,indices2.x))
			*texture(texarr, vec3(microUV.yz, microTex))
			*factors.x;
	if (factors.y>0.01f)
		acc+=texture(texarr, vec3(-texcoords.xz,indices.y))
		*texture(texarr, vec3(-macroUV.xz,indices2.y))
		*texture(texarr, vec3(microUV.xz, microTex))
		*factors.y;
	if (factors.z>0.01f)
		acc+=texture(texarr, vec3(-texcoords.xy,indices.z))
		*texture(texarr, vec3(-macroUV.xy,indices2.z))
		*texture(texarr, vec3(microUV.xy, microTex))
		*factors.z;
	return acc;
}
vec4 texTriplanarArray5(sampler2DArray texarr,vec3 indices,vec3 indices2,vec3 norm,vec3 texcoords,float microScale,vec3 scaleuv){
	vec3 factors=triplanarFactorsFrom(norm);
	vec4 acc=vec4(0.f,0.f,0.f,0.f);
	
	vec3 microUV=texcoords*microScale;
	
	if (factors.x>0.01f)
		acc+=texture(texarr, vec3(-texcoords.yz*scaleuv.xx,indices.x))
			*texture(texarr, vec3(microUV.yz*scaleuv.xx, indices2.x))
			*factors.x;
	if (factors.y>0.01f)
		acc+=texture(texarr, vec3(-texcoords.xz*scaleuv.yy, indices.y))
		*texture(texarr, vec3(microUV.xz*scaleuv.yy, indices2.y))
		*factors.y;
	if (factors.z>0.01f)
		acc+=texture(texarr, vec3(-texcoords.xy*scaleuv.zz,indices.z))
		*texture(texarr, vec3(-microUV.xy*scaleuv.zz,indices2.z))
		*factors.z;
	return acc;
}

out vec4 outFragColor;
void main() {

	vec3 norm=normalize(v_norm);
    //0=grey gravel 1=browb gravel 2=grass,3=rock,4=greybrown,5=cliff
    float ysel=v_pos.z-30.0;
	//float indexh=ysel>0.0?4:0;
	float indexv=1;

	vec4 tex0color=texTriplanarFromArray(
            uTexArray0,
            ysel<0.0?vec3(3,3,2):vec3(5,5,4),
            norm, v_pos.xyz*0.05f);

	vec4 tex2color=texTriplanarFromArray(
            uTexArray0,
            ysel<0.0?vec3(3,3,2):vec3(5,5,4),
            norm, v_pos.xyz*0.01f);

	vec4 tex1color=texTriplanarFromArray(uTexArray0, vec3(1,1,0), norm,v_pos.xyz*0.25);

	vec4 texcolor=texTriplanarArray5(uTexArray0,
			ysel<0.0?vec3(7,7,1):vec3(7,7,3),
			ysel<0.0?vec3(0,0,0):vec3(0,0,0),
			norm,v_pos.xyz*0.0125,8.1f,
			vec3(1.0,1.0,2.0))*1.4f;

//	vec4 albedo=vec4((tex0color*tex1color*tex2color*v_color*2.0f*1.4f).xyz, 1.0);
	vec4 albedo=vec4(texcolor.xyz,1.0);

	// Scaling factor for use of multiplicative microtexture. assumes its 50% average
	float specFactor= improvisedSpecularFactor(albedo);
	
	outFragColor = applyLight(albedo,norm,specFactor,shadowFactor());
	//outFragColor = (gl_FragCoord.x<960)?vec4(shadowZ):vec4(shadowPos.z);
	//v_pos.zzzz;

	//outFragColor = vec4(gl_FragCoord.x/1920.0,gl_FragCoord.y/1080.0,0.f,1.0f);//
	// dbg- show shadowmap
	//outFragColor = texture(uShadowMap[gl_FragCoord.x<960?0:1], vec2(gl_FragCoord.x/1920.0,gl_FragCoord.y/1080.0));

}

