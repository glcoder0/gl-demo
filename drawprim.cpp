
#include "drawprim.h"

// todo - misc gl helpers, shouldn't depend on mesh for this
extern GLuint glGenVertexArrayAndBind();
extern GLuint glGenBufferAndBind(GLuint typeOfTarget);


DebugLines g_DebugDraw;
SpriteBuffer g_Sprites;
int g_ShowDebugLines=0;
GLuint g_QuadVAO;
GLuint g_QuadVBO;

void DrawSomeDebugLines(){
	g_DebugDraw.rect2d(-0.9,0.9,-0.8,0.8,0xffffffff);
	g_DebugDraw.cross(0.85,0.85,0.05,0xffffffff);
	g_DebugDraw.line2d(VertexC{Vec3{-0.9,0.9,0.0},RGBA8(0xff0000)}, VertexC{Vec3{-0.8,0.8,0.0},RGBA8(0x00ff00)});
	g_DebugDraw.line2d(VertexC{Vec3{-0.9,0.8,0.0},RGBA8(0x0000ff)}, VertexC{Vec3{-0.8,0.9,0.0},RGBA8(0x0000ff)});
	g_DebugDraw.flush();
}


void CreateQuad() {
    if (g_QuadVAO) return;  // lazy creation , skip if already done.
	static const GLfloat s_quad_vertices[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f,  1.0f, 0.0f,
	};

	g_QuadVAO=glGenVertexArrayAndBind();
	
	g_QuadVBO=glGenBufferAndBind(GL_ARRAY_BUFFER);
    glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,12,(void*)0);
	glBufferData(GL_ARRAY_BUFFER,sizeof(float)*3*3*2,(void*)s_quad_vertices, GL_STATIC_DRAW);
	glBindVertexArray(0);
}
void DebugDrawQuad(Vec2 pos,Vec2 size, GLuint texId,Vec4 colorMul,Vec4 colorOfs){
    if (!g_QuadVAO)CreateQuad();
	glUseProgram(g_ShaderPrograms.debugQuad);
	glBindVertexArray(g_QuadVAO);

	glSetTextureLayer(0,texId);

	SetUniform(0,Vec4(pos.x,pos.y,0.f,1.f));
	SetUniform(1,Vec4(pos.x+size.x,pos.y,0.f,1.f));
	SetUniform(2,Vec4(pos.x,pos.y+size.y,0.f,1.f));
	SetUniform(3,Vec4(pos.x+size.x,pos.y+size.y,0.f,1.f));
	SetUniform(4,colorOfs);
	SetUniform(5,colorMul);


	glBindBuffer(GL_ARRAY_BUFFER,g_QuadVBO);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}

void DrawQuad(Vec2 pos,Vec2 size,GLuint shader, GLuint texId0,GLuint texId1, GLuint target, Vec4 parameter0,Vec4 parameter1)
{
    if (!g_QuadVAO)CreateQuad();
	glUseProgram(shader);
	glBindVertexArray(g_QuadVAO);

	if (target!=0xffff0000){// need option type.
		glBindFramebuffer(GL_FRAMEBUFFER, target);
	}
	glSetTextureLayer(0,texId0);
	glSetTextureLayer(1,texId1);

	SetUniform(0,Vec4(pos.x,pos.y,0.f,1.f));
	SetUniform(1,Vec4(pos.x+size.x,pos.y,0.f,1.f));
	SetUniform(2,Vec4(pos.x,pos.y+size.y,0.f,1.f));
	SetUniform(3,Vec4(pos.x+size.x,pos.y+size.y,0.f,1.f));
	SetUniform(4,parameter0);
	SetUniform(5,parameter1);

	glBindBuffer(GL_ARRAY_BUFFER,g_QuadVBO);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

}



