#pragma once
#include "shaders.h"
#include "mesh.h"
// buffer util for drawing primitives generated on the CPU 
// + various debug drawing primitives (bounding boxes etc)
extern GLuint g_QuadVAO;
extern GLuint g_QuadVBO;



template<typename VERTEX,int PRIM_TYPE>
struct	DrawBuffer{	// for cpu to write vertices into
	DrawBuffer(){}
	GLuint m_vao,m_vbo;
	bool m_init;
	const int MaxVertices=1024*16;
	GLuint shader;
	std::vector<VERTEX> vertices;
	void Init() {
		vertices.reserve(MaxVertices);
		if (!m_vao){ 
			m_vao=glGenVertexArrayAndBind();
			m_vbo=glGenBufferAndBind(GL_ARRAY_BUFFER);
			glSetVertexAttribPointers<VERTEX>();
		    //glEnableVertexAttribArray(0);
		    //glEnableVertexAttribArray(1);
			//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VERTEX), (void*)0);
			//glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexC), (void*)12);
			vertices.resize(MaxVertices);
			glBufferData(GL_ARRAY_BUFFER,sizeof(VERTEX)*MaxVertices,(void*)&vertices[0],GL_DYNAMIC_DRAW);
			vertices.resize(0);
			shader = (sizeof(VERTEX)==sizeof(VertexC))?g_ShaderPrograms.color:g_ShaderPrograms.particle;
			// todo - configure shader properly.
		}
	}
	inline void lazyInit(){
		if (!m_init){this->Init();m_init=true;}
	}
	void flush3d(const Matrix4& projView){
		glEnable(GL_DEPTH_TEST);
		render(projView);
		discard();
	}
	void discard(){
		vertices.resize(0);
	}
	void flush() {
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
	    glEnableVertexAttribArray(0);
	    glEnableVertexAttribArray(1);
		render(Matrix4::identity());
		vertices.resize(0);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		discard();
	}
	void render(const Matrix4& projView,GLuint tex0=0,GLuint tex1=1,GLuint tex2=2,GLuint tex3=3){
		int nvtc=vertices.size();
		if (!nvtc) return;
		glDisable(GL_CULL_FACE);
		glEnable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);
		glAlphaFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glAlphaFunc(GL_ONE,GL_ONE);
		glUseProgram(shader);
		SetUniform(uMatProj,projView);
		glSetTextureLayer(0,tex0);
		glSetTextureLayer(1,tex1);
		glSetTextureLayer(2,tex2);
		glSetTextureLayer(3,tex3);
		
	    glEnableVertexAttribArray(0);
	    glEnableVertexAttribArray(1);
	    glEnableVertexAttribArray(2);
		glBindVertexArray(m_vao);
		glBindBuffer(GL_ARRAY_BUFFER,m_vbo);
		glBufferData(GL_ARRAY_BUFFER,sizeof(VERTEX)*nvtc,(void*)&vertices[0], GL_DYNAMIC_DRAW);
		glDrawArrays(PRIM_TYPE,0,nvtc);
		glDepthMask(true);
		glDisable(GL_BLEND);
		glEnable(GL_CULL_FACE);
	}
};

struct SpriteBuffer : public DrawBuffer<VertexCT2, GL_TRIANGLES>{
	// default - vertical alignment. 
	
	void drawQuad(const VertexCT2& v0,const VertexCT2& v1,const VertexCT2& v2,const VertexCT2& v3){
		vertices.push_back(v0);
		vertices.push_back(v1);
		vertices.push_back(v3);
		vertices.push_back(v0);
		vertices.push_back(v3);
		vertices.push_back(v2);
	}
	void drawSprite(const CamView& cv, Vec4 pos, uint32_t color, float r,float scroll=0.f) {
		lazyInit();
		auto ay=cv.up();//Vec4(0.0,0.0,1.0,0.0);
		auto ax=cv.horiz();//Vec4(1.0,0.0,0.0,0.0);//cv.fwd().cross(ay);
		auto p0 = pos-ay*r;
		auto p1 = pos+ay*r;
		auto dx=ax*r;
		auto rgba=(TVec4<uint8_t>&) color;	// TODO - packed type constructor.
		// 2nd UV will be used to animate..
		float s0=scroll;
		float s1=scroll*-1.f;
		drawQuad(
			VertexCT2{(p0-dx).xyz(), rgba, Vec2(0.0f,0.0f),Vec2(0.0f,0.0f-scroll) },
			VertexCT2{(p0+dx).xyz(), rgba, Vec2(1.0f,0.0f),Vec2(1.0f,0.0f-scroll) },
			VertexCT2{(p1-dx).xyz(), rgba, Vec2(0.0f,1.0f),Vec2(0.0f,1.0f-scroll) },
			VertexCT2{(p1+dx).xyz(), rgba, Vec2(1.0f,1.0f),Vec2(1.0f,1.0f-scroll) });
	}
	void lineSprite(const CamView& cv, Vec4 pos0,Vec4 pos1, uint32_t color, float r,float scroll=0.f) {
		lazyInit();
		// todo - shader to raytrace volumetric capsule!!!
		// do it 90's style for now.
		Vec4 spriteAxis=pos1.sub(pos0).normalize();
		Vec4 horiz = pos0.sub(cv.pos()).cross(spriteAxis).normalize();
		auto dx=horiz*r;
		auto rgba=(TVec4<uint8_t>&) color;	// TODO - packed type constructor.
		// 2nd UV will be used to animate..
		float s0=scroll;
		float s1=scroll*-1.f;
		drawQuad(
			VertexCT2{(pos0-dx).xyz(), rgba, Vec2(0.0f,0.0f),Vec2(0.0f,0.0f-scroll) },
			VertexCT2{(pos0+dx).xyz(), rgba, Vec2(1.0f,0.0f),Vec2(1.0f,0.0f-scroll) },
			VertexCT2{(pos1-dx).xyz(), rgba, Vec2(0.0f,1.0f),Vec2(0.0f,1.0f-scroll) },
			VertexCT2{(pos1+dx).xyz(), rgba, Vec2(1.0f,1.0f),Vec2(1.0f,1.0f-scroll) });
	}

};

struct DebugLines: public DrawBuffer<VertexC,GL_LINES> { // TODO - back in rust, that has to be compoosition not inheritance..

	DebugLines& drawAxes(const Matrix4& mt, float r=10.f,uint32_t color = 0xffffff){
		drawCorner(mt,Vec4(0.0),Vec3(r,r,r), 0xffff0000,0xff00ff00,0xff0000ff);
		return *this;
	}
	void drawCorner(const Matrix4& mt, const Vec4& point,const Vec3& size,uint32_t colorx,uint32_t colory,uint32_t colorz){
		auto cp=mt*point;
		line3d(cp,cp+mt.ax*size.x,colorx);
		line3d(cp,cp+mt.ay*size.y,colory);
		line3d(cp,cp+mt.az*size.z,colorz);
	}
	void drawBox(const Matrix4& mt, uint32_t color,const Extents4f& e){drawBox(mt,color,e.min.x,e.min.y,e.min.z, e.max.x,e.max.y,e.max.z);}
	void drawSphere(uint32_t color,const Vec4& centre,float r){
		// TODO draw curve from lambda.
		float a=0.f,da=2*PI/32.f;
		Vec4 opx=Vec4(0.f,r,0.0f,0.0f)+centre;
		Vec4 opy=Vec4(r,0.0f,0.f,0.0f)+centre;
		Vec4 opz=Vec4(0.f,0.0f,r,0.0f)+centre;
		for (int i=1; i<=32; i++,a+=da){
			float s=sin(a)*r,c=cos(a)*r;
			Vec4 px = Vec4(0.0,	c,	s,	0.f)+centre;
			Vec4 py = Vec4(c, 0.f,		s,	0.f)+centre;
			Vec4 pz = Vec4(c,	s,	0.f,		0.f)+centre;
			line3d(opx,px,color);
			line3d(opy,py,color);
			line3d(opz,pz,color);
			opx=px;
			opy=py;
			opz=pz;
		}
	}
	void drawBox(const Matrix4& mt, uint32_t color, float x0,float y0,float z0, float x1,float y1,float z1){
		auto rsize=Vec3(x1-x0,y1-y0,z1-z0);
		auto maxs=max(rsize.x,max(rsize.y,rsize.z));
		auto mins=rsize*(1.0f/4.0f);
		auto  size=Vec3(maxs).min(mins);
		// box points.
		Vec4 corner[8];
		for (int i=0; i<8;i++){
			drawCorner(// todo:: VecN::boxcorner(i)= unit corner 0-8
				mt,Vec4(i&1?x0:x1,i&2?y0:y1,i&4?z0:z1,1.f), 
				Vec3(i&1?size.x:-size.x,i&2?size.y:-size.y,i&4?size.z:-size.z),
				color,color,color);
		}
	}
	void drawCross3d(const Vec4& pt,uint32_t color, float size){
		line3dp(pt.subadd(Vec4(size,0.f,0.f,0.f)),color);
		line3dp(pt.subadd(Vec4(0.f,size,0.f,0.f)),color);
		line3dp(pt.subadd(Vec4(0.f,size,0.f,0.f)),color);
	}

	void drawGridXY(const Matrix4& mt,uint32_t color, float cellSize,int start,int end){
		float sx=(float)start*cellSize;
		float ex=(float)end*cellSize;
		float sy=(float)start*cellSize;
		float ey=(float)end*cellSize;
		for (int i=start; i<=end; i++){

			float fi=(float)i;
			
			line3d({Vec3(fi*cellSize,sy,0.f),RGBA8(color)},{Vec3(fi*cellSize,ey,0.f),RGBA8(color)});
			line3d({Vec3(sx,fi*cellSize,0.f),RGBA8(color)},{Vec3(ex,fi*cellSize,0.f),RGBA8(color)});
		}

	}
	DebugLines& line3d(const Vec4& a,const Vec4& b,uint32_t color){
		line3d({a.xyz(),RGBA8(color)},{b.xyz(),RGBA8(color)});
		return *this;
	}
	void line3dp(const std::pair<Vec4,Vec4>& line,uint32_t color){
		line3d(line.first,line.second,color);
	}
	void line3d(VertexC v0,VertexC v1){
		lazyInit();
		vertices.push_back(v0);
		vertices.push_back(v1);
	}
	void line2d(VertexC v0,VertexC v1){
		lazyInit();
		if (!m_init){this->Init();m_init=true;}
		vertices.push_back(v0);
		vertices.push_back(v1);
		if (vertices.size()>=MaxVertices) {this->flush();};
	}
	void line2d(Vec4 a,Vec4 b, uint32_t color){
		line2d(VertexC{a.xyz(),RGBA8(color)},VertexC{b.xyz(),RGBA8(color)});
	}
	void cross(float x0,float y0,float r,uint32_t color) {
		line2d(VertexC{Vec3{x0-r,y0-r,0.0},RGBA8(color)},VertexC{Vec3{x0+r,y0+r,0.0},RGBA8(color)});
		line2d(VertexC{Vec3{x0+r,y0-r,0.0},RGBA8(color)},VertexC{Vec3{x0-r,y0+r,0.0},RGBA8(color)});
	}
	void rect2d(float x0,float y0, float x1,float y1, uint32_t color){
		VertexC vt[4]={
			{Vec3{x0,y0,0.0},RGBA8(color)},
			{Vec3{x1,y0,0.0},RGBA8(color)},
			{Vec3{x0,y1,0.0},RGBA8(color)},
			{Vec3{x1,y1,0.0},RGBA8(color)},
		};
		line2d(vt[0],vt[1]);
		line2d(vt[2],vt[3]);
		line2d(vt[0],vt[2]);
		line2d(vt[1],vt[3]);
	}
	void cuboid(Vec4 points[8],uint32_t color){
		static int edgei[12][2]={
			{0,1},{2,3},{0,2},{1,3},
			{0,4},{1,5},{2,6},{3,7},
			{4,5},{4,7},{4,6},{5,7}
		};
		for (int i=0; i<12; i++){
			auto si=edgei[i][0],ei=edgei[i][1];
			this->line2d(points[si],points[ei],color);
		}
	}
};

extern DebugLines g_DebugDraw;
extern SpriteBuffer g_Sprites;
extern int g_ShowDebugLines;

void DrawSomeDebugLines();
void CreateQuad();
void DebugDrawQuad(Vec2 pos,Vec2 size, GLuint texId,Vec4 colorMul=Vec4(1.f,1.f,1.f,1.f),Vec4 colorOfs=Vec4(0.f,0.f,0.f,0.f));
void DrawQuad(Vec2 pos,Vec2 size,GLuint shader, GLuint texId0,GLuint texId1=0, GLuint target=0xffff0000, Vec4 parameter0=Vec4(0.f,0.f,0.f,0.f),Vec4 parameter1=Vec4(1.f,1.f,1.f,1.f));




