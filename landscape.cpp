#include "vecmathnew.h"
#include "landscape.h"

Landscape* g_pLandscape;

void SetLights(const RenderCtx& ctx);
extern Mesh* g_pTorus;
float Landscape::getHeight(Vec4 pos)const{
//	Vec2 ixy = (pos.xy()-origin.xy())*invCellSize & sizemask;
	auto ixy=Vec2i((pos.xy()-origin.xy())*invCellSize) & sizemask;

	//int ix=((int)((pos.x-origin.x)*invCellSize)) & sizemask;
	//int iy=((int)((pos.y-origin.y)*invCellSize)) & sizemask;
	//float fx=pos.x*invCellSize;
	//float fy=pos.y*invCellSize;
	auto fxy = pos.xy()*invCellSize;
	auto oxy = fxy - Vec2(floor(fxy.x),floor(fxy.y));
	//float ox=fxy.x-floor(fxy.x);
	//float oy=fxy.y-floor(fxy.y);

	return bilerp(getHeightI(ixy),getHeightI(ixy+Vec2i(1,0)),getHeightI(ixy+Vec2i(0,1)),getHeightI(ixy+Vec2i(1,1)),oxy);
	// todo -interp normal.
}

PosNorm Landscape::rayTraceDown(Vec4 pos) const{
	// todo - formally define the heightfield as cubic, linear, or what.
	float x=pos.x,y=pos.y;
	float ofs=cellSize*0.5f;
	float z=getHeight(pos), 
		zl=getHeight(Vec4(x-ofs,y,z,1.f)),
		zr=getHeight(Vec4(x+ofs,y,z,1.f)),
		zu=getHeight(Vec4(x,y-ofs,z,1.f)),
		zd=getHeight(Vec4(x,y+ofs,z,1.f));

	float nx = -(zr-zl)/(ofs*2.f); // todo .. not sure this is really precise
	float ny = -(zd-zu)/(ofs*2.f);
	float nz = 1.0f;
	float inv=1.0f/sqrt(nx*nx+ny*ny+nz*nz);
	return	PosNorm{ Vec4(x,y,z,1.0f), Vec4(nx,ny,nz,0.f) };
}

Landscape* MakeLandscape(int side,float cellsize,int numStaticObjects,const char** landscape_texnames, float* htarray){
	auto* ls=new Landscape;
	g_pLandscape=ls;	// TODO this is horrible. eliminate g_pLandscape
	ls->init(side,cellsize,numStaticObjects, landscape_texnames, htarray);
	return ls;
}

void GenHeights(int side, float cellSize, std::vector<float>& heights){
    heights.resize(side*side);
	for (auto& ht:heights){ht=0.f;}
	float av=0.f;
	
	printf("average=%f\n",av/256.0f);
	const int NumShelf=5;
	std::vector<float> tmph[NumShelf];

	float shelf=0.01f;
	for (int k=0; k<NumShelf ;k++){
		tmph[k].resize(side*side);
		if (k==4){
			midpoint_displacement_fractal(&tmph[k][0],side,side,1.0f, 0,0.05f,0.53f,0xff981224);
		} else {
			midpoint_displacement_fractal(&tmph[k][0],side,side/4,0.25f, 0,0.05f,0.53f,0xff981224);
		}
		for (auto& ht : tmph[k]){
/*			
			float v=ht+0.0f;
			if (v<0.f) v*=0.01f;
			if (v>0.0) {v*=1.0/(1.f-0.1f); v*=v;}
			if (v>shelf) {
				v-=shelf;
				if (v>=0.f) {
					v*=0.05f;
				}
				v+=shelf;
			}
*/
			if (k==4) {
				ht=sin(ht*6.f)*0.33f;
				ht-=0.125f;
				if (ht<0.f) ht=0.f;
				ht-=0.02f;
				
				//ht=ht*ht*10.f;
			} else
			if (k==3){
				ht = (sin(ht*64.f)*0.5f+ht);
				if (ht<0.f) ht=0.f;;
			} else if (k<3){
				ht = (sin(ht*48.f)*0.5f+ht);
				ht+=0.25f;
				if (ht<0.f) ht=0.f;;
				ht-=0.25f;

			}
		}
	}
	
	int hi=0;

	for ( auto& ht : heights){

		float v=ht-0.1f;
		if (v<0.f) v*=0.02f;
		if (v>0.0) {v*=1.0/(1.f-0.1f); v=exp(log(v)*1.2f);}
//		if (v>shelf) v=shelf+(v-shelf)*0.125f;//v=0.1f-(v-0.1f)*0.1f;
		// try to make initilization of a cameraaround 0,0,0 reasonable -
		// move it down 'a bit'.
		//v=v+tmph[hi]+tmph[hi];
		
		//v+=tmph[0][hi]+(tmph[1][hi]-tmph[2][hi])*2.f-tmph[3][hi]+tmph[4][hi]-tmph[5][hi];
		v=0;
		v+=(tmph[0][hi]+tmph[2][hi])*tmph[1][hi]*0.025f;
		v+=tmph[3][hi]*0.0125f;
		v+=tmph[4][hi]*0.25f;

		v = v*side*cellSize; // vertical scaling
			//*ptr *= (pow(0.5f * *ptr + 0.5f,3.0f)-0.5f) ;//*;
		ht=v;//+frands(10.f);
		hi++;
	}
}
void
Landscape::init(int side,float _cellSizeIn, int numStaticObjects, const char** landscape_tex,float *overide_ht){
    int num_ao_samples=16;
    m_textureArray = LoadTextureArray(Vec2i(512,512),3,landscape_tex);
    if (!g_pTorus){
       	g_pTorus = Mesh::make_torus(16,16, 1.0f,0.33f);
    }

	float worldSize=side * _cellSizeIn;

	LOGI("creating landscape %d x %d",side,side);
	this->sizemask=side-1; // todo - assert PowerOf2..
	this->cellSize=_cellSizeIn;
	this->xsize=this->ysize = side;
	this->invCellSize=1.0/_cellSizeIn;
	// centre it around world 0,0,0
	this->origin=Vec4(-side*(float)cellSize*0.5f,-side*(float)cellSize*0.5f,0.0f,1.0f);
	this->microtex = g_Textures.get("gravel_midgrey.jpg");

	int i,j;
	heights.resize(side*side);
    if (overide_ht) {
        for (i=0; i<side*side;i++){heights[i]=overide_ht[i];}
    } else
        GenHeights(side,cellSize,heights);

	auto blockSize=std::max(xsize,ysize);
	while (blockSize*blockSize>8*1024 && (xsize/blockSize)*(ysize/blockSize)<1024){
		blockSize/=2;
	}
	//auto blockSize=sqrt(side); // 2048 as aprox 32 x 32 x (32x32)
	this->renderable = new RenderNode;
	//mesh=new Mesh();
	for (int blockv=0; blockv<side; blockv+=blockSize) {
		for (int blocku=0; blocku<side; blocku+=blockSize){
	
			auto subMesh = Mesh::make_grid_from_fn<TestVertex>(
				blockSize+1,blockSize+1,
				[&](int u1,int v1)->TestVertex{
					auto u=u1+blocku,v=v1+blockv;
					auto ht=&heights[0];
					auto dz_dx = (float)(*addr2d(ht,xsize, u+1,v) -*addr2d(ht,xsize, u-1,v))/(2.0f*cellSize);
					auto dz_dy = (float)(*addr2d(ht,xsize, u,v+1) -*addr2d(ht,xsize, u,v-1))/(2.0f*cellSize);
					float nx = -dz_dx; // todo .. not sure this is really precise
					float ny = -dz_dy;
					float nz = 1.0f;
					float inv=1.0f/sqrt(nx*nx+ny*ny+nz*nz);
					float fu=(float) u,fv=(float) v;
					auto fuv=Vec2(u,v);

					//auto zpos=heights[(u%xsize)+(v%ysize)*xsize];
					auto posxy = Vec2(u,v)*cellSize;
					//auto xpos = cellSize*(float)u;
					//auto ypos = cellSize*(float)v;
					auto zpos = this->getHeight(Vec4(posxy,0.0f,0.0f)+origin);
					// AO aproximation. look at points in a cone
					// todo - horizion tracing, more accurate
					float acc=0.f;
					float invao=0.f;
					float radius=cellSize*16.0f;
					for (i=0; i<num_ao_samples; i++) {
						auto delta=Vec2Random(cellSize*radius);
						float extrapolated_z = zpos + dz_dx * delta.x + dz_dy * delta.y;
						auto sample_z = this->getHeight(Vec4(posxy+delta,0.f,0.f)+origin);
						//float light = clamp01(1.0f-(extrapolated_z - sample_z)/radius);
						float light = clamp01(1.0f+(extrapolated_z-sample_z)/radius); // parts will compensate
						acc+=light;
						
						invao+=clamp01(1.0f+(sample_z-extrapolated_z)/radius); //'corner factor'
					}
					acc*=1.0/(float)num_ao_samples;
					invao*=1.0/(float)num_ao_samples;
					
					unsigned char val = (int)(255.0f*clamp01((acc+1.f)*0.5f));

					return TestVertex{
						{origin.x+posxy.x,origin.y+posxy.y, origin.z+zpos},
						{val,val,val,val},
						{nx*inv, ny*inv, nz*inv},// todo -calc normal.
						
						//Layers<array<Half,2>,2>{
						{	array<Half,2>({fu,fv}),	// todo - texcoords should allow breaks etc.
							array<Half,2>({fu,fv})
						}
						//}
					};
				}
			);
			int microtex=g_Textures.get("gravel_midgrey.jpg");
			subMesh->tex0=microtex;
			subMesh->tex1=microtex;
			subMesh->shader = g_ShaderPrograms.landscape;
			RenderNode* node=new RenderNode(subMesh,Matrix4::identity());
			renderable->push_child(node);

			int waterSize=4;	// lower res for water mesh
			subMesh = Mesh::make_grid_from_fn<TestVertex>(
				waterSize+1,waterSize+1,
				[&](int u1,int v1)->TestVertex{
					int u=blocku+(u1*blockSize/waterSize);
					int v=blockv+(v1*blockSize/waterSize);
					float fu=(float) u,fv=(float) v;
					auto posxy = Vec2(u,v)*cellSize;
					return TestVertex{
						Vec3(origin.x+posxy.x,origin.y+posxy.y,0.f),
						{128,128,128,224 },
						Vec3(0.f,0.f,1.f),
						{
							std::array<Half,2>{{Half((float)fu),Half((float)fv)}},
							std::array<Half,2>{{Half((float)fu),Half((float)fv)}}
						}
					};
				});

			subMesh->tex0=g_Textures.get("n_water.jpg");
			subMesh->tex1=g_Textures.get("horizon.jpg");
			subMesh->shader = g_ShaderPrograms.water;
			subMesh->transparent=true;
			node = new RenderNode(subMesh,Matrix4::identity());
			renderable->push_child(node);
		}
	}
	renderable->buildTree();
	//mesh->init_bounds();
	LOGI("creating landscape done %d x %d",side,side);

    // default for testing is to scatter some random static objects into the world.

	if (numStaticObjects)
		this->staticObjects=initStaticObjects(this,worldSize,numStaticObjects);
	else this->staticObjects=nullptr;
}

void Landscape::render(RenderCtx& rc)const {
	auto eyepos =rc.cv.pos();

	float worldSize = this->cellSize* (float) this->xsize;
	// need +ve for fmod
	Vec4 origin=Vec4(-worldSize*32.f,-worldSize*32.f,0.f,1.0f);
	Vec4 ofs2=eyepos+origin;
	float dx=fmod(ofs2.x,worldSize);
	float dy=fmod(ofs2.y,worldSize);
    // render 2x2 copies of the world for wraparound. Most of this will be trivially culled.
	for (int j=-1; j<1; j++) {
		for (int i=-1; i<1; i++) {
			Vec4 wrapOfsCentre=Vec4(eyepos.x-dx + i*worldSize,eyepos.y-dy + j*worldSize,0.f,1.f);
			if(!rc.cv.is_shadow) {
//				printf("ofs %.3f %.3f   %.3f %3f   \n", dx,dy, ofs2.x, ofs2.y);
			}

            glUseProgram(rc.cv.is_shadow?g_ShaderPrograms.landscapeShadowPass:g_ShaderPrograms.landscape);
			Matrix4 putWorldAt=Matrix4::translate(wrapOfsCentre);
			SetUniform(uMatProj, rc.cv.proj_view);
			SetUniform(uMatModelView, putWorldAt);

			SetUniform(uEyePos,rc.cv.pos());
            // this was redundant. mesh render calls will get their lights
            //rc.setLightShaderConstants(Matrix::identity(),)
			//SetLights(rc);	// TODO - llandsacpevel on both
			
            // the landscape exclusively uses a texture-array.
			glSetTextureLayer(0, 0);
			glSetTextureLayer(1, 0);	//
			glSetTextureLayer(2, g_Textures.get("n_water.jpg"));	//
			
			//glUniform1i(uTexArray, 4);
			glSetTextureLayerEx(4,m_textureArray,GL_TEXTURE_2D_ARRAY);
			renderable->render(rc,putWorldAt);
			glSetTextureLayerEx(4,0,GL_TEXTURE_2D_ARRAY);
			if (this->staticObjects) {
				glUseProgram(g_ShaderPrograms.tex2Blend);
				SetUniform(uMatProj, rc.cv.proj_view);
				// TODO - from a world light struct
				// SetLights(rc);	// TODO - light culling against 'landscape chunks'...
				//  TODO from the model or instance!!
				//glSetTextureLayer(0, g_Texture[6]); //70pc grey microtexture.
				//glSetTextureLayer(1, g_Texture[6]);	//

				this->staticObjects->render(rc,putWorldAt);
			}
		}
	}
}



RenderNode* initStaticObjects(Landscape* ls,float worldSize, int numObjects) {
	
	int numPerCluster=std::max(numObjects/512,16);
	// TODO  - flat array approach, get rid of those linklists!!!
	RenderNode*	root=new RenderNode;
	float clusterRadius=100.f;
	for (int i=0; i<numObjects;i+=numPerCluster){

		Vec4 clusterc=Vec4(frands(worldSize*0.5f),frands(worldSize*0.5f),0.f,1.f);;
		for (int j=0 ;j<numPerCluster; j++){
			auto wn= new RenderNode;
			float scaling=4.f+frands(3.f);
			// translate, scale,rotate;
			//= rot * scale* trans * point
			wn->matrix = Matrix4::rotZ(frands(PI)) * Matrix4::rotX(frands(PI))*Matrix4::scale(scaling);
			wn->matrix.aw=clusterc+Vec4(gaussRand(clusterRadius),gaussRand(clusterRadius),0.f,0.f);
			auto h=ls->getHeight(wn->matrix.aw);
			float dy = (frands(0.5f)+0.5f); dy=dy*dy;
            // copy and retexture
			auto mesh = new Mesh(*g_pTorus); 
			wn->pMesh=mesh;
			wn->matrix.aw.z=h + dy*scaling*5.f+ wn->pMesh->sphere.radius*scaling;
			// todo - matrices to support scaling!!!
			wn->bounds.centre=wn->matrix.aw;//wn->matrix * Vec4(wn->pMesh->sphere.centre;
			wn->bounds.radius= wn->pMesh->sphere.radius*scaling;

			int ntex=g_Textures.size();
			mesh->tex0 = g_Textures[((j+i)%ntex)];
			mesh->tex1 = g_Textures[((j+i)/ntex)%ntex];
			mesh->shader = g_ShaderPrograms.tex2Blend;
			root->push_child(wn);
		}
	}
	printf("build static object tree.\n");
	root->buildTree();
	return root;
}

