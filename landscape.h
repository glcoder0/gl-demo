#pragma once
#include "vecmathnew.h"
#include "mesh.h"
#include "resourcemgr.h"

struct Mesh;
struct RenderNode;
struct Landscape {
	// TODO: cell data. height, attributes.
	Vec4 origin;
	std::vector<float> heights;
	int	xsize;
	float cellSize;
	float invCellSize;
	int ysize;
	int sizemask;
	GLuint microtex;
	void init(int side,float cellSize, int numStaticObjects, const char** texnames, float* ht=nullptr);
	Mesh* mesh=0;
	void render(RenderCtx&)const; // todo - passes, camera, ...
	PosNorm rayTraceDown(Vec4 pos) const;
	float getHeight(Vec4 pos) const;
	// todo - actually store normals etc in cpu accessible form. interpolate from those.
	float getHeightI(int i,int j)const {return heights[(i&sizemask)+xsize*(j&sizemask)];}
	float getHeightI(Vec2i ij)const {return getHeightI(ij.x,ij.y);}
	RenderNode*	staticObjects=nullptr;
	RenderNode*	renderable=nullptr;
    GLuint m_textureArray;
};

Landscape* MakeLandscape(int side,float cellsize,int numRandomStaticObjects,const char** landscape_texnames, float* htarray);
// better to add the static objects afterward, but there's a hardcoded test inbuilt.

RenderNode* initStaticObjects(Landscape*,float worldSize, int num);
extern  Landscape* g_pLandscape;// todo.. eliminate globals
