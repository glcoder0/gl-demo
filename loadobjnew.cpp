#include "vecmathnew.h"
#include <map>
#include <string>
#include <iostream>     // std::ios, std::istream, std::cout
#include <fstream>      // std::filebuf
#include <sstream>
#include "loadobjnew.h"


auto    loadOBJMaterialLib (MatLib& mlib,const string& filename)->void {
	ifstream fs;
	fs.open (filename) ;
	if (!fs.good())
	{
		cout<<__FUNCTION__<<"could not open "<<filename<<"\n";
		return;
	}
	cout<<"Reading MatLib:"<<filename<<"\n";
	do {
		string tok,line;
		fs>>tok;
		cout <<"matlib line token"<< tok<<"\n";
		if (tok=="newmtl") {
		nextMat:
			string texName="default.tga";
			string matName; fs>>matName;
			do {
				fs >> tok;
				printf("mtl tok=%s\n",tok.c_str());
				if (tok=="map_Kd") {
					fs>>texName;
					cout<<matName<<".tex="<<texName<<"\n";
					OBJMaterialDef newMat; newMat.texture_diffuse=texName;
					mlib[matName]=newMat;
				} else
				if (tok=="newmtl") goto nextMat;
				else	
					getline(fs, line);
			} while (!fs.eof());
		}
		else
			getline(fs,line);
	} while (!fs.eof());
	fs.close();
	for (auto& mt:mlib){
		printf("material[%s]: texture_diffuse=%s\n",mt.first.c_str(),mt.second.texture_diffuse.c_str());
	}
}


//uint32_t (*loadTexture)(const char* fn), const char* path
void loadOBJGeometry(MeshBuilder& mesh, MatLib& matLib, ifstream& fs,float scale)
{
	for (auto x : matLib) { cout << x.first<<":"<<x.second.texture_diffuse<<"\n";}
	cout <<"read geom{\n";

	std::map<VertexI,uint32_t> vtmap;
	int currTexSubset=0;
	do {
		char c= fs.peek();
		if (c==-1 || !c) break;
		if (c==' ' || c=='\r' || c=='\n' || c=='\t') {
			c=fs.get();
			continue;
		}
		if (c=='#') 
		{
			char comment[512];
			fs.getline(comment,512);
			continue;
		}
		std::string id;
		fs >> id;
		if (id=="v") 
		{
            Vec3 pos; fs >> pos.x; fs>>pos.y; fs>>pos.z;
			pos=pos*scale;
			mesh.positions.push_back(pos);
		} else if (id=="vt") 
		{
            Vec2 uv; fs >> uv.x >> uv.y;
			
			mesh.texcoords.push_back(uv);
		} else if (id=="vn") {
            Vec3 norm; fs >> norm.x >> norm.y >> norm.z;
			mesh.normals.push_back(norm);
		} else if (id == "s") {
			std::string str; fs>>str;
			cout << "smoothing group = "<<str<<"\n";
		} 
		else if (id=="g") 
		{
			std::string name;
			fs >> name;
			cout <<"geometry:"<<name<<" geometry within index block, is that allowwed?\n";
		} else if (id=="usemtl") {
			std::string matName;
			std::string texname="";
			fs >> matName;
			cout <<"usemat ="<< matName<<"\n";
			if (auto mt=matLib.find(matName); mt!=matLib.end()) 
			{
				cout<<"found material "<<matName<<" tex="<<mt->second.texture_diffuse<<"\n";
				texname=mt->second.texture_diffuse;
			} else 
			{
				size_t	len=matName.length();
                auto pos=matName.find_first_of(".");
                texname=matName.substr(0,pos);
                texname+=".tga";
			}
//            int	texIndex=loadTexture(texname.c_str());
			int txi=mesh.insertTexture(texname);
			currTexSubset = mesh.insertSubset(txi,txi);
			auto& sst=mesh.subsets[currTexSubset];
			printf("created subset:%s-> [%s,%s]\n", texname.c_str(),mesh.texnames[sst.textures[0]].c_str(),mesh.texnames[sst.textures[1]].c_str());
			// todo - autotexturing of the second layer using inner/outer edgeoverlay
		} else if (id=="f") {
            if (!mesh.subsets.size()){
				int txinull=mesh.insertTexture("n_water.jpg");
				currTexSubset=mesh.insertSubset(txinull,txinull);
			}
			auto& subset=mesh.subsets[currTexSubset];
            if (!mesh.texcoords.size()){
                mesh.texcoords.push_back(Vec2(0.f,0.f));
            }
            if (!mesh.normals.size()){
                mesh.normals.push_back(Vec3(0.f,0.f,1.f));
            }
            //skipWhiteSpaceInLine(fs);
			char c;
			int	n=0;
            const int MaxVtc=32;
			int	face_pos[MaxVtc];
			int	face_tex[MaxVtc];
			int	face_norm[MaxVtc];

			do {
				fs >> face_pos[n]; face_tex[n]=1;face_norm[n]=1;
				if (fs.peek()=='/')
				{
					c=fs.get();
                    fs >>face_tex[n];
					if (fs.peek()=='/')	{
						c=fs.get();
						fs>>face_norm[n];
					}
				}
                //skipWhiteSpaceInLine(fs);
				n++;
				if (n>MaxVtc) {
					cout<<"error too many poly-vertices in dotobj file\n";
					ASSERT(0 && "TOO MANY POLYVERTICES IN OBJ");
					exit(0);
				}
				
			} while (fs.peek()!='\n' && fs.peek()!='\r' && fs.peek());
			c=fs.get(); 

            // make 'IVertices'
            typedef int VertexIndex_t;
            VertexIndex_t face_vertex[MaxVtc];
            for (int k=0; k<n; k++){
				int txci=face_tex[k]-1;
                face_vertex[k]=mesh.insertVertexI(VertexI{face_pos[k]-1, 0,face_norm[k]-1, {txci,txci}});
            }

			// store tris
			for (int k=2; k<n; k++) {
                subset.triangles.push_back(
                    std::array<int,3>{{face_vertex[0],face_vertex[k],face_vertex[k-1]}}
                );
			}
	//		cout <<"read poly\n";
		}
		//cout <<"read block\n";
	} while (1);
	cout <<"read geom\n";	
}

//uint32_t (*loadTexture)(const char* filename)
MeshBuilderNodes*
ParseOBJFile(ifstream& fs,const char* dirname) {
    if (!fs.good()) return nullptr;
	MatLib	matLib;
	// todo: handle multiple objects properly
    auto scale=1.f;
    MeshBuilderNodes* mdl=new MeshBuilderNodes;

	do 
	{
        
		char c=0;
		if (fs.eof())
			break;
		fs >> c;
		if (c=='m') {
			std::string matLibFileName;
			std::string tok;
			fs>>tok>>matLibFileName;
            loadOBJMaterialLib(matLib,std::string(dirname)+matLibFileName);
		}else
		if (c=='#')
		{	char comment[512];
			fs.getline(comment,512);
		} else
		if (c=='o' || c== 'g') {
            mdl->emplace_back();
			std::string name; fs >> name;
			cout << (c=='0'?"read object ":"read geom") << name <<":-\n";
            mdl->back().name=name;
            loadOBJGeometry(mdl->back(),matLib, fs,scale);
		}
		else {

			char line[1024];
			fs.getline(line,1024);
			line [10]=0;
			printf ("loadobj: skip %s unknown\n",line);
		}
	}
	while (1);
//    scn->calculateBounds();
    return mdl;
}

#ifdef MAIN_loadobjnew
uint32_t loadTextureNull(const char* texname){return 0;}
int main(int argc, const char** argv){
    auto fs=ifstream("assets/structure0.obj");

    auto mdl=ParseOBJFile(fs,"assets/");
    return 0;
}
#endif

