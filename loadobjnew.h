#pragma once
#include <map>
#include <vector>
#include "meshbuilder.h"

#define _CRT_SECURE_NO_WARNINGS

struct OBJMaterialDef
{
	string texture_diffuse;
	// TODO - eventually read all the specular control etc.
};
typedef std::map<std::string,OBJMaterialDef> MatLib;
auto    loadOBJMaterialLib (MatLib& mlib,const string& filename)->void;

//
void loadOBJGeometry(MeshBuilder& mesh, MatLib& matLib, ifstream& fs,float scale);

//uint32_t (*loadTexture)(const char* filename)
MeshBuilderNodes* ParseOBJFile(ifstream& fs,const char* relatedDirname);

