#sdl20config should generate "-I/usr/include/SDL2 -D_REENTRANT"
run:
	g++ -DMAIN_shadertestnew $(wildcard *.cpp) -std=c++1z -fno-exceptions -Wno-shift-negative-value -lGLU -lGL -lSDL2 -lpthread -I/usr/include/SDL2 -D_REENTRANT -o gldemo -mfma -msse4
	./gldemo
