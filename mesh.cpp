#include "mesh.h"
#include "triangulate.h"
#include "drawprim.h"

Mesh*	g_pTorus;
Mesh* MeshNew(){return new Mesh();}
Mesh* MeshMakeTorus(int x,int y,float rx,float ry){return Mesh::make_torus(x,y,rx,ry);}


void
FillTorusVertices(TestVertex* dv, int numU,int numV,float rx,float ry) 
{	// TODO a grid generator (gen vertex from fractional i,j)
	int	i,j;
	float fi=0.f, fj=0.f, dfi=1.f/(float)(numU-1),  dfj=1.f/(float)(numV-1);

	
	const float pi=3.14159265,pi2=pi*2.f;
	for (fj=0.f,j=0; j<numV; j++,fj+=dfj) 
	{
		for (fi=0.f,i=0; i<numU; i++,fi+=dfi, dv++) 
		{
			int dvi=i+j*numU;
			float cx = sin(2.f*pi*fi);
			float sx = cos(2.f*pi*fi);
			float sy = sin(2.f*pi*fj);
			float cy = cos(2.f*pi*fj);
			dv->norm={(sy)*cx,(sy)*sx,cy};
			dv->pos={(rx+sy*ry)*cx, (rx+sy*ry)*sx, ry*cy};
			dv->texLayers={
					array<Half,2>({ fi*8.f,fj*2.f}),
					array<Half,2>({fi*9.1f,fj*2.2f})}; // slightly displace 2nd texture it could be rotated, anythig..
			auto outf= (sy*0.125f+(1.f-0.125f));
			float alpha=0.5f+0.5f*pow(sin(4.f*pi*fi),1.0/3.0f);
			dv->color={255,255,255, (uint8_t)(255.f*alpha)};
		}
	}
}

GLuint glGenVertexArrayAndBind(){
    GLuint id; glGenVertexArrays(1,&id);
    glBindVertexArray(id);
    return id;
}
GLuint glGenBufferAndBind(GLuint bufferType){
    GLuint id; glGenBuffers(1,&id);
    glBindBuffer(bufferType,id);
    return id;
}

Mesh* Mesh::make_torus(int	numU, int numV, float mainR,float innerR)
{
	vector<TestVertex>	vertices; vertices.resize(numU*numV);
	FillTorusVertices(&vertices[0], (IndexType)numU,(IndexType)numV, mainR,innerR);
	std::vector<IndexType>	indices; indices.resize((numV-1)*(2*numU+2));
	FillGridIndices(&indices[0],(IndexType)numU,(IndexType)numV);

	auto mesh= new Mesh(vertices,indices, GL_TRIANGLE_STRIP);
	mesh->shader=g_ShaderPrograms.tex2Blend;
	return mesh;
}

void InitMeshFromBuilder(Mesh* rmesh, const MeshBuilderNodes* meshes) {

			
	std::vector<Mesh::Subset> subsets;
	//TODO - load objects should return RENDERNODES, not raww Meshes.
	std::vector<TestVertex> vertices;
	std::vector<uint32_t> indices;
	for (auto& objm : *meshes){
		int baseVertexIndex=vertices.size();

		// gather the rendervertex components to make GPU vertices.
		for (int i=0; i<objm.vertices.size(); i++){
			const VertexI& ivertex=objm.vertices[i];
			Vec3 pos=objm.positions[ivertex.posIndex];
			Vec2 uv0=objm.texcoords[ivertex.texcoordIndex[0]];
			Vec2 uv1=objm.texcoords[ivertex.texcoordIndex[1]];
			Vec3 normal=objm.normals[ivertex.normalIndex];
			Vec4 color=(objm.colors.size())?objm.colors[ivertex.colorIndex]:Vec4(1.0f,1.0f,1.0f,1.0f);
			uint8_t r=color.x*255;
			uint8_t g=color.y*255;
			uint8_t b=color.z*255;
			uint8_t a=color.w*255;

			vertices.push_back(
				TestVertex{
					pos,
					{r,g,b,a},
					normal,
					{
						std::array<Half,2>({Half(uv0.x),Half(uv0.y)}),
						std::array<Half,2>({Half(uv1.x),Half(uv1.y)})
					}
				}
			);
		}

		// copy trilist indices inplace.
		// actually those are just sequential already..
		for (int txgroupi=0; txgroupi<objm.subsets.size(); txgroupi++){
			extern GLuint GetTextureOrLoad(const char* name);
			auto tex0 = GetTextureOrLoad(objm.texnames[objm.subsets[txgroupi].textures[0]].c_str());
			auto tex1 = GetTextureOrLoad(objm.texnames[objm.subsets[txgroupi].textures[1]].c_str());
			auto subsetBaseIndex=indices.size();
			auto& tris=objm.subsets[txgroupi].triangles;
			for (auto tri:tris){
				for (int k=0; k<3; k++){
					int rvertexi=tri[k];
					int vti=tri[k]+baseVertexIndex;
					if (rvertexi<0 || rvertexi>=vertices.size()){
						LOGE("OBJ loader - rvertex out of bounds %d/%lu %lu",rvertexi,vertices.size(),objm.vertices.size());
					}
					if(vti<0 || vti>=vertices.size()) {
						printf("vertex out of bounds %d/%lu\n",vti,vertices.size());
					}
					indices.push_back(vti);
				}
			}
			if (!tex1){tex1=tex0;}	// todo - select single,double layer modes, surely!
			subsets.emplace_back(subsetBaseIndex,indices.size()-subsetBaseIndex, tex0,tex1);
			printf("made a subset: num=%lu range=[%lu-%lu]\n",indices.size()-subsetBaseIndex, subsetBaseIndex,indices.size() );
		};
	}
	*rmesh = std::move(Mesh(vertices,indices,subsets, GL_TRIANGLES));
    rmesh->shader = g_ShaderPrograms.tex2Blend; // todo - pick it from teh blendMode and allow overide per subset
}



#ifndef IQM_OFS
#define IQM_OFS(TYPE,hdr,ofs) ((TYPE*) ( (unsigned char*)(hdr)+ (hdr)->ofs))
#endif
#ifndef __IQM_H__
struct iqmheader
{
    char magic[16]; // the string "INTERQUAKEMODEL\0", 0 terminated
    uint32_t version; // must be version 2
    uint32_t filesize;
    uint32_t flags;
    uint32_t num_text, ofs_text;
    uint32_t num_meshes, ofs_meshes;
    uint32_t num_vertexarrays, num_vertexes, ofs_vertexarrays;
    uint32_t num_triangles, ofs_triangles, ofs_adjacency;
    uint32_t num_joints, ofs_joints;
    uint32_t num_poses, ofs_poses;
    uint32_t num_anims, ofs_anims;
    uint32_t num_frames, num_framechannels, ofs_frames, ofs_bounds;
    uint32_t num_comment, ofs_comment;
    uint32_t num_extensions, ofs_extensions; // these are stored as a linked list, not as a contiguous array
};

struct iqmmesh
{
    uint32_t name;     // unique name for the mesh, if desired
    uint32_t material; // set to a name of a non-unique material or texture
    uint32_t first_vertex, num_vertexes;
    uint32_t first_triangle, num_triangles;
};

enum // vertex array type
{
    IQM_POSITION     = 0,  // float, 3
    IQM_TEXCOORD     = 1,  // float, 2
    IQM_NORMAL       = 2,  // float, 3
    IQM_TANGENT      = 3,  // float, 4
    IQM_BLENDINDEXES = 4,  // ubyte, 4
    IQM_BLENDWEIGHTS = 5,  // ubyte, 4
    IQM_COLOR        = 6,  // ubyte, 4

    // all values up to IQM_CUSTOM are reserved for future use
    // any value >= IQM_CUSTOM is interpreted as CUSTOM type
    // the value then defines an offset into the string table, where offset = value - IQM_CUSTOM
    // this must be a valid string naming the type
    IQM_CUSTOM       = 0x10
};

enum // vertex array format
{
    IQM_BYTE   = 0,
    IQM_UBYTE  = 1,
    IQM_SHORT  = 2,
    IQM_USHORT = 3,
    IQM_INT    = 4,
    IQM_UINT   = 5,
    IQM_HALF   = 6,
    IQM_FLOAT  = 7,
    IQM_DOUBLE = 8,
};

struct iqmvertexarray
{
    uint32_t type;   // type or custom name
    uint32_t flags;
    uint32_t format; // component format
    uint32_t size;   // number of components
    uint32_t offset; // offset to array of tightly packed components, with num_vertexes * size total entries
                 // offset must be aligned to max(sizeof(format), 4)
};

struct iqmtriangle
{
    uint32_t vertex[3];
};
struct iqmadjacency
{
    // each value is the index of the adjacent triangle for edge 0, 1, and 2, where ~0 (= -1) indicates no adjacent triangle
    // indexes are relative to the iqmheader.ofs_triangles array and span all meshes, where 0 is the first triangle, 1 is the second, 2 is the third, etc. 
    uint32_t triangle[3];
};

struct iqmjoint
{
    uint32_t name;
    int32_t parent; // parent < 0 means this is a root bone
    float translate[3], rotate[4], scale[3]; 
    // translate is translation <Tx, Ty, Tz>, and rotate is quaternion rotation <Qx, Qy, Qz, Qw>
    // rotation is in relative/parent local space
    // scale is pre-scaling <Sx, Sy, Sz>
    // output = (input*scale)*rotation + translation
};

struct iqmpose
{
    int32_t parent; // parent < 0 means this is a root bone
    uint32_t channelmask; // mask of which 10 channels are present for this joint pose
    float channeloffset[10], channelscale[10]; 
    // channels 0..2 are translation <Tx, Ty, Tz> and channels 3..6 are quaternion rotation <Qx, Qy, Qz, Qw>
    // rotation is in relative/parent local space
    // channels 7..9 are scale <Sx, Sy, Sz>
    // output = (input*scale)*rotation + translation
};
//ushort frames[]; // frames is a big unsigned short array where each group of framechannels components is one frame

struct iqmanim
{
    uint32_t name;
    uint32_t first_frame, num_frames; 
    float framerate;
    uint32_t flags;
};

enum // iqmanim flags
{
    IQM_LOOP = 1<<0
};

struct iqmbounds
{
    float bbmins[3], bbmaxs[3]; // the minimum and maximum coordinates of the bounding box for this animation frame
    float xyradius, radius; // the circular radius in the X-Y plane, as well as the spherical radius
};
template<typename T>
inline void read_from(T& dst, unsigned char*& ptr) {dst=*(T*)ptr; ptr+=sizeof(T);}
Mesh* LoadIQM(const char* f){
	float scaleFactor=1.0f;
	auto hdr=(iqmheader*)loadAndAllocResourceFile(f,nullptr);
	if (!hdr) return nullptr;
	LOGI("IQM header: %d %d %d", hdr->num_meshes,hdr->num_vertexes,hdr->num_triangles);
	auto va=IQM_OFS(iqmvertexarray,hdr,ofs_vertexarrays);
	// TODO- choose vertex format based on the arrays we actually have;
	std::vector<TestVertex> vertices;
	vertices.resize(hdr->num_vertexes);

	// clear vertex color to default
	for (uint32_t i=0; i<hdr->num_vertexes;i++){
		vertices[i]=TestVertex{
				{0.f,0.f,0.f},
				{255,255,255,255},
				{0.0f,0.0f,1.0f}, 
				{array<Half,2>({0.f,0.f}),array<Half,2>({0.f,0.f})}
		};
	}
	for (uint32_t j=0; j<hdr->num_vertexarrays; j++,va++) {
		LOGI("IQM va[%d] type=%d flags=%x fmt=%d size=%d ofs=%d",j,va->type,va->flags,va->format,va->size,va->offset);	
		auto src = ((unsigned char*)hdr)+va->offset;//va,offset);

		auto dvt=&vertices[0];

		for (uint32_t i=0; i<hdr->num_vertexes; i++,dvt++){
			// yuk. TODO better.
			switch (va->type){
				case IQM_POSITION: read_from(dvt->pos,src); for (int k=0; k<3;k++) {dvt->pos[k]*=scaleFactor;}; break;
				case IQM_NORMAL: read_from(dvt->norm,src); break;
				case IQM_TEXCOORD: {array<float,2> uv;read_from(uv,src); dvt->texLayers[0]={uv[0],uv[1]}; dvt->texLayers[1]=dvt->texLayers[0];} /* just duplicate 2nd texcoord*/break;
				case IQM_COLOR: read_from(dvt->color,src);dvt->color[3]=0; /*uesd for texblend*/break;
			}
		}
	}
	std::vector<uint32_t> indices;
	indices.resize(hdr->num_triangles*3);
	LOGI("len=%zu",indices.size());
	auto tri=IQM_OFS(iqmtriangle,hdr,ofs_triangles);
	uint32_t* dst=&indices[0];
	for (uint32_t i=0; i<hdr->num_triangles;i++,tri++,dst+=3){
		for (int k=0; k<3; k++){
			dst[k]=tri->vertex[k];
		}
	}
	free(hdr);
	return new Mesh(vertices,indices,GL_TRIANGLES);
	
}
#endif



Mesh* meshFromTriangulation(Triangulation* trm){
	vector<TestVertex> vertices;
	vector<uint32_t> indices;
	int i=0;
	for (auto v=trm->vertices; v;v=v->next,i++){
		if (v->id==-1)continue; //
		v->index=i;
		TestVertex vt;
		vt.pos=std::array<float,3>(v->pos);
		vt.color={255,255,255,255};
		vt.norm={0.0,0.0,1.0}; // todo - triangle normal utility code.
		float uvscale=0.1f;
		vt.texLayers[0]={v->pos.x*0.9f*uvscale,v->pos.y*0.9f*uvscale};
		vt.texLayers[1]={v->pos.x*1.2f*uvscale,v->pos.y*1.2f*uvscale};
		vertices.push_back(vt);
	}
	i=0; for (auto tri=trm->all_triangles; tri;tri=tri->next){
		if (tri->hasVertexID(0xffffffff))
			continue;
		if (!tri->hasSplit()){
			tri->index=i++;
			int swizzle[3]={0,2,1}; // todo .. which is invertex, our renderer or the triangulator
			for (int k=0; k<3; k++)
				indices.push_back(tri->vertex[swizzle[k]]->index);
		}
	}
	
	makeVertexNormals(vertices, indices);
	return new Mesh(vertices,indices,GL_TRIANGLES);
}

Mesh* makeSurfaceMesh(){
	auto trm=makeSurfaceDemo(16);
	auto mesh= meshFromTriangulation(trm);
	delete trm;
	return mesh;
}


void
RenderNode::render(RenderCtx& rc,const Matrix4& hmat, int depth,bool skipCullTest)const {
	//if (rc.shadow) skipCullTest=true;
	if (g_ShowDebugLines){
		g_DebugDraw.drawSphere(0xff00ffff-depth*0x004040, hmat*this->bounds.centre,this->bounds.radius);
	}
	if (!skipCullTest) {
		auto cull=rc.cv.cull_test_sphere_at(hmat*this->bounds.centre,this->bounds.radius,1.0f);
		if (cull==CullResult::INVIS)
			return;
		if (cull==CullResult::ALL_VIS)
			skipCullTest=true;
	}

    Matrix4 mt=hmat*this->matrix;
	if (auto mesh=this->pMesh) {
		RenderMeshAt(rc,this->pMesh, mt);
	}
	
	this->for_each_child([&](int,auto& n){
		n.render(rc,mt, depth++,skipCullTest);
	});
}

void	SetLights(const RenderCtx& rc);


extern void RenderMeshAt(const Mesh* msh, const Matrix4& mt);

void
RenderNode::buildTree(int maxPerNode) {
	// get bounds of the whole thing.
	Extents4f centreBounds; centreBounds.init(); // bb of CENTRES, not actual objects - because we split by centre.


	this->for_each_child([&](int,auto& c){
		centreBounds.include(c.bounds.centre);
	});
	this->bounds.centre=centreBounds.centre();

	float maxd=0.f;
	this->for_each_child([&](int,auto& c){
		float d=this->bounds.centre.dist(c.bounds.centre)+c.bounds.radius;
		updatemax(maxd,d);
	});
	this->bounds.radius=maxd;
	if(this->num_children()<maxPerNode)
		return;

	// pick the longest axis.. divide
	Vec4 size=centreBounds.size();
	int	 axisu=0,axisv=-1;

	// quadtree splits but pick the best axes.
	// 6 cases. split xy, xz xz
	// 'faceArea.xyz' = area of the bounding box faces perp to x,y,z axes..
	// eg. box size(1,2,10) has areas {20,10,2}

	// 1st split axis = longest side..
	// if any single axis is 2x length of the next, only split by that one.
	if (size.x > size.y && size.x > size.z){
		axisu=0;
		axisv = size.y>size.z?1:2;
	}
	else if (size.y > size.x && size.y > size.z){
		axisu=1;
		axisv = size.x>size.z?0:2;
	}
	else{
		axisu=2;
		axisv = size.x>size.y?0:1;
	}
	

	if (size[axisu]<0.00001f) {
		printf("zero vol node unsplittable %d ch\n",this->num_children());
		// TODO- we might still split by states, primitive sizes, ...
		return;
	}

	// if any one axis is "much longer (2x)" only split by that; else split into 4 quadtree style nodes
	int numSplit=(size[axisu]>size[axisv]*2.f)?2:4;
	if (numSplit==2){
		printf("node %.3f %.3f %.3f split only by %d\n",size.x,size.y,size.z,axisu);
	}
	
	RenderNode* sub[4]={nullptr,nullptr,nullptr,nullptr};
	while (auto sn=this->pop_child()) {
		auto ofs=sn->bounds.centre.sub(this->bounds.centre);
		int bucket = ofs[axisu]>0.0f?1:0;
		if (numSplit==4) {
			bucket += ofs[axisv]>0.0f?2:0;
		};
		RenderNode** pp=&sub[bucket]; // pick bucket.
		sn->next=*pp;
		*pp=sn;
	}
	
	for (int i=0; i<numSplit; i++) {
		if (auto *sn=sub[i]) {
			// todo - collect child splits here , to make more than 2 nodes per split.
			auto *newn= new RenderNode;
			newn->child=sn; sub[i]=nullptr;
			newn->pMesh=nullptr;
			this->push_child(newn);
			newn->buildTree(maxPerNode);
		}
	}

	printf("splitting done - numNodes=%d\n",num_children());
}




extern float g_angle;
void RenderMeshAt(RenderCtx& rc,const Mesh* msh,const Matrix4& mt){
    rc.changeShader(msh->shader);
    glUseProgram(msh->shader);
    rc.setLightShaderConstants(mt, msh->sphere);
    SetUniform(uMatProj,rc.cv.proj_view);
	SetUniform(uMatModelView, mt);
	SetUniform(uAnim, Vec4(g_angle,0.f,0.f,0.f));

	glBindVertexArray(msh->vao);
	if (msh->subsets.size()) {
        // a *textured* mesh comes in with subsets that define textures.
        // todo- select shader here aswell, use rendercontext.
		int	t=0;
		for (auto& ss:msh->subsets){
			glSetTextureLayer(1,ss.tex1);
			glSetTextureLayer(0,ss.tex0);
			glDrawElements(msh->primType, ss.numIndices, GL_UNSIGNED_INT,(void*)(sizeof(GLuint)*ss.firstIndex));
			if (g_ShowDebugLines)
				g_DebugDraw.drawSphere(0xff00ffff,mt*ss.sphere.centre,ss.sphere.radius);
			t++;
		}
		
	} else {
        //rc.changeShader(this->shaderProg[rc.pass]);
        if (msh->transparent && !rc.transparent) {
            // TODO - sort..
            rc.transparencies.push_back(DrawReq(msh,mt));
        } else // opaque pass.
        {

            rc.changeShader(msh->shader);
            glSetTextureLayer(0,msh->tex0);
            glSetTextureLayer(1,msh->tex1);
            // an *untextured* mesh just inherits whatever textures are set by the caller
		    glDrawElements(msh->primType, msh->numIndices, GL_UNSIGNED_INT,0);
        }
    }
}

void	RenderMeshTextureOveride(RenderCtx& rc, const Mesh* msh,const Matrix4& mat,int t0,int t1) 
{	
	int	i;
    rc.changeShader(msh->shader);
	glSetTextureLayer(0, t0);
	glSetTextureLayer(1, t1);
	glBindVertexArray(msh->vao);
    rc.changeShader(msh->shader);
    SetUniform(uMatModelView,mat);
	glDrawElements(msh->primType, msh->numIndices, GL_UNSIGNED_INT,0);
}

Mesh*	LoadMesh(const char* filename){
	extern Mesh* LoadOBJAsync(const char* filename);
	auto ext=getFilenameExt(filename);
	if (cmpupr(ext,"IQM")) {
		return LoadIQM(ext);
	} else if (cmpupr(ext,"OBJ")){
		return LoadOBJAsync(filename);
		//LoadOBJ(ext);
	} else {
		LOGE("unknown mesh file format %s",filename);
		return nullptr;
	}
}


#include <fstream>
Mesh* LoadOBJNow(const char* fname){
	std::string pathname;
	pathname="assets/";
	pathname+=fname;

	ifstream fs(pathname,  std::ifstream::in);

	bool mergemeshes=true;
	MeshBuilderNodes* srcmeshes=ParseOBJFile(fs,"assets/");
	if (!srcmeshes)
		return nullptr;
	Mesh* msh=new Mesh();	 // empty mesh.
	InitMeshFromBuilder(msh,srcmeshes);
	delete srcmeshes;
	return msh;
}
