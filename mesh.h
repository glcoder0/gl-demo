#pragma once
#include "vecmathnew.h"
#include "textures.h"
#include "shaders.h"
#include "loadobjnew.h"
extern int g_ShowDebugLines;

GLuint glGenVertexArrayAndBind();
GLuint glGenBufferAndBind(GLuint typeOfTarget);
extern Mesh*	g_pTorus;


// Template assists for setting up vertex attributes matching struct fields
// overloads get the element type for multi-d attrib fields, either as std::array<> or TVec<>
// template/overload stuff to get vertex layout from struct/struct field pointers.
inline GLuint glTypeOf(const float& f){return GL_FLOAT;}
inline GLuint glTypeOf(const Half& f){return GL_HALF_FLOAT;}
inline GLuint glTypeOf(const uint8_t& f){return GL_UNSIGNED_BYTE;}
inline GLuint glTypeOf(const int8_t& f){return GL_BYTE;}
inline GLuint glTypeOf(const uint32_t& f){return GL_UNSIGNED_INT;}
inline GLuint glTypeOf(const uint16_t& f){return GL_UNSIGNED_SHORT;}
inline GLuint glTypeOf(const int32_t& f){return GL_INT;}
inline GLuint glTypeOf(const int16_t& f){return GL_SHORT;}
template<typename V,typename T,size_t N> void glSetAttribPointer(GLuint attr_index, const std::array<T,N> V::*pElem){
	V* dummy_vertex=(V*)nullptr;
	if (N>0) {	// disable anything by using array<T,0> ?
		glEnableVertexAttribArray(attr_index);
		glVertexAttribPointer(attr_index, N, glTypeOf((const T&)0), GL_FALSE,	sizeof(V), (void*) &(((V*)nullptr)->*pElem));
	}
}
template<typename V,typename T>
void glSetAttribPointer(GLuint attr_index, const TVec2<T> V::*pElem){
	glEnableVertexAttribArray(attr_index);
	glVertexAttribPointer(attr_index, 2, glTypeOf((const T&)0), GL_FALSE,	sizeof(V), (void*) &(((V*)nullptr)->*pElem));
}
template<typename V,typename T>
void glSetAttribPointer(GLuint attr_index, const TVec3<T> V::*pElem){
	glEnableVertexAttribArray(attr_index);
	glVertexAttribPointer(attr_index, 3, glTypeOf((const T&)0), GL_FALSE,	sizeof(V), (void*) &(((V*)nullptr)->*pElem));
}
template<typename V,typename T>
void glSetAttribPointer(GLuint attr_index, const TVec4<T> V::*pElem){
	glEnableVertexAttribArray(attr_index);
	glVertexAttribPointer(attr_index, 4, glTypeOf((const T&)0), GL_FALSE,	sizeof(V), (void*) &(((V*)nullptr)->*pElem));
}

// 2d element array, elems assumed consecutive
template<typename V,typename T,size_t N,size_t M> void glSetAttribPointer(GLuint attr_index, const std::array<std::array<T,N>,M> V::*pElem){
	V* dummy_vertex=(V*)nullptr;
	auto& elem=dummy_vertex->*pElem;
	for (int i=0; i<M; i++){
		glEnableVertexAttribArray(attr_index+i);
		glVertexAttribPointer(attr_index+i, N, glTypeOf(elem[i][0]), GL_FALSE,	sizeof(V), (void*) &elem[i]);

	}
}


template<typename VERTEX>
inline void glSetVertexAttribPointers(){
	glSetAttribPointer(VertexAttrIndex_pos,&VERTEX::pos);
	glSetAttribPointer(VertexAttrIndex_color,&VERTEX::color);
	glSetAttribPointer(VertexAttrIndex_tex0,&VERTEX::texLayers);// overload sets uv layers.
	glSetAttribPointer(VertexAttrIndex_norm,&VERTEX::norm);
}


template<>
inline void glSetVertexAttribPointers<VertexC>(){
	glSetAttribPointer(VertexAttrIndex_pos,&VertexC::pos);
	glSetAttribPointer(VertexAttrIndex_color,&VertexC::color);
}

template<>
inline void glSetVertexAttribPointers<VertexCT>(){
	glSetAttribPointer(VertexAttrIndex_pos,&VertexCT::pos);
	glSetAttribPointer(VertexAttrIndex_color,&VertexCT::color);
	glSetAttribPointer(VertexAttrIndex_tex0,&VertexCT::tex);
};
template<>
inline void glSetVertexAttribPointers<VertexCT2>(){
	glSetAttribPointer(VertexAttrIndex_pos,&VertexCT2::pos);
	glSetAttribPointer(VertexAttrIndex_color,&VertexCT2::color);
	glSetAttribPointer(VertexAttrIndex_tex0,&VertexCT2::tex0);
	glSetAttribPointer(VertexAttrIndex_tex1,&VertexCT2::tex1);
};
struct RenderCtx;
template<typename T> class HrcItem{
// TODO - optimized state where it copies everything into an array pointed by 'child'.
public:
	T* next=nullptr; T*child=nullptr;
	void push_child(T* newobj){
		newobj->next=child;child=newobj;
	}
	T* pop_child(){
		if (T* obj=child){child=obj->next;obj->next=nullptr;return obj;}
		else return nullptr;
	}
	int num_children()const{int i=0;for (auto* o=child; o;o=o->next){i++;}; return i;}
	void reverse_children(){
		T* lst=nullptr; 
		while (auto *o=pop_child()){
			o->next=lst; 
			lst=o;
		};
		child=lst;
	}
	~HrcItem(){
		while (auto*p=pop_child()){
			delete p;
		}
	}
	// todo - make iterors aswell? i prefer foreach though..
	template<class F> void for_each_child(F f){
		int i=0;for (auto*p=child; p; p=p->next){
			f(i,*p);
		}
	}
	template<class F> void for_each_child(F f) const{
		int i=0;for (auto*p=child; p; p=p->next){
			f(i,*p);
		}
	}
};


struct Mesh {			// TODO - bounding box information, mterial organization,optional CPU copy,..
	typedef uint32_t IndexType;
	enum {IndexSize = sizeof(IndexType) };
	struct Subset{
		// todo - string ID,bounding box?
		int32_t firstIndex,numIndices;
		GLuint tex0,tex1;
		Sphere sphere;
		Subset(int first,int count,int t0,int t1):firstIndex(first),numIndices(count),tex0(t0),tex1(t1){
			LOGI("SS init %d-%d, %d\n",firstIndex,numIndices,firstIndex+numIndices);
		}
	};
	Extents4f bounding_box;
	Sphere	sphere;	// might be off-centre.
	Mesh() {};
	typedef	::TestVertex Vertex;
	// refcount for all buffers sharing the same data. we can copy and retexture.
	// TODO individual vbos,ibos because we may copy different parts.
	// its' really a "pSharedVAO" but the handle is here to avoid an indirection.
	// TODO - dual vertex arrays so we can copy one part and override the other.
	int32_t* pBufferRefCount=nullptr;	
	// TODO could just be index of refcount. 
	// TODO can the GL object indices be counted on for this purpose?
	// e.g.   g_GLObjectRefCounts[this->vao] g_GLObjectRefCounts[texid]  etc..

	GLuint vao=0;
    bool transparent=false;
	GLuint	vbo=0;
	GLuint	ibo=0;
	int	numVertices=0,numIndices=0;
	uint8_t vertexSize=0;
	GLuint shader = 0;	// TODO: maybe replace with 'rendermode' index, selectable per pass
	GLuint primType=0;
    GLuint tex0=0,tex1=0;
	std::vector<Subset> subsets;
	void releaseBuffers(){
		if (pBufferRefCount){
			if (*--pBufferRefCount <=0) {
				glDeleteVertexArrays(1,&vao); glDeleteBuffers(1,&vbo); glDeleteBuffers(1,&ibo);
				vao=0;vbo=0;ibo=0;
			}
		}
	}
	Mesh& operator=(Mesh&& src) //vertexSize(src.vertexSize),bounding_box(std::move(src.bounding_box)),sphere(std::move(src.sphere)),vao(src.vao),vbo(src.vbo),ibo(src.ibo),numVertices(src.numVertices),numIndices(src.numIndices),program(src.program),subsets(std::move(src.subset))
	{
		releaseBuffers();
		vertexSize=(src.vertexSize),bounding_box=(std::move(src.bounding_box)),
		sphere=(std::move(src.sphere)),vao=(src.vao),vbo=(src.vbo),ibo=(src.ibo),
		numVertices=(src.numVertices),numIndices=(src.numIndices),
		shader=(src.shader),subsets=(std::move(src.subsets));
		primType=std::move(src.primType);
		
		src.vao=0;
		src.vbo=0;
		src.ibo=0;
		
		printf("renderable mesh move constructor\n");
		return *this;
	}

	Mesh(Mesh& src) //vertexSize(src.vertexSize),bounding_box(std::move(src.bounding_box)),sphere(std::move(src.sphere)),vao(src.vao),vbo(src.vbo),ibo(src.ibo),numVertices(src.numVertices),numIndices(src.numIndices),program(src.program),subsets(std::move(src.subset))
	{	vertexSize=src.vertexSize,bounding_box=src.bounding_box,
		sphere=src.sphere,vao=src.vao,vbo=src.vbo,ibo=src.ibo,
		numVertices=src.numVertices,numIndices=src.numIndices,
		shader=src.shader,subsets=src.subsets;
		primType=src.primType;
		// add reference to underlying buffers.
		pBufferRefCount=src.pBufferRefCount;
		(*pBufferRefCount)++;
		printf("renderable mesh copy constructor\n");
	}


//	(const Mesh& src){printf("error only move construction supported atm.\n");}
	// todo -tree of sub=culls

	template<typename V,typename INDEX>
	void init_bounds(const V*vertices,int nv,const INDEX* indices,int ni) {
		calc_bounds_subset(vertices,indices,ni,&this->bounding_box,&this->sphere);
	}
	// will support single meshes being split into renderable subsets
	template<typename V,typename INDEX>
	void calc_bounds_subset(const V* vertices, const INDEX* indices,int numIndices,Extents4f* oobb,Sphere* sph);

	static Mesh* make_torus(int usize,int vsize,float mainR,float innerR);

	template<typename VERTEX>
	inline Mesh(const std::vector<VERTEX>& vertices,const std::vector<IndexType>& indices,GLuint _primType=GL_TRIANGLES):
		Mesh(&vertices[0],vertices.size(),&indices[0],indices.size(),_primType)
	{}

	template<typename VERTEX>
	inline Mesh(const std::vector<VERTEX>& vertices,const std::vector<IndexType>& indices,std::vector<Subset>& _ss, GLuint _primType=GL_TRIANGLES):
		Mesh(&vertices[0],vertices.size(),&indices[0],indices.size(),_primType)
	{
		printf("adding subsets %lu to %lu\n",_ss.size(),subsets.size());
		for (int i=0; i<_ss.size(); i++){
			this->subsets.push_back(_ss[i]);
		}
		//this->subsets=_ss;
		for (auto& ss:subsets){
			ss.sphere.centre=Vec4(0.f,0.f,0.f,1.f);
			ss.sphere.radius=-1.f;
			for (int i=0; i<ss.numIndices; i++){
				auto vti=indices[ss.firstIndex+i];
				auto& v=vertices[vti];
				if (vti>=0 && vti<vertices.size()) {
					// huh??
					ss.sphere.includePoint(Vec4(v.pos,1.f));
				} else {

					printf("ERROR %x %d/%lu %d/subset[%d-%d]/%lu\n",vti,vti,vertices.size(),i,ss.firstIndex,ss.firstIndex+ss.numIndices,indices.size());
				}
			}
		}
	}

	template<typename VERTEX>
	Mesh(const VERTEX* vertices, int num_vertices,const IndexType* indices,int num_indices,GLuint _primType=GL_TRIANGLES) {
		this->load_from_arrays(vertices,num_vertices,indices,num_indices,_primType);
	}
	template<typename VERTEX>
	void load_from_arrays(const VERTEX* vertices, int num_vertices,const IndexType* indices,int num_indices,GLuint _primType=GL_TRIANGLES);
	template<typename VERTEX>
	static Mesh* make_grid_from_fn(int numU,int numV,std::function<VERTEX(int,int)> genHt);
	// retrieve CPU view of data.
	template<typename VERTEX>
	void get_arrays(std::vector<VERTEX>& verticesOut,std::vector<IndexType>& indices);
	//static Mesh* make_grid_from_heights_wrap(float* ht, int numU,int numV,float cellxsize,float cellysize);
	
};

template<typename INDEX>
void FillGridIndices(INDEX* indices, INDEX numU,INDEX numV){
	
		// per strip..
	INDEX	dvi=0,vi=0;
	bool flip=false;
	for (INDEX j=0; j<(numV-1); j++) 
	{
		
		// per quad
		int dvistart=dvi++;
		for (INDEX i=0; i<numU; i++) 
		{
			int i0=j*numU + i; int i1= (j+1)*numU + i;
			indices[dvi++] = (flip)?i0:i1;
			indices[dvi++] = (flip)?i1:i0;
		}
		indices[dvistart]=indices[dvistart+1];
		auto last_index=indices[dvi-1];
		indices[dvi++]= last_index;	// end with degen
		//flip^=true;
	}
}

template<typename VERTEX>
Mesh* Mesh::make_grid_from_fn(int numU,int numV, std::function<VERTEX(int,int)> makeVertex){
	vector<VERTEX>	vertices; vertices.resize(numU*numV);
	int	i,j;
	vertices.resize(numU*numV);
	for (j=0; j<numV; j++){
		for (i=0; i<numU;i++){
			vertices[i+j*numU]=makeVertex(i,j);
		}
	}

	std::vector<IndexType>	indices; indices.resize((numV-1)*(2*numU+2));
	FillGridIndices(&indices[0],(IndexType)numU,(IndexType)numV);
	return new Mesh(vertices,indices, GL_TRIANGLE_STRIP);
}

Mesh* MeshNew();
Mesh* MeshMakeTorus(int x,int y,float rx,float ry);


// write normals onto vertices, using the given index list to read vertex.pos, write vertex.normal.
// assumes members 'pos','norm' are present on 'V'
template<typename V,typename INDEX>
void makeVertexNormalsInArrays(V* vertices,int nVertices, const INDEX* indices,int nIndices, bool strips=false){
	std::vector<Vec3f> normals;
	normals.resize(nVertices,Vec3f(0.f));

	// todo - smmoothing..
	// todo - if strips, handle the swap and ignore degenerate-tris.
	for (int i=0; i<nIndices; i+=3){
		int vi0=indices[i+0];
		int vi1=indices[i+1];
		int vi2=indices[i+2];
		if (vi0==vi1 || vi1==vi2 || vi0==vi2)
			continue; // ignore degenerate triangles.
		auto n=triangleNormal(
				Vec3f(vertices[vi0].pos),  

				Vec3f(vertices[vi1].pos),
				Vec3f(vertices[vi2].pos)
				);
		if (strips && (i&1)) {n=-n;}	// mesh flips for triangle-strips.
		normals[vi0]+=n;
		normals[vi1]+=n;
		normals[vi2]+=n;
	}
	for (auto& n:normals) {n.normalize();}

	for (int i=0; i<nVertices; i++){
		vertices[i].norm=normals[i];
	}
}
template<typename V,typename INDEX>
void makeVertexNormals(std::vector<V>& vertices,const std::vector<INDEX>& indices, bool strips=false){
	makeVertexNormalsInArrays(&vertices[0],vertices.size(),&indices[0],indices.size(),strips);
}
#define SH_LOG printf
template<typename T>
void LoadBuffer(GLuint bufferType,GLuint bufferId, const T* data, int numElems){
	size_t nBytes=sizeof(T)*numElems;
    SH_LOG("buffer loaded %d size=%lu\n",bufferId, nBytes);
	glBindBuffer(bufferType,bufferId);
	glBufferData(bufferType,sizeof(T)*numElems,(void*) data, GL_STATIC_DRAW);
    auto err=glGetError();
    if (err!=GL_NO_ERROR) {
        if (err==GL_OUT_OF_MEMORY){
            SH_LOG("out of memory attempting to alloc %lu bytes\n",nBytes);
        }
        SH_LOG("gl error=%d",err);
    }
}

template<typename T>
auto CreateBuffer(const T* data,size_t num_elems,  int bufferType)->int {
	GLuint	id;
	glGenBuffers(1,&id);
	LoadBuffer(bufferType,id,data,num_elems);
	return	id;
}
template<typename T>
int CreateBuffer(std::vector<T>& src,int bufferType) {return CreateBuffer(&src[0],src[0].size(),bufferType);}




template<typename VERTEX,typename INDEX>
void Mesh::calc_bounds_subset(const VERTEX* vertices,const INDEX* indices, int num_indices, Extents4f* bounding_box,Sphere* sphere){
	bounding_box->init();
	
	auto sv=vertices;

	for (int i=0; i<num_indices; i++,sv++){
		sv=&vertices[indices[i]];
		bounding_box->include(Vec4(sv->pos,1.f));
	}
	sphere->centre=bounding_box->centre().xyz1();
	sphere->radius=0.f;
	float maxr2=0.f;
	
	for (int i=0; i<num_indices; i++,sv++){
		sv=&vertices[indices[i]];
		auto pos=Vec4(sv->pos,1.f);
		float r2=(pos-sphere->centre).lengthSquared();
		maxr2 = max(maxr2,r2);
	}
	sphere->radius = sqrt(maxr2);
	bounding_box->dump();
}





template<typename VERTEX>
void Mesh::load_from_arrays(const VERTEX* vertices,int numVertices,const uint32_t* indices,int numIndices,GLuint primType){
	this->init_bounds(vertices,numVertices,indices,numIndices);	// wretched C++ lib doesn't have ranges.
	this->numVertices=numVertices;
	this->numIndices=numIndices;

	if (!this->vbo ||!this->ibo ||!this->vao){
	    this->vao=glGenVertexArrayAndBind();
		this->pBufferRefCount = new int32_t;
		*this->pBufferRefCount=1;
		this->vertexSize = sizeof(VERTEX);
		// create and load in one step..
		this->ibo =glGenBufferAndBind(GL_ELEMENT_ARRAY_BUFFER);//CreateBuffer(indices,numIndices, GL_ELEMENT_ARRAY_BUFFER);
		this->vbo =glGenBufferAndBind(GL_ARRAY_BUFFER); //CreateBuffer(vertices,numVertices, GL_ARRAY_BUFFER);
	}
	glBindVertexArray(this->vao);
		// reload existing buffers.
	LoadBuffer(GL_ELEMENT_ARRAY_BUFFER,this->ibo,indices,numIndices);
	LoadBuffer(GL_ARRAY_BUFFER,this->vbo,vertices,numVertices);

	this->vertexSize=sizeof(VERTEX);
	// TODO - supposed to be able to cache all that here.
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,this->ibo);
	glBindBuffer(GL_ARRAY_BUFFER,this->vbo);

	// uses overloads to set the vertex offsets. If num components=0, a channel is ignored.
	glSetVertexAttribPointers<VERTEX>();

    glBindVertexArray(0);
	this->primType=primType;
}

struct RenderNode :HrcItem<RenderNode>{
	Sphere bounds;	// boundingsphere, might not be centred.
	Matrix4	matrix=Matrix4::identity(); // todo - pmatrix or matrix index.
	Mesh*	pMesh=nullptr;			// content (find the rest if it's visible)
	// material overide. TODO-place this in an optional struct.
	float drawDistScale=1.f;	// small details allowed to cull early. TODO fade em in.
	inline Vec4 pos()const {return matrix.aw;}
	void buildTree(int maxPerNode=16);
	void render(RenderCtx& c,const Matrix4& hmat, int depth=0, bool skipCullTest=false)const;
	RenderNode(){
	};
	RenderNode(Mesh* mesh, const Matrix4& mat) {
		this->pMesh=mesh;
		this->bounds.centre = mat * mesh->sphere.centre;
		this->bounds.radius = mesh->sphere.radius * mat.maxAxisLength();
		this->matrix = mat;
		//this->tex0=t0;this->tex1=t1;
		// setup with 2 textures, default is this
		
	}
};

Mesh* LoadIQM(const char* f);
struct Triangulation;
Mesh* meshFromTriangulation(Triangulation* trm);
Mesh* makeSurfaceMesh();
void InitMeshFromBuilder(Mesh* rmesh, const MeshBuilderNodes* meshes);
void RenderMeshAt(RenderCtx& cx,const Mesh* msh,const Matrix4& mt);
void RenderMeshTextureOveride(RenderCtx& cx,const Mesh* msh,const Matrix4& m, int t0,int t1);
Mesh*	LoadMesh(const char* filename);
Mesh* LoadOBJNow(const char* fname);

