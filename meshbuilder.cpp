#include "vecmathnew.h"
#include "meshbuilder.h"

int MeshBuilder::insertVertex(Vec3 pos,Vec4 color,Vec3 normal, Vec2 tex0, Vec2 tex1) {
	VertexI vt;
	vt.posIndex = find_or_insert(pos, findposition,positions);
	vt.colorIndex = find_or_insert(color, findcolor,colors);
	vt.normalIndex = find_or_insert(normal, findnormal,normals);
	vt.texcoordIndex[0] = find_or_insert(tex0, findtexcoord,texcoords);
	vt.texcoordIndex[1] = find_or_insert(tex1, findtexcoord,texcoords);
	return insertVertexI(vt);
}
int MeshBuilder::insertVertexI(const VertexI& vt){
	return find_or_insert(vt, findvertex,vertices);
}
int MeshBuilder::insertTexture(const std::string& texname){
	return find_or_insert(texname,findtexture,texnames);
}
int MeshBuilder::insertSubset(int baseTex,int overlayTex){
	// is a little different.
	// subsets contain triangles, so the 'insert' process
	auto texpair=std::array<int,2>{{baseTex,overlayTex}};
	return find_or_insert(texpair, findsubset, subsets);
	/*
	auto found=findsubset.find(texpair);
	if (found==findsubset.end()){
		auto& newss=subsets.emplace_back();
		newss.textures=texpair;
		newss.blendMode=BlendMode::VertexAlpha;
		return subsets.size()-1;
	} else {
		return found->second;
	}
	*/
}

void MeshBuilder::insertTriangleTex2(VertexIndex v0,VertexIndex v1,VertexIndex v2, TextureIndex t0,TextureIndex t1){
	int ssi=insertSubset(t0,t1);
	Subset& ss=this->subsets[ssi];
	auto tri=std::array<int,3>{{v0,v1,v2}};
	ss.triangles.emplace_back(tri);
}

// free up memory after building stuff
void MeshBuilder::releaseMaps() {
    this->findcolor.clear();
    this->findnormal.clear();
    this->findposition.clear();
    this->findtexture.clear();
    this->findsubset.clear();
    this->findvertex.clear();
}