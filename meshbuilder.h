#pragma once

#include "vecmathnew.h"

// shading mode enumeration 


// for working with a paired map & items
template<typename T,typename T_HOLDER=T /* Y must be initializable from T*/>
inline int find_or_insert(const T& item, std::map<T,int>& finder, std::vector<T_HOLDER>& items){
	if (auto found=finder.find(item);found!=finder.end()){
		return found->second;
	} else {
		int id=(int)items.size();
		items.emplace_back(item);
		finder[item]=id;
		return id;
	}
}

// intermedite form of mesh - not renderable.
// superset of fileformats and renderables
// exach "VertexI" corresponds to a finalised vertex, but 
// the components are stored index in local position,texcoord,normals arrays.
// Handy intermediate for mesh processing.
// support 2 texture layers by default, usually one is ignored.
struct VertexI {
	int posIndex;int colorIndex;int normalIndex;int texcoordIndex[2];
	uint64_t cmpVal()const{
	// 64bits:   24bit pos, 24bit normal, 
	// todo .. this isn't going to cut it. we need propper comparison that includes all.
		return ((uint64_t)posIndex<<48)+((uint64_t)colorIndex)+(((uint64_t)normalIndex)<<24)+((uint64_t)texcoordIndex[0])+((uint64_t)texcoordIndex[1]);
	}
};

// intermediate mesh representation between engine vertices and fileformat vertices
// contains hash tables for efficient construction &texture sorting 
struct MeshBuilder {
    // most are 'Todo' , just leaving an open vocabulary

    std::string name;
    std::vector<Vec3>   positions;  
	std::vector<Vec4>	colors;
    std::vector<Vec3>   normals;
    std::vector<Vec2>   texcoords;

    std::vector<VertexI>    vertices;   // indexes the above arrays 

	std::vector<std::string>	texnames;

	typedef int TextureIndex;
	typedef int VertexIndex;
	typedef int SubsetIndex;
	typedef std::array<VertexIndex,3> Triangle;
	struct Subset{
		std::string name;
		std::vector<Triangle> triangles;
		std::array<TextureIndex,2> textures;
		ShadingMode	mode=ShadingMode::VertexAlpha;
		Subset(){}
		Subset(const std::array<TextureIndex,2>& texpair):textures(texpair){};
	};
	std::vector<Subset> subsets; // subsets contain the actual triangles per texture
	VertexIndex insertVertex(Vec3 pos,Vec4 color,Vec3 normal, Vec2 tex0, Vec2 tex1);
	VertexIndex insertVertexI(const VertexI& vt);
	TextureIndex insertTexture(const std::string& texname);
	void insertTriangleTex2(VertexIndex i0,VertexIndex i1,VertexIndex i2,TextureIndex tex0, TextureIndex tex1);
	SubsetIndex insertSubset(TextureIndex baseTexture,TextureIndex overlayTexture); //
    void releaseMaps();
private:
	// only valid if constructed through the 'add???' interface, not for general consumption
	// this is doubling up as a meshbuilder.
	std::map<string,int> findObjectName;
	std::map<Vec3,int> findposition;
	std::map<Vec4,int> findcolor;
	std::map<Vec3,int> findnormal;
	std::map<Vec2,int> findtexcoord;
	std::map<VertexI,VertexIndex> findvertex;
	std::map<string,TextureIndex> findtexture;
	std::map<std::array<int,2>,int> findsubset;
};
typedef std::vector<MeshBuilder> MeshBuilderNodes;// TODO replace with interim hrc. this grew out of OBJ reader which is just flat list.

inline bool operator<(const std::array<int,2>& a,const std::array<int,2>& b){
	return ((a[0]<<16)+a[1]) < ((b[0]<<16)+b[1]);
}
inline bool operator<(const std::array<int,4>& a,const std::array<int,4>& b){
	// its just a hash really.
	return ((a[0]<<16)+a[2]) < ((b[0]<<16)+b[2]);
}

inline bool operator<(const VertexI& a,const VertexI& b){
	return (a.cmpVal()<b.cmpVal());
}
