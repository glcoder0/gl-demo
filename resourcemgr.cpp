#include "resourcemgr.h"

ASyncLoader g_AsyncLoader;

GLuint LoadTextureAsync(const char* fname){
	printf("issuing async load req %s\n",fname);
	return g_AsyncLoader.load_texture(fname);
}

Mesh* LoadOBJAsync(const char* fname){
	return g_AsyncLoader.load_obj_mesh(fname);
}

extern GLuint g_TextureArray;// todo legacy, eliminate.let ResourceMgr manage a texarray 
ResourceMgr<GLuint,LoadTextureAsync> g_Textures;
ResourceMgr<Mesh*,LoadMesh>	g_Meshes;

// "" or nullptr return 'no texture'.
GLuint GetTextureOrLoad(const char* name){
	if (!name) return 0;
	if (strlen(name)){
		return g_Textures.get(name);
	} 
	else return 0;
}



QueueSync<int> foo;
void* threadfunc(void*){
	printf("hello from thread\n");
	while (1) {
		// grrr . this is so much better in Rust
		decltype(foo.pop()) tmp;
		while ((tmp=foo.pop())){
			printf("thread found %d\n",*tmp);
		}
		printf("thread waiting\n");
		this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	return nullptr;
}
void test_queue() {
	foo.push(10);
	std::thread task(threadfunc,nullptr);
//	pthread_create(&task,nullptr, threadfunc,nullptr);
	this_thread::sleep_for(std::chrono::milliseconds(1000));
	foo.push(0); foo.push(1); foo.push(2);
	while (1){ printf("pushde to thread\n");foo.push(rand());		this_thread::sleep_for(std::chrono::milliseconds(000));
;}
	return ;
}
