#pragma once
#include "textures.h"
#include "mesh.h"
#include "loadobjnew.h"
#include <tuple>
#include <optional>
#include <iostream>
#include <fstream>      // std::filebuf
#include <deque>      // std::filebuf
#define ASSERT_GL_THREAD /*todo .. tls?*/

#include <thread>
#include <chrono>
#include <mutex>
#include <optional>
template<typename T>
class QueueSync {
	std::deque<T>	q;
	std::mutex m;
public:
	QueueSync() : m(){}

	void push(const T&& val) {
		m.lock();
		
		this->q.push_front(std::move(val));
		m.unlock();
	}

	std::optional<T> pop() {
		// better in rust :(
		m.lock();
		std::optional<T> val;
		if (q.size()) {
			val = std::move(q.back());
			q.pop_back();
		}
		m.unlock();
		return val;
	}
};

typedef std::vector<MeshBuilder>* (*MeshLoaderFunc_t)(ifstream& srcFile,const char* relatedAssetPath);
struct ASyncLoader {
	// todo -abstract out async loader items?
	// TODO   - dynamic prioritizing
	// TODO - decompresssion threads
	// TODO - out-of-order arival of web resources.


	typedef void*(*threadfunc_t)(void*);
	QueueSync<pair<GLuint,std::string>> requests;
	struct TexData{GLuint texid;std::string filename; uint8_t* buffer;Vec2i size; int bytepp;};
	QueueSync<TexData> interim_image_q;
	// TODO- change to abstract resource/inintialiser? types.. handle vs object pointer, kinda like stronger types here.
	struct MeshReq{Mesh* meshToInit;std::string filename;MeshLoaderFunc_t loader;};
	QueueSync<MeshReq> meshreq;
	struct MeshData{Mesh* mesh;std::string filename; MeshBuilderNodes* meshbuilder;};
	QueueSync<MeshData> interim_mesh_q;
	std::thread task;
	
	ASyncLoader() : task(background_task,this){
		printf("init async loader %p\n",this);

		//pthread_create(&task,nullptr,(threadfunc_t) background_task, this);
	}
/*
	void load(const char* filename) {
		// todo - cached standins
		file_requests.push(std::string(filename));

	}
*/
	GLuint load_texture(const char* filename){
		// todo: check if the thing is already loaded.
		auto tx= glGenTextureAndBind(GL_TEXTURE_2D);
		glTexFilterWrap(GL_TEXTURE_2D,GL_LINEAR_MIPMAP_LINEAR,GL_REPEAT);
		uint8_t data[1024];
		int i; for (i=0; i<1024;i++){data[i]=255;}
		glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA8,16,16,0, GL_RGBA,GL_UNSIGNED_BYTE, data);

		// dummy placeholder.TODO want 16x16 fallbacks..
		requests.push(std::make_pair(tx,std::string(filename)));
		return tx;
	}
	Mesh* load_obj_mesh(const char* filename){
		return load_mesh(filename, ParseOBJFile);
	}
	Mesh* load_mesh(const char* filename,MeshLoaderFunc_t loader){
		extern Mesh* MeshNew();
		Mesh* msh = MeshNew();	// empty mesh, awaiting initialize by background task
		meshreq.push(MeshReq{msh,std::string(filename),loader});
		return msh;
	}
	static void* background_task(ASyncLoader* asl) {
		printf("hello from async loader thread %p\n",asl);
		while (1) {
			// meshes
			for (auto mrq=asl->meshreq.pop(); mrq ;mrq=asl->meshreq.pop()){	// grrr. RUST IS BETTER.
				char filename[512];
				snprintf(filename,512,"assets/%s",mrq->filename.c_str());
				auto fs=ifstream(filename);
				if (fs.good()){
					auto intermediateMmeshes=mrq->loader(fs,"assets/");
					// TODO - error detection on OBJ file parsing.
					asl->interim_mesh_q.push(MeshData{mrq->meshToInit,mrq->filename, intermediateMmeshes});
					printf("async loader got mesh %s\n",mrq->filename.c_str());
				} else {
					printf("async tex loader failed to load %s\n", mrq->filename.c_str());
				}
			}
			//tx
			decltype(requests.pop()) rq;
			while ((rq=asl->requests.pop())) {

				Vec2i sz;int bytepp;
				auto buffer=LoadTextureRaw(rq->second.c_str(),&sz,&bytepp);
				if(buffer) {
					printf("async tex loader got %s\n", rq->second.c_str());
					asl->interim_image_q.push(TexData{rq->first, rq->second, buffer, sz, bytepp});
				} else {
					printf("async tex loader failed to load %s\n", rq->second.c_str());
				}
			}
		    std::this_thread::sleep_for (std::chrono::milliseconds(8));

		}
	}
	// Must be polled on the main thread, eg once per frame
	//does the final setup for loaded entities prepared on background threads
	// which need final OpenGL resource initialization (ie buffer object and texture load)
	void update() {
		for (auto img=interim_image_q.pop(); img; img=interim_image_q.pop())
		{
			printf("async loader main thread update: got data for %s",img->filename.c_str());
			ReInitTextureFromRawMem(img->texid,img->buffer,img->size,img->bytepp);
			free(img->buffer);
		}
		for (auto msh=interim_mesh_q.pop(); msh; msh=interim_mesh_q.pop()){
			InitMeshFromBuilder(msh->mesh,msh->meshbuilder);
			delete msh->meshbuilder;
		}
	}
};

extern ASyncLoader g_AsyncLoader;
GLuint LoadTextureAsync(const char* fname);
struct Mesh;
Mesh* LoadOBJAsync(const char* fname);


template<typename RESOURCE, RESOURCE (*LOADER)(const char* filename) >
struct ResourceMgr{
	typedef int ResourceIndex;
	std::map<std::string,ResourceIndex> names;
	std::vector<RESOURCE>	items;
	 // a map name->item. adds reload functionality,..
	RESOURCE get(const std::string& name) {
		if (auto index=names.find(name); index!=names.end()){
			return items[index->second];
		} else {
			names.insert(std::make_pair(name, (ResourceIndex)items.size()));
			items.push_back(LOADER(name.c_str()));
			return items.back();
		}







































































































































	};
	ResourceIndex size() const {return items.size();}
	RESOURCE operator[](ResourceIndex index) {return items[index];}
};
GLuint LoadTextureAsync(const char* fname);
Mesh* LoadOBJAsync(const char* fname);
GLuint GetTextureOrLoad(const char* name); // exists to dodge header include. jst g_Textures.get(..)
extern ResourceMgr<Mesh*,LoadMesh>	g_Meshes;
extern ResourceMgr<GLuint,LoadTextureAsync> g_Textures;


