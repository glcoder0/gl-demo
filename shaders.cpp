#include "shaders.h"
#include "drawprim.h"
ShaderPrograms g_ShaderPrograms;

extern GLuint GetTextureOrLoad(const char*);
// todo - glhelpers.h/cpp
GLuint glGenFramebufferAndBind();
GLuint glGenTextureAndBind(GLuint type);
void glTexFilterWrap(GLuint target,GLuint filter,GLuint wrapmode);
void LazyInitRenderer();

#ifdef __ANDROID__
#define SHADER_VERSION \
"#version 300 es\nprecision highp float;\nprecision highp int;\n"
#else
// was version 330 but its not taking the 2 line prefix, why?!

#define SHADER_VERSION \
"#version 420 \n" \
"#extension GL_ARB_explicit_uniform_location : require \n" \
"#extension GL_ARB_separate_shader_objects : enable \n"
#endif

const char* g_ShaderVersionVS=SHADER_VERSION "\n#define varying out\n";
const char* g_ShaderVersionPS=SHADER_VERSION "\n#define varying in\n";


GLuint
GFX_CompileShaderSrcs(int shaderType, const char* ppSources[], bool proceed) 
{
	int numSources=0;while (ppSources[numSources]) numSources++;
	auto	shader = glCreateShader(shaderType);
	glShaderSource(shader, numSources,ppSources,nullptr);
	glCompileShader(shader);
	int	status;glGetShaderiv(shader,GL_COMPILE_STATUS,&status);
	if (status==GL_FALSE) {
		char	log[512]; int len;
		glGetShaderInfoLog(shader, 512,&len, log);
		for (int i=0; i<numSources;i++){
			LOGE("\n%s\n", ppSources[i]);
		}
		LOGE("compile shader[%d] failed:%d:\n%s\n", shader, len,log);
		if (!proceed)
			exit (0);//todo - return 0 and offer fallback paths?
		else return 0;
	}	
	else LOGI("create shader[%d] - compile suceeded\n",  shader);
	return	shader;
}
GLuint
GFX_CompileShaderSrc(int shaderType, const char* src){
	const char* ppSrc[2]={src,nullptr};
	return GFX_CompileShaderSrcs(shaderType,ppSrc);
}
// polling for dynamic shader update

#include <sys/stat.h>
time_t g_shader_file_time;
void UpdateShaders(bool forceRefresh){
	struct stat file_stat;
	const char* shadersource = "assets/landscape.glsl";
	stat(shadersource,&file_stat);
	if (!(file_stat.st_mtime > g_shader_file_time || forceRefresh))
		return;
	g_shader_file_time=file_stat.st_mtime;

	extern const char* g_VS_ColorTex2[];
	extern const char* g_PS_Particle[];
	extern const char* g_PS_Landscape[];
	size_t sz;
	
	auto *dat1=loadAndAllocResourceFile("landscape.glsl",&sz);
	if (!dat1) return;
	auto dat=(char*) malloc(sz+1); memcpy(dat,dat1,sz); dat[sz]=0;
	if (dat){
		//const char** ppSrc=&g_PS_Landscape[0];
		const char** ppSrc=&g_PS_Landscape[0];
		const char* overide[16];
		int	 i;
		for ( i=0; i<15&& ppSrc[i]; i++){
			overide[i]=ppSrc[i];
		}
		overide[i]=nullptr;
		overide[i-1]= dat;
		//auto prog=GFX_CreateShaderProgramFromSrcs(g_VS_Landscape, g_PS_LandscapeOveride);
		GLuint tmp=GFX_CompileShaderSrcs(GL_FRAGMENT_SHADER,overide,true);
		// relink? does it work?
		if (tmp) {
			auto prog=g_ShaderPrograms.landscape;
			GLuint sh[2];int num;
			glGetAttachedShaders(prog,2,&num,sh);
			glDetachShader(prog,sh[1]);
			
			glAttachShader(prog,tmp);
			glLinkProgram(prog);
		}
		free(dat);
	}
}

#ifdef _MSC_VER
#define strdup _strdup
#endif
const char* marker(const char* fn,int ln){char str[256];snprintf(str,256,"//%s:%d - SHADER SOURCE\n#line %d\n",fn,ln,ln);return strdup(str);}
#define SHADER_LINE marker(__FILE__,__LINE__)

//TODO: split into default uniforms, default vertex, default vertex-shader-out
// parameter 'varying' must be defined as 'out' for vertex shader, 'in' for pixel shader.

const char*g_PSVS_Common=R"SHADER(
#line 500
#ifndef varying
#error varying must be defined as in/out by a previous source block for this shader
#endif
// outputs from Vertex shaders / inputs to pixel shaders
varying vec4 v_pos; 
varying vec4 v_color; 
varying vec3 v_norm; 
varying vec2 v_tex0;
varying vec2 v_tex1;
varying vec4 v_tangent; 
varying vec4 v_binormal;

// todo - make this with a macro in sync with the CPU side enum
#line 600
layout(location=0) uniform mat4 uMatProj; 
layout(location=4) uniform mat4 uMatModelView; 
layout(location=8) uniform vec4 uAmbient; 
layout(location=9) uniform vec4 uAmbientDX; 
layout(location=10) uniform vec4 uAmbientDY; 
layout(location=11) uniform vec4 uAmbientDZ; 
layout(location=12) uniform vec4 uEyePos; 
layout(location=13) uniform vec4 uFogColor; 
layout(location=14) uniform vec4 uFogFalloff; 
layout(location=15) uniform vec4 uSunDir; 
layout(location=16) uniform float uSpecularPower; 
layout(location=17) uniform vec4 uSunColor; 
layout(binding=0) uniform sampler2D uTex0; 
layout(binding=1) uniform sampler2D uTex1; 
layout(binding=2) uniform sampler2D uTex2; 
layout(binding=3) uniform sampler2D uTex3;
layout(location=22) uniform int uNumLights; 
layout(location=23) uniform vec4 uLightPos[4]; 
layout(location=27) uniform vec4 uLightColor[4]; 
layout(location=31) uniform vec4 uAnim;	//e.g. water ripples,swaying grass.
layout(location=32) uniform mat4 uMatWorldToShadowBuffer[2]; 
layout(binding=4) uniform sampler2DArray uTexArray0;
layout(binding=5) uniform sampler2DArray uTexArray1;
layout(binding=6) uniform sampler2D uShadowMap[2];	// try also samplerShadow
)SHADER";

// todo - make this with a macro in sync with CPU side VertexAttrIndex..
//inputs to vertex shaders, from vertex data.
#define SHADER_VERTEX_ATTR \
"layout(location=0) in vec3 a_pos; \n"\
"layout(location=1) in vec4 a_color; \n"\
"layout(location=2) in vec3 a_norm; \n"\
"layout(location=3) in vec2 a_tex0; \n"\
"layout(location=4) in vec2 a_tex1; \n"

const char* g_VS_Default[]={
	g_ShaderVersionVS , g_PSVS_Common, SHADER_VERTEX_ATTR,SHADER_LINE,
	R"SHADER(
void main() {
	v_pos = uMatModelView * vec4(a_pos.xyz,1.0);
	// todo - scaling aware normal matrix associated with it..
	vec3 enorm = (uMatModelView * vec4(a_norm.xyz,0.0)).xyz;
	gl_Position = uMatProj * v_pos;
	v_color = a_color*vec4(1.0/255.0,1.0/255.0,1.0/255.0,1.0/255.0);
	v_tex0 = a_tex0;
	v_tex1 = a_tex1;
	v_norm = enorm;
}

)SHADER",
nullptr};

// cloned from 'default' but the landscape shader is likely to diverge alot.
const char* g_VS_Landscape[]={
	g_ShaderVersionVS , g_PSVS_Common, SHADER_VERTEX_ATTR,SHADER_LINE,
	R"SHADER(
void main() {
	v_pos = uMatModelView * vec4(a_pos.xyz,1.0);
	gl_Position = uMatProj * v_pos;
	v_color = a_color*vec4(1.0/255.0);// custom normalize, allow 0-128 or whatver
	v_tex0 = a_tex0;
	v_tex1 = a_tex1;
	v_norm = (uMatModelView * vec4(a_norm.xyz,0.0)).xyz;
}

)SHADER",
nullptr};



const char* g_VS_Color[]= {g_ShaderVersionVS,R"SHADER(
layout(location=0) uniform mat4 uMatProj; 
layout(location=0) in vec4 a_pos;
layout(location=1) in vec4 a_color;
out vec4 v_color;
void main() {
	gl_Position = uMatProj * a_pos;
	v_color = a_color*vec4(1.0/255.0,1.0/255.0,1.0/255.0,1.0/255.0);
}
)SHADER",
nullptr};

const char* g_PS_Color[]={g_ShaderVersionPS, R"SHADER(
in vec4 v_color;
out vec4 outFragColor;
void main() {
	outFragColor = v_color;// used for debug lines with blank tex.
}
)SHADER",
nullptr};

const char* g_VS_NOP[]= {g_ShaderVersionVS,R"SHADER(
layout(location=0) in vec4 a_pos;
layout(location=0) uniform vec4 uQuadPos[4];
out vec2 v_texcoord;
vec2 g_texcoord[]={vec2(0.0,0.0),vec2(1.0,0.0),vec2(0.0,1.0),vec2(1.0,1.0)};
void main() {
	gl_Position = uQuadPos[gl_VertexID];
	v_texcoord=g_texcoord[gl_VertexID];
}
)SHADER",
nullptr};

const char* g_PS_DebugQuad[]={g_ShaderVersionPS, R"SHADER(
in vec2 v_texcoord;
out vec4 outFragColor;
layout( binding=0) uniform sampler2D uTex0;
layout(location=4) uniform vec4 uColorOfs;
layout(location=5) uniform vec4 uColorMul;
void main() {

	outFragColor = texture2D(uTex0,v_texcoord)*uColorMul+uColorOfs;
	//,0.0,1.0);// used for debug lines with blank tex.
}
)SHADER",
nullptr};



const char* g_PoissonDisk16=R"SHADER(
vec2 poissonDisk16[16]={
	vec2(-0.038,-0.544),// 0
	vec2(0.513,-0.357),// 1
	vec2(0.066,-0.929),// 2
	vec2(0.080,-0.120),// 3
	vec2(-0.651,-0.500),// 4
	vec2(-0.457,-0.004),// 5
	vec2(-0.173,0.322),// 6
	vec2(0.501,0.178),// 7
	vec2(0.779,-0.104),// 8
	vec2(-0.987,-0.137),// 9
	vec2(-0.772,0.539),// 10
	vec2(0.130,0.772),// 11
	vec2(0.681,0.713),// 12
	vec2(-0.330,-0.882),// 13
	vec2(0.455,-0.842),// 14
	vec2(-0.228,0.966),// 15

}; 
)SHADER";

const char* g_GaussianPoints64=R"SHADER(
vec2 gaussianPoints64[64]={
vec2(-0.58916,-0.12164),	vec2(0.22388,-0.07339),	vec2(-0.39078,0.18904),	vec2(-0.62138,0.54359),	vec2(-0.61234,-0.81134),	vec2(0.05083,0.06818),	vec2(-0.79370,0.42018),	vec2(0.60086,-0.20161),	vec2(0.34607,0.15533),	vec2(0.61928,0.03902),	vec2(-0.08395,0.32855),	vec2(0.01271,0.17654),	vec2(0.29048,0.69365),	vec2(0.08324,-0.33440),	vec2(-0.11890,0.49495),	vec2(0.69675,0.49370),	vec2(-0.02989,0.61278),	vec2(0.73099,-0.18208),	vec2(-0.12013,-0.05827),	vec2(0.01865,0.29298),	vec2(0.48351,-0.34952),	vec2(-0.25153,-0.14371),	vec2(0.40187,0.11157),	vec2(-0.35541,0.14146),	vec2(0.25433,0.20412),	vec2(-0.59392,0.63239),	vec2(0.16852,0.17857),	vec2(0.69240,0.49634),	vec2(-0.54179,-0.25734),	vec2(0.46292,-0.16371),	vec2(-0.04979,-0.03656),	vec2(-0.44684,0.20953),	vec2(-0.14081,-0.20717),	vec2(0.43570,-0.44875),	vec2(-0.00066,-0.20857),	vec2(-0.79860,-0.13231),	vec2(-0.56540,-0.13370),	vec2(0.20934,0.76234),	vec2(-0.34950,-0.24562),	vec2(-0.16930,0.01117),	vec2(0.37761,-0.04480),	vec2(0.22923,-0.58244),	vec2(0.23630,-0.07962),	vec2(-0.50325,0.09732),	vec2(0.39844,-0.17110),	vec2(0.66624,-0.23265),	vec2(-0.68295,-0.47957),	vec2(-0.51433,-0.16232),	vec2(-0.66437,-0.32339),	vec2(0.29935,-0.20653),	vec2(-0.69684,-0.02664),	vec2(-0.49806,0.73129),	vec2(0.03433,0.50818),	vec2(0.15146,0.01735),	vec2(-0.20271,-0.38371),	vec2(0.06941,0.14626),	vec2(0.24686,0.96991),	vec2(-0.08295,0.08189),	vec2(0.47726,0.02014),	vec2(-0.87404,-0.12038),	vec2(0.31168,-0.13913),vec2(0.66336,-0.06833),	vec2(0.41603,-0.25638),	vec2(0.00194,-0.10951)
};
)SHADER";

const char* g_PoissonDisk64=R"SHADER(
vec2 poissonDisk64[64]={
vec2(0.358,-0.088),// 0
vec2(0.149,-0.076),// 1
vec2(0.394,0.229),// 2
vec2(0.565,0.059),// 3
vec2(0.207,-0.303),// 4
vec2(0.177,0.188),// 5
vec2(0.623,-0.128),// 6
vec2(0.511,-0.298),// 7
vec2(0.090,-0.575),// 8
vec2(0.005,-0.187),// 9
vec2(-0.017,-0.380),// 10
vec2(0.317,-0.490),// 11
vec2(0.790,0.159),// 12
vec2(0.817,-0.120),// 13
vec2(0.652,-0.466),// 14
vec2(0.835,-0.351),// 15
vec2(0.513,-0.633),// 16
vec2(0.704,-0.691),// 17
vec2(0.565,0.387),// 18
vec2(0.802,0.377),// 19
vec2(0.969,0.072),// 20
vec2(0.538,-0.821),// 21
vec2(0.694,0.610),// 22
vec2(0.337,-0.843),// 23
vec2(0.403,0.566),// 24
vec2(0.519,0.754),// 25
vec2(0.379,0.905),// 26
vec2(0.139,0.472),// 27
vec2(0.236,0.659),// 28
vec2(0.016,0.074),// 29
vec2(-0.190,0.002),// 30
vec2(-0.123,0.345),// 31
vec2(0.010,0.739),// 32
vec2(0.178,0.850),// 33
vec2(0.136,-0.890),// 34
vec2(-0.214,-0.561),// 35
vec2(-0.018,-0.725),// 36
vec2(0.301,0.381),// 37
vec2(-0.344,-0.412),// 38
vec2(-0.195,-0.245),// 39
vec2(-0.459,-0.095),// 40
vec2(-0.327,0.174),// 41
vec2(-0.531,-0.468),// 42
vec2(-0.528,-0.699),// 43
vec2(-0.305,-0.843),// 44
vec2(-0.739,-0.630),// 45
vec2(-0.028,0.530),// 46
vec2(-0.058,-0.968),// 47
vec2(0.219,-0.711),// 48
vec2(-0.220,0.563),// 49
vec2(-0.269,0.810),// 50
vec2(-0.573,0.118),// 51
vec2(-0.604,-0.209),// 52
vec2(-0.731,-0.363),// 53
vec2(-0.476,0.355),// 54
vec2(-0.456,0.781),// 55
vec2(-0.435,0.602),// 56
vec2(0.032,0.953),// 57
vec2(-0.799,0.434),// 58
vec2(-0.711,0.261),// 59
vec2(-0.847,-0.004),// 60
vec2(-0.965,0.160),// 61
vec2(-0.959,-0.247),// 62
vec2(-0.645,0.643)// 63
};
)SHADER";


const char* g_PS_BloomV0[]={g_ShaderVersionPS, g_GaussianPoints64, R"SHADER(

in vec2 v_texcoord;
out vec4 outFragColor;
layout( binding=0) uniform sampler2D uTex0;
layout(location=4) uniform vec4 uColorOfs;
layout(location=5) uniform vec4 uColorMul;
float circleFilter(vec2 v){
	float f=v.x*v.x+v.y*v.y;
	f=max((1.0-f*f),0.f);
	return f*f;
}
void main() {

	vec4 acc;
	int num=16;
	float pixelRadius=8.0f;
	int rnd=int(int(gl_FragCoord.x*0x1923)^int(gl_FragCoord.y*0x982));
	for (int i=0; i<num; i++) {
		vec2 ofs=gaussianPoints64[(i*5+rnd)&63]
				*pixelRadius*vec2(1.0/ 1920.0,1.0/1080.0);
		vec4 v=texture2D(uTex0, v_texcoord+ofs*3.0);//-uColorOfs;
		//v=max(v,vec4(0.0f,0.0f,0.0f,0.0f));
		// bounded aprox of gauss
		acc+=v;// * circleFilter(ofs*(1.0/8.0));
	}
	// TODO SCALING SHOULD RECOVER SAME BRIGHTNESS LEVEL 
	outFragColor = acc*(1.0/num)+texture2D(uTex0,v_texcoord)*0.75;

	//,0.0,1.0);// used for debug lines with blank tex.
}
)SHADER",
nullptr};

const char* g_PS_DepthOfField[]={g_ShaderVersionPS, g_GaussianPoints64, R"SHADER(

in vec2 v_texcoord;
out vec4 outFragColor;
layout( binding=0) uniform sampler2D uTex0;
layout( binding=1) uniform sampler2D uTex1;
layout(location=4) uniform vec4 uColorOfs;
layout(location=5) uniform vec4 uColorMul;
void main() {

	vec4 acc;
	int num=16;
	float w=texture2D(uTex1,v_texcoord).x;
	float z=1.0/(1.0001-w);
	float pixelRadius=max(clamp((z-2000.0) * 8.0f/1000.0f,0.0,16.0),
				clamp(16.0*(25.0-z)/25.0, 0.0,16.0));
 
	
	int rnd=int(int(gl_FragCoord.x*0x1923)^int(gl_FragCoord.y*0x982));
	for (int i=0; i<num; i++) {
		vec2 ofs=gaussianPoints64[(i*5+rnd)&63]
				*pixelRadius*vec2(1.0/ 1920.0,1.0/1080.0);
		vec4 v=texture2D(uTex0, v_texcoord+ofs*3.0);//-uColorOfs;
		//v=max(v,vec4(0.0f,0.0f,0.0f,0.0f));
		// bounded aprox of gauss
		acc+=v;// * circleFilter(ofs*(1.0/8.0));
	}
	// TODO SCALING SHOULD RECOVER SAME BRIGHTNESS LEVEL 
	outFragColor = acc*(1.0/num);

	//,0.0,1.0);// used for debug lines with blank tex.
}
)SHADER",
nullptr};



const char* g_PS_SSAO[]={g_ShaderVersionPS,SHADER_LINE, R"SHADER(

vec2 poissonDisk[4] = vec2[](
  vec2( -0.94201624, -0.39906216 ),
  vec2( 0.94558609, -0.76890725 ),
  vec2( -0.094184101, -0.92938870 ),
  vec2( 0.34495938, 0.29387760 )
);

in vec2 v_texcoord;
out vec4 outFragColor;
layout( binding=0) uniform sampler2D uTexImage;
layout( binding=1) uniform sampler2D uTexDepth;
layout(location=4) uniform vec4 uColorOfs;
layout(location=5) uniform vec4 uColorMul;
float circleFilter(vec2 v){
	float f=v.x*v.x+v.y*v.y;
	f=max((1.0-f*f),0.f);
	return f*f;
}
void main() {

	float accz=0.0;
	float wsum=0.0;
	float invz=texture2D(uTexDepth, v_texcoord).x;
	float z0 = 1.0/(1.0001-invz); // central value;
	float dzdx=dFdx(invz);
	float dzdy=dFdy(invz);
	const float maxdz=10.0;
	float acc_ambience=0.0;
	for (int i=0; i<256; i++) {
		//int ps=(i*0x8726^
	//				((int(gl_FragCoord.x/1920)*13)>>5)+
//					((int(gl_FragCoord.y/1080)*0x1324)>>5)
				//)&3;

		vec2 ofs=(vec2((i&15)-8,(i/16)-8)
				//+poissonDisk[ps]*(1.0/16.0)
			)
				*vec2(1.0/ 1920.0,1.0/1080.0);
		float z=1.0/(1-texture2D(uTexDepth, v_texcoord+ofs*3.0).x);//-uColorOfs;
		float dz=z-z0;
		float w=circleFilter(ofs*(1.0/3.0));
		// fi the gap is too big, dont occlude (it's different objects in space, not local occlusion
		// do this by de-weighting them.

		float interp_invz=invz+(dzdx*ofs.x+dzdy*ofs.y)*3.0f*2.0f;
		float zc=1.0/(1.0001-invz);
//		float zc=z0+(dzdx*ofs.x+dzdy*ofs.y)*3.0f*2.0f; //x2:wrt 'window coordinate' -1to1.texcoords are 0-1 

#ifdef SUPRESS_BIG_DIST_CHANGE
		float absdz=abs(z-z0);
		if (absdz>maxdz) {
			float fade=1.0-(absdz-maxdz)/maxdz;
			w*=clamp(fade, 0.0, 1.0);
		}
#endif

		wsum+=w;
		accz+=z * w;
		acc_ambience += w*clamp((z0-z) ,-2.0, 2.0);
	}
	float z_env = accz/wsum; // weighted sum of surrounding z's

	float ambience1=clamp(0.9-0.55*acc_ambience/wsum,0.0,1.0);
	outFragColor = 
/*	// splitscreen debug of stages.
			(gl_FragCoord.x<700.0)?vec4(ambience1,ambience1,ambience1,1.0):
			(gl_FragCoord.x<1200)?vec4(0.5f+dzdx,0.5f+dzdy,1.0,1.0):
*/
				texture2D(uTexImage,v_texcoord.xy)*ambience1;
}
)SHADER",
nullptr};

const char* g_PS_Edges[]={g_ShaderVersionPS, R"SHADER(
in vec2 v_texcoord;
out vec4 outFragColor;
layout( binding=0) uniform sampler2D uTex0;
layout(location=4) uniform vec4 uColorOfs;
layout(location=5) uniform vec4 uColorMul;
float circleFilter(vec2 v){
	float f=v.x*v.x+v.y*v.y;
	f=max((1.0-f*f),0.f);
	return f*f;
}
void main() {

	vec4 acc;
	for (int i=0; i<256; i++) {
		vec2 ofs=vec2(((i&15)-8)/1920.0,
					((i/16)-8)/1080.0);
		vec4 v=texture2D(uTex0, v_texcoord+ofs*3.0);//-uColorOfs;
		//v=max(v,vec4(0.0f,0.0f,0.0f,0.0f));
		// bounded aprox of gauss
		acc+=v * circleFilter(ofs*(1.0/8.0));
	}
	outFragColor = 1.5f*texture2D(uTex0,v_texcoord)*0.75f-acc*(1.0/256.0);

	//,0.0,1.0);// used for debug lines with blank tex.
}
)SHADER",
nullptr};


const char* g_VS_ColorTex[]= {g_ShaderVersionVS,R"SHADER(
layout(location=0) uniform mat4 uMatProj; 
layout(location=0) in vec4 a_pos;
layout(location=1) in vec4 a_color;
layout(location=3) in vec2 a_tex0; //inline with other
out vec4 v_color;
out vec2 v_tex0;
void main() {
	gl_Position = uMatProj * a_pos;
	v_color = a_color*vec4(1.0/128.0,1.0/128.0,1.0/128.0,1.0/255.0);
	v_tex0 = a_tex0;
}
)SHADER",
nullptr};
const char* g_VS_ColorTex2[]= {g_ShaderVersionVS,R"SHADER(
layout(location=0) uniform mat4 uMatProj; 
layout(location=0) in vec4 a_pos;
layout(location=1) in vec4 a_color;
layout(location=3) in vec2 a_tex0; //inline with other
layout(location=4) in vec2 a_tex1; //inline with other
out vec4 v_color;
out vec2 v_tex0;
out vec2 v_tex1;
void main() {
	gl_Position = uMatProj * vec4(a_pos.xyz,1.0);
	v_color = a_color*vec4(1.0/128.0,1.0/128.0,1.0/128.0,1.0/255.0);
	v_tex0 = a_tex0;
	v_tex1 = a_tex1;}
)SHADER",
nullptr};

const char* g_PS_ColorTex[]={g_ShaderVersionPS, R"SHADER(
layout(binding=0) uniform sampler2D uTex0;
in vec4 v_color;
in vec2 v_tex0;
out vec4 outFragColor;
void main() {
	outFragColor = texture(uTex0,v_tex0);// used for debug lines with blank tex.
}
)SHADER",
nullptr};

const char* g_PS_Particle[]={g_ShaderVersionPS, R"SHADER(
layout(binding=0) uniform sampler2D uTex0;
layout(binding=1) uniform sampler2D uTex1;
layout(binding=2) uniform sampler2D uTex2;
in vec4 v_color;
in vec2 v_tex0;
in vec2 v_tex1;
out vec4 outFragColor;
void main() {

    vec4 blob=       +texture(uTex2,v_tex0)*2.0;
                //-texture(uTex2, (v_tex0-vec2(1.25,1.25f))*2.0 )*1.5f;

    vec4 blobplus=blob+
                texture(uTex0,v_tex0)
                +texture(uTex1,v_tex1*0.25f).yyyy*1.0
                -vec4(1.5,1.5,1.5,2.0);;
    blobplus*=v_color;
    blobplus=clamp(blobplus,vec4(0.0f),vec4(1.0f));
    outFragColor=blobplus;

}
)SHADER",
nullptr};


// 2 texture layers, blended using vertex alpha
const char g_PS_Common[]= R"SHADER(
#line 6000
#define varying in

float sqr(float f){return f*f;}

float clamp01(float fv){return clamp(fv,0.0f, 1.0f);}
vec3 applyFog(vec3 color){
	float f=1.f/gl_FragCoord.w;
	return mix(color,uFogColor.xyz,  clamp(uFogFalloff.x+f*uFogFalloff.y,0.0,1.0));
}
vec4 applyFogARGB(vec4 color){
	float f=1.f/gl_FragCoord.w;
	return mix(color,vec4(uFogColor.xyz,0.0),  clamp(uFogFalloff.x+f*uFogFalloff.y,0.0,1.0));
}

vec4 pointlight(vec3 pos, vec3 norm,vec3 lpos, vec4 color, float falloff) {
	vec3 dv=lpos-pos;
	float d2=sqrt(dot(dv,dv));
	float f=clamp( 1.0-(d2/falloff),0.0,1.0);
	vec3 lv=normalize(dv);
	return clamp(dot(lv,norm),0.0,1.0) * f*color;
}
#line 7000
float improvisedSpecularFactor(vec4 surfaceColor){
	return clamp(2.0*sqrt( dot(surfaceColor.xyz,surfaceColor.xyz)*(1.0/3.0))-0.5 ,0.0, 1.0);
}
vec3 triplanarFactorsFrom(vec3 norm){
	vec3 factors = clamp(abs(norm)-vec3(0.5,0.5,0.5),0.0,1.0);
	vec3 fnorm=factors*(1.0/(factors.x+factors.y+factors.z));
	return fnorm;
}
vec3 triplanarFactors(vec3 norm){
	return triplanarFactorsFrom(norm);
}
#line 9000
vec4 texTriplanar(sampler2D tops,sampler2D sides,vec3 norm,vec3 texcoords){
	vec3 fnorm=triplanarFactorsFrom(norm);
	return
		texture(tops, -texcoords.xy)*fnorm.z+
		texture(sides, -texcoords.xz)*fnorm.y+
		texture(sides, -texcoords.yz)*fnorm.x;
}
vec4 texTriplanarFromArray(sampler2DArray texarr,vec3 indices,vec3 norm,vec3 texcoords){
	vec3 fnorm=triplanarFactorsFrom(norm);
	return
		texture(texarr, vec3(-texcoords.xy,indices.z))*fnorm.z+
		texture(texarr, vec3(-texcoords.xz,indices.y))*fnorm.y+
		texture(texarr, vec3(-texcoords.yz,indices.x))*fnorm.x;
}
#line 9500
vec4 applyLight(vec4 albedo, vec3 norm, float specFactor, float sunShadowFactor){
	vec3 fromEye=normalize(v_pos.xyz-uEyePos.xyz); // reflected vector from eye for specular higlight
	vec3 refl = fromEye-norm*2.0*dot(fromEye,norm);
	float highlightIntensity=pow(max(0.f,dot(refl,uSunDir.xyz)),6.0f);
	// 'shadowFactor' applies to *sun* only.
	vec4 spec=highlightIntensity*uSunColor*specFactor*2.5f*sunShadowFactor;
	//vec4 diffuseLight=uAmbient+norm.x*uAmbientDX+norm.y*uAmbientDY+norm.z*uAmbientDZ;
	vec4 diffuseLight = clamp(dot(norm.xyz,uSunDir.xyz),0.0f,1.0f) * uSunColor*sunShadowFactor;
	for (int i=0; i<uNumLights; i++) {
		diffuseLight +=pointlight(v_pos.xyz, norm.xyz, uLightPos[i].xyz, uLightColor[i].xyzw, uLightPos[i].w);
	}
	vec4 ambient=uAmbient+uAmbientDZ*norm.z;
	albedo*=vec4(v_color.xyz,0.0);
	return vec4(
			applyFog(albedo.xyz*
				(ambient.xyz+(diffuseLight.xyz+spec.xyz))), 
			albedo.w);
}


vec2 poissonDisk[4] = vec2[](
  vec2( -0.94201624, -0.39906216 ),
  vec2( 0.94558609, -0.76890725 ),
  vec2( -0.094184101, -0.92938870 ),
  vec2( 0.34495938, 0.29387760 )
);
float random (vec2 st,int seed) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        (43758.5453123+seed*12758.5453123));
}
float shadowFactor(){
	// k=cascade index

	int k=gl_FragCoord.w<(1.0/256.0)?1:0; //further one =0 , nearer one =1
	
	vec4 shadowPos1 = (uMatWorldToShadowBuffer[k] * v_pos);
	vec3 shadowPos =(shadowPos1.xyz+vec3(1.0f,1.0f,1.0f))*vec3(0.5f,0.5f,0.5f);
	
	//float shadowZ= texture(uShadowMap, shadowPos.xy).x;

	float shadowFactor=1.0;
	float bias=0.005f;
	//if (shadowZ <shadowPos.z-0.01f) shadowFactor=0.25;

    vec2 st = gl_FragCoord.xy/vec2(1920.0,1080.0);


	int ns=(gl_FragCoord.w<(1.0/128.0))?4:(gl_FragCoord.w<(1.0/64.0))?8:16;
	float occlusion=0.0;
	for (int i=0;i<ns;i++){
		vec2 delta=vec2(random(st,i),random(st.yx,i))-vec2(0.5,0.5);
		if ( texture(
				uShadowMap[k],
				shadowPos.xy + (poissonDisk[i/4]+poissonDisk[i&3])/2800.0
					).x  
			<  shadowPos.z-bias 
			)
		{
			occlusion+=1.0;
		}
	}
	//float fadeshadow = 1.0;//clamp(gl_FragCoord.w-(1.0/384.0),0.0,1.0);
	shadowFactor-=occlusion/ns;
	return shadowFactor;
}

)SHADER";


// 2 texture layers, blended using vertex alpha
const char* g_PS_Tex2Blend[]={ 
	g_ShaderVersionPS,	g_PSVS_Common, g_PS_Common, marker(__FILE__,__LINE__),
	R"SHADER(

out vec4 outFragColor;

void main() {
	vec4 tex0color=texture(uTex0, v_tex0);
	vec4 tex1color=texture(uTex1, v_tex1);
	vec4 tex0mid = textureLod(uTex0, v_tex0, 8.0);
	vec4 tex1mid = textureLod(uTex1, v_tex1, 8.0);
	float h0 = dot(tex0color.xyz-tex0mid.xyz,vec3(0.5,0.5,0.5)) - (v_color.w-0.5 );
	float h1 = dot(tex1color.xyz-tex1mid.xyz,vec3(0.5,0.5,0.5)) + (v_color.w-0.5 );
	float alpha = clamp01((h1-h0)+0.5);
	vec4 albedo=mix(tex0color,tex1color,alpha).xyzw*vec4(v_color.xyz,1.0);
	float specFactor = clamp01(2.f*(h0+h1));
	outFragColor = applyLight(albedo,normalize(v_norm),specFactor, shadowFactor());
}
)SHADER",
nullptr};

// 2 texture layers multiplied together, 2x such that midgrey = neutral
const char* g_PS_Tex2Mul2x[]={ 
	g_ShaderVersionPS,	g_PSVS_Common, g_PS_Common, marker(__FILE__,__LINE__),
	R"SHADER(

out vec4 outFragColor;

void main() {
	vec4 tex0color=texture(uTex0, v_tex0);
	vec4 tex1color=texture(uTex1, v_tex1);
	vec4 albedo=tex0color*tex1color*2.0f*v_color;
	outFragColor = applyLight(albedo,normalize(v_norm), albedo.w, shadowFactor());
}
)SHADER",
nullptr};
// 2 texture layers, alpha channel of *second* blends over the first, plus vertex alpha control.
const char* g_PS_Tex2AlphaTex[]={ 
	g_ShaderVersionPS,	g_PSVS_Common, g_PS_Common, marker(__FILE__,__LINE__),
	R"SHADER(

out vec4 outFragColor;

void main() {
	vec4 tex0color=texture(uTex0, v_tex0);
	vec4 tex1color=texture(uTex1, v_tex1);
	vec4 albedo=mix(tex0color,tex1color,tex1color.w*v_color.w)*v_color;
	outFragColor = applyLight(albedo,normalize(v_norm), albedo.w, shadowFactor());
}
)SHADER",
nullptr};


// 2 texture layers, water normal maps
const char* g_PS_Water[]={ 
	g_ShaderVersionPS,	g_PSVS_Common, g_PS_Common, marker(__FILE__,__LINE__),	R"SHADER(

out vec4 outFragColor;
#define SQR(X) ((X)*(X))
void main() {
	vec2 scroll=vec2(uAnim.x,0.f);
	vec3 tex0color=(texture(uTex0, v_tex0*0.05f - scroll*0.5f).xyz-vec3(0.5,0.5,0.5))
			-(texture(uTex0, v_tex0*0.1f + scroll).xyz-vec3(0.5,0.5,0.5))
			+(texture(uTex0, v_tex0*0.025f + scroll.yx*0.33).xyz-vec3(0.5,0.5,0.5))
			-(texture(uTex0, v_tex0*0.005f - scroll.yx*0.1).xyz-vec3(0.5,0.5,0.5));
	// todo - what happened to v_norm?
	vec3 norm = normalize(v_norm+0.5f*(tex0color.xyz));


	vec3 toEye = normalize(v_pos.xyz - uEyePos.xyz);
	vec3 refl = normalize(toEye-2.0f*norm*(dot(norm,toEye))) ; //shouldnt need to
         
	float fresnel=1.0-0.5f*(SQR(dot(norm,toEye))); // not helping.
	float specFactor = 1.0;

	outFragColor = applyFogARGB(vec4(fresnel*texture(uTex1, (refl.xz*0.5+0.5)).xyz,v_color.w));
}
)SHADER",
nullptr};


const char* g_PS_Landscape[]={
	g_ShaderVersionPS,	g_PSVS_Common,	g_PS_Common, 	marker(__FILE__,__LINE__),
R"SHADER(
out vec4 outFragColor;


void main() {
	vec3 norm=normalize(v_norm);
	float indexh=(v_pos.z+10.0)*0.1;
	float indexv=indexh;
	vec4 tex0color=texTriplanarFromArray(uTexArray0,vec3(indexh,indexv,indexh), norm, v_pos.xyz*0.05f);
	vec4 tex1color=texTriplanar(uTex0, uTex1, norm,v_pos.xyz*0.25);

	//vec4 albedo=vec4((tex0color*tex1color*v_color*1.4f).xyz, 1.0);

	// Scaling factor for use of multiplicative microtexture. assumes its 50% average
	vec4 albedo = tex1color * tex0color*v_color*2.0f;	
	float specFactor= improvisedSpecularFactor(albedo);

	
	outFragColor = applyLight(albedo,norm,specFactor,shadowFactor());
	//outFragColor = (gl_FragCoord.x<960)?vec4(shadowZ):vec4(shadowPos.z);
	//v_pos.zzzz;

	//outFragColor = vec4(gl_FragCoord.x/1920.0,gl_FragCoord.y/1080.0,0.f,1.0f);//

	// dbg- show shadowmap
	//outFragColor = texture(uShadowMap[gl_FragCoord.x<960?0:1], vec2(gl_FragCoord.x/1920.0,gl_FragCoord.y/1080.0));
}
)SHADER",
nullptr};

const char* g_PS_ShadowPass[]={
	g_ShaderVersionPS,	g_PSVS_Common,	g_PS_Common, 	marker(__FILE__,__LINE__),
R"SHADER(
layout(location = 0) out float fragmentdepth;

void main() {
	fragmentdepth=gl_FragCoord.z;
}
)SHADER",
nullptr};


GLuint GFX_CreateShaderProgram(GLuint vs, GLuint ps){
	LOGI("Create shader prog.. %s\n","");
	auto	prog = glCreateProgram();
	glAttachShader(prog, vs);
	glAttachShader(prog, ps);

	LOGI("linking verteshader[%d], pixelshader[%d] to program[%d]\n", vs,ps, prog);
	glLinkProgram(prog);

	GLint err=0;
	glGetProgramiv(prog,GL_LINK_STATUS,&err);
	if (err!=GL_TRUE) {	
		char errMsg[1024];int len;
		glGetProgramInfoLog(prog,1024,&len,errMsg);
		LOGI("link program failed:\n %s\n",errMsg);
		return 0;
	} else LOGI("link program status %d\n", err);

	return prog;
}

GLuint	GFX_CreateShaderProgramFromSrcs(
			const char** vertexShaderSource,
			const char** pixelShaderSource)
{
	return GFX_CreateShaderProgram(
			GFX_CompileShaderSrcs(GL_VERTEX_SHADER, vertexShaderSource),
			GFX_CompileShaderSrcs(GL_FRAGMENT_SHADER, pixelShaderSource));
}

void	CreateShaders()
{
	g_ShaderPrograms.debugQuad=GFX_CreateShaderProgramFromSrcs(g_VS_NOP, g_PS_DebugQuad);
	g_ShaderPrograms.color=GFX_CreateShaderProgramFromSrcs(g_VS_Color, g_PS_Color);
	g_ShaderPrograms.colorTex=GFX_CreateShaderProgramFromSrcs(g_VS_ColorTex, g_PS_ColorTex);
	g_ShaderPrograms.particle=GFX_CreateShaderProgramFromSrcs(g_VS_ColorTex2, g_PS_Particle);
	g_ShaderPrograms.tex2Blend=GFX_CreateShaderProgramFromSrcs(g_VS_Default, g_PS_Tex2Blend);
	g_ShaderPrograms.tex2Mul2x=GFX_CreateShaderProgramFromSrcs(g_VS_Default, g_PS_Tex2Mul2x);
	g_ShaderPrograms.landscape=GFX_CreateShaderProgramFromSrcs(g_VS_Landscape, g_PS_Landscape);
	g_ShaderPrograms.landscapeShadowPass=GFX_CreateShaderProgramFromSrcs(g_VS_Landscape, g_PS_ShadowPass);
	g_ShaderPrograms.defaultShadowPass=GFX_CreateShaderProgramFromSrcs(g_VS_Default, g_PS_ShadowPass);
	g_ShaderPrograms.water=GFX_CreateShaderProgramFromSrcs(g_VS_Landscape, g_PS_Water);

	
	g_ShaderPrograms.ssao=GFX_CreateShaderProgramFromSrcs(g_VS_NOP, g_PS_SSAO);
	g_ShaderPrograms.depthOfField=GFX_CreateShaderProgramFromSrcs(g_VS_NOP, g_PS_DepthOfField);
	// postprocessing
	g_ShaderPrograms.bloomv0=GFX_CreateShaderProgramFromSrcs(g_VS_NOP, g_PS_BloomV0);
	g_ShaderPrograms.edges=GFX_CreateShaderProgramFromSrcs(g_VS_NOP, g_PS_Edges);
	g_ShaderPrograms.depthOfField=GFX_CreateShaderProgramFromSrcs(g_VS_NOP, g_PS_DepthOfField);
}
GLuint ShaderPrograms::getShadowPassShader(GLuint sh){
	if (sh==landscape){ return landscapeShadowPass;}
	else return defaultShadowPass;
}

void sdl_pick_largest_screen(SDL_Rect& out){
	int	i;
	out.w=0;
	for (i=0; i<SDL_GetNumVideoDisplays(); i++) {
		SDL_Rect rc;
	    SDL_GetDisplayBounds( i, &rc );
		if (rc.w>out.w){out=rc;}
	}
}
SDL_Window* init_sdl_gl_window(int w,int h, SDL_GLContext* pGLC){
	SDL_DisplayMode DM;
	SDL_GetDesktopDisplayMode(0, &DM);
    
	LOGI("screen %d x %d",DM.w,DM.h);

	SDL_Init(SDL_INIT_EVERYTHING);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    SDL_GL_SetSwapInterval(0);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);

    int screenI=0;
    SDL_Rect bounds{0,0,0,0};
    SDL_GetDisplayBounds( 0, &bounds );
    int fullscreen=0;
	if (w==0 && h==0) {
        fullscreen=SDL_WINDOW_MAXIMIZED|SDL_WINDOW_BORDERLESS;
        w=bounds.w;h=bounds.h;
    }
    auto wnd(
        SDL_CreateWindow("test", bounds.x,bounds.y,
            bounds.w, bounds.h, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN |SDL_WINDOW_MAXIMIZED|SDL_WINDOW_BORDERLESS));
    auto glc=SDL_GL_CreateContext(wnd);;
	glewInit();
    if (pGLC)
        *pGLC = glc;
	SDL_ShowWindow(wnd);
    return wnd;

}

#ifdef MAIN_shaders
int main(int argc,const char** argv){
    init_sdl_gl_window(0,0,nullptr);
    CreateShaders();

}
#endif


void SetUniform(const GLuint index,const Vec4& v){
	glUniform4fv(index,1,(float*)&v); // todo - whats the safer way assumes mem layout.
}
void SetUniform(GLuint index,const Matrix4& m){
	glUniformMatrix4fv(index,1,GL_FALSE, (float*)&m);	// todo -whats the safer way. assumes mem layout
}



void SetUniformLight(int index, const Vec4& posRadius, const Vec4& color){
	SetUniform(uLightPos+index, posRadius);
	SetUniform(uLightColor+index, color);
}
void glSetTextureLayer(int layer, GLuint texName){
	glActiveTexture(GL_TEXTURE0+layer);	glBindTexture(GL_TEXTURE_2D, texName);
}
void glSetTextureLayerEx(int layer, GLuint texName,GLuint type){
	glActiveTexture(GL_TEXTURE0+layer);	glBindTexture(type, texName);
}

GLuint g_hdrFBO;
GLuint g_hdrDepthBuffer;
GLuint g_hdrColorBuffer;
GLuint g_tmpfbo[3];
GLuint g_ShadowMapFBO[SHADOWMAP_CASCADES];
GLuint g_ShadowMapDepth[SHADOWMAP_CASCADES];// 1 cascades.

void InitShadowMap() {
	if (g_ShadowMapFBO[0]){ return;} // it's already initialized?
	int	i;
	for (i=0; i<SHADOWMAP_CASCADES; i++) {
		g_ShadowMapFBO[i]=glGenFramebufferAndBind();
		g_ShadowMapDepth[i]=glGenTextureAndBind(GL_TEXTURE_2D);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16,SHADOW_SIZE,SHADOW_SIZE, 0, GL_DEPTH_COMPONENT,GL_FLOAT,NULL);
	// clamp surelu?!
		glTexFilterWrap(GL_TEXTURE_2D,GL_NEAREST, GL_CLAMP); 


		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, g_ShadowMapDepth[i], 0);
	}
	glDrawBuffer(GL_NONE);
	//glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);  
}

extern int g_ShowDebugLines;
CamView
SetupShadowCameraRefactored(CamView& cam,Vec4 shadow_dir,int cascadeIndex,float zrelnear, float zrelfar){
	CamView shcam;
	Vec4 frustum[8];
	cam.frustum_vertices(frustum,zrelnear,zrelfar);	// only shadow half the depth range?

	shcam.set_orthographic(shadow_dir, Vec4(0.f,1.0f,0.f,0.f), frustum,8, 0.f);

	Vec4 tfrustum[8];
	for (int i=0; i<8; i++){
		//tfrustum[i]=((frustum[i]-fcentre)*(1.0/1000.f)).xyz1();
		tfrustum[i]=cam.proj_view*frustum[i];
		//shadowProj * worldToShadow*points[i];
	}
	if (g_ShowDebugLines) {
		g_DebugDraw.cuboid(tfrustum,0xff00ff00);
		g_DebugDraw.line2d(Vec4(-1.f,-1.f,-2.f,1.f),Vec4(1.f,1.f,2.f,1.f),0xffff0000);
		g_DebugDraw.flush();
	}
	cam.worldToShadowBuffer[cascadeIndex] = shcam.proj_view;
	return shcam;
}
extern Vec2i g_viewport;

void RenderCtx::setMainLight(const DirLight& _s,Vec4 _ambient, Vec4 _ambient_dz){
	sun=_s;
	ambient=_ambient;
	ambient_dz=_ambient_dz;
}
void RenderCtx::insertLight(const PointLight& lt){

	if (this->cv.cull_test_sphere_at(lt.pos,lt.radius)
		!=CullResult::INVIS)
	{
		this->lights.push_back(lt);
	}
}
void RenderCtx::setLightShaderConstants(const Matrix4& objMat,const Sphere& bsphere)const{
	auto&rc=*this;
	SetUniform(uEyePos,rc.cv.pos());

	if (!rc.cv.is_shadow) {
		for (int k=0; k<SHADOWMAP_CASCADES; k++) {
			glSetTextureLayer(6+k,g_ShadowMapDepth[k]);
			SetUniform(uMatWorldToShadowBuffer+k, rc.cv.worldToShadowBuffer[k]);
		}
	}

	float s=20.f;
	// specular highlight, only one supported eg representing main sun light?
	SetUniform(uSunDir, rc.sun.dir);
//	SetUniform(uSpecularColor, Vec4{0.5f,0.5f,0.5f,0.f});
	SetUniform(uSunColor, rc.sun.color);
	// describe a spherical harmonic - essentially a 4x4 matrix transforming normal vector into a color
	// aproximatly it's an ambient color and variation of color along x,y,z axes.
	// you can approximate environment lights this way.
	SetUniform(uAmbient, rc.ambient);
	SetUniform(uAmbientDX, Vec4(0.0));//Vec4(0.00,0.1,0.1,0.0));//Vector4f{0.0f,0.0f,0.25f,1.f});
	SetUniform(uAmbientDZ, rc.ambient_dz);// Vector4f{0.25f,0.25f,0.25f,1.f});
	//SetUniform(uDiffuseDY, Vec4(0.7*0.7071,0.6*0.7071,0.40*0.7071,0.0));// Vector4f{0.25f,0.0f,0.0f,1.f});
	SetUniform(uAmbientDY, Vec4(0.0));//Vec4(0.1,0.22,0.3,0.0));// Vector4f{0.25f,0.0f,0.0f,1.f});
	// simple depth based fog
	SetUniform(uFogColor, rc.fogColor);
	SetUniform(uFogFalloff, rc.fogFalloff);

	glUniform1i(uNumLights,4);
	// Cull these lights to the current object.
	#ifdef ANDROID
	// my entry level android phone doesn't like doing a lot of per pixel lights.
	glUniform1i(uNumLights,0);
	#else
	#endif
	int nLights=std::min(4,(int)this->lights.size());
	glUniform1i(uNumLights, nLights);
	for (int i=0; i<nLights; i++){
		SetUniformLight(i, Vec4(this->lights[i].pos.xyz(),this->lights[i].radius),this->lights[i].color);
	}
}
// todo - really goes in a seperate render layer, but its better here than in the main program file!
void RenderShadowBuffer(CamView& realcam, std::function<void(RenderCtx&)>& renderScene){
	LazyInitRenderer();
	Vec4 shadow_dir = Vec4(0.0f,1.0f,1.0f,0.0f).normalize();
	Vec4 shcentre;

	RenderCtx shadowrender;
	shadowrender.cv.m_viewport_size=Vec2i(SHADOW_SIZE,SHADOW_SIZE);
	glViewport(0,0,SHADOW_SIZE,SHADOW_SIZE);
	int k;
	float s_cascadescale[SHADOWMAP_CASCADES][2]={
		{0.f,0.25f}, {0.25f,1.0f}
	};
	for (k=0; k<SHADOWMAP_CASCADES; k++) {
		glBindFramebuffer(GL_FRAMEBUFFER,g_ShadowMapFBO[k]);
//	    		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, g_ShadowMapDepth[k], 0);
		glClear(GL_DEPTH_BUFFER_BIT);
		glCullFace(GL_BACK);
		float zreln=SHADOWMAP_CASCADES==1?0.f:s_cascadescale[k][0];
		float zrelf=SHADOWMAP_CASCADES==1?1.f:s_cascadescale[k][1];
		shadowrender.cv = SetupShadowCameraRefactored(realcam,shadow_dir,k,zreln,zrelf);
		shadowrender.pass = 1;
		renderScene(shadowrender);
	}

	glBindFramebuffer(GL_FRAMEBUFFER,0);

	glViewport(0,0,g_viewport.x,g_viewport.y);
	glCullFace(GL_FRONT);
}

void RenderAllPasses(const CamView& camViewIn, PostProcess ppmode,std::function<void(RenderCtx&)> gatherLights, std::function<void(RenderCtx& c)> renderSceneIntoPass) {


	LazyInitRenderer();
	CamView cam=camViewIn;// todo - make this immutable by putting the shadowmap matrices into *renderctx*
	RenderShadowBuffer(cam, renderSceneIntoPass);

	RenderCtx rc; rc.cv=cam;rc.currShaderProg=-1;
	gatherLights(rc);
	rc.pass=0;

	switch (ppmode) {
	default:
	case PostProcess::None:
		glBindFramebuffer(GL_FRAMEBUFFER,0);
		glClearColor(rc.fogColor.x,rc.fogColor.y,rc.fogColor.z,rc.fogColor.w);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		renderSceneIntoPass(rc);
	break;

	case PostProcess::DepthOfField:
	case PostProcess::SSAO:
	case PostProcess::Edges:
	case PostProcess::Bloom:
		glBindFramebuffer(GL_FRAMEBUFFER,g_hdrFBO);
		glClearColor(rc.fogColor.x,rc.fogColor.y,rc.fogColor.z,rc.fogColor.w);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		renderSceneIntoPass(rc);

		if (rc.transparencies.size()) {
			//printf("got %d transparent reqs\n",rc.transparencies.size());
			rc.currShaderProg=-1;
			rc.transparent=true;
			glEnable(GL_BLEND);  
			glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA); // alpha-add blending. premult-alpha.

			for (auto& dr:rc.transparencies) {
				RenderMeshAt(rc, dr.msh,dr.mat);
			}
			glDisable(GL_BLEND);  
		}
		// transparencies pt.2
		// TODO: these draw-buffers should be part of the render context.
		g_Sprites.render(rc.cv.proj_view,
			GetTextureOrLoad("scrap_aluminum.jpg"),
			GetTextureOrLoad("gravel_midgrey.jpg"),
			GetTextureOrLoad("blob.tga")

			);
		g_DebugDraw.render(rc.cv.proj_view);


		glBindFramebuffer(GL_FRAMEBUFFER,0);
		auto shader =
			ppmode==PostProcess::Bloom?g_ShaderPrograms.bloomv0:
			ppmode==PostProcess::SSAO?g_ShaderPrograms.ssao:
			ppmode==PostProcess::DepthOfField?
			g_ShaderPrograms.depthOfField:g_ShaderPrograms.edges;
		Vec2 sz(0.2,0.2);
		DrawQuad(Vec2(-1.0,-1.0), Vec2(2.0,2.0),shader, g_hdrColorBuffer,g_hdrDepthBuffer, 0);
		DebugDrawQuad(Vec2(-1.0f,-0.4f), sz,g_hdrColorBuffer ,Vec4(1.0),Vec4(1.0f,1.0f,1.0f,0.f));
		DebugDrawQuad(Vec2(-1.0f,-0.2f), sz,g_hdrColorBuffer ,Vec4(1.0f),Vec4(-1.0f,-1.0f,-1.0f,0.f));
		DebugDrawQuad(Vec2(-1.0f,0.0f), sz,g_hdrColorBuffer );
		DebugDrawQuad(Vec2(-1.0f,0.2f), sz,g_hdrDepthBuffer );
	
		DebugDrawQuad(Vec2(-1.0f,0.4f), sz,g_ShadowMapDepth[0] );
		DebugDrawQuad(Vec2(-1.0f,0.6f), sz,g_ShadowMapDepth[1] );
	break;
	};
};

void InitRenderBuffers(){
	InitShadowMap();
	if (g_hdrFBO) return; // already done.

	// create an HDR framebuffer with color & depth maps.
	g_hdrFBO=glGenFramebufferAndBind();

	g_hdrDepthBuffer=glGenTextureAndBind(GL_TEXTURE_2D);
	glTexImage2D(GL_TEXTURE_2D,0,GL_DEPTH_COMPONENT32,g_viewport.x,g_viewport.y,0, GL_DEPTH_COMPONENT, GL_FLOAT,NULL);
	glTexFilterWrap(GL_TEXTURE_2D,GL_NEAREST,GL_CLAMP);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, g_hdrDepthBuffer, 0);
	//glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1024, 768);

//	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16,SHADOW_SIZE,SHADOW_SIZE, 0, GL_DEPTH_COMPONENT,GL_FLOAT,NULL);

	g_hdrColorBuffer=glGenTextureAndBind(GL_TEXTURE_2D);
	glTexFilterWrap(GL_TEXTURE_2D, GL_LINEAR, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, g_viewport.x,g_viewport.y, 0, GL_RGBA, GL_FLOAT, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,g_hdrColorBuffer,0);

	glBindFramebuffer(GL_FRAMEBUFFER,0);


}
bool g_isInit=false;
void InitRenderer(){
	CreateShaders();
	CreateQuad();
	InitRenderBuffers();

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
    glBlendFunc(GL_ONE,GL_ZERO);
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	g_isInit=true;
}
void LazyInitRenderer(){
	if (!g_isInit) InitRenderer();
}

void
RenderCtx::changeShader(GLuint shader){
	//TODO - if shadowpass..
	if (shader!=currShaderProg){
		glUseProgram(shader);
		currShaderProg=shader;
	}
}


