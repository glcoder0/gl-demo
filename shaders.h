#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include "vecmathnew.h"


struct ShaderPrograms {
	GLuint tex2Blend,tex2AlphaTex,tex2Mul2x,landscape,color,colorTex,water;
	GLuint landscapeShadowPass,defaultShadowPass;
	GLuint particle;
	GLuint debugQuad;
		// TODO..
	GLuint bloomv0,extractHighlight,blurX,blurY,downsample,ssao,edges,depthOfField,retro;

	GLuint getShadowPassShader(GLuint someShader);	// *most* use shadowPass, a few exceptions are tested.
};
extern ShaderPrograms g_ShaderPrograms;

GLuint
GFX_CompileShaderSrcs(int shaderType, const char* ppSources[], bool proceedIfFailed=false);

GLuint
GFX_CompileShaderSrc(int shaderType, const char* src);

void	CreateShaders();

GLuint	GFX_CreateShaderProgramFromSrcs(
			const char** vertexShaderSource,
			const char** pixelShaderSource);

const int MaxLights=4;
enum ShaderUniform {
	uMatProj = 0,
	uMatModelView = 4,
	uAmbient = 8,
	uAmbientDX = 9,
	uAmbientDY = 10,
	uAmbientDZ = 11,
	uEyePos = 12,
	uFogColor = 13,
	uFogFalloff = 14,
	uSunDir = 15,
	uSpecularPower = 16,
	uSunColor = 17,
	uTex0 = 18,
	uTex1 = 19,
	uTex2 = 20,
	uTex3 = 21,
	uNumLights = 22,
	uLightPos = 23,
	uLightColor = uLightPos+MaxLights, // 27
	uTexArray = uLightColor+MaxLights,//31
	uAnim = 31,
	uMatWorldToShadowBuffer=32,
	UNextFreeSlot=SHADOWMAP_CASCADES*4+uMatWorldToShadowBuffer
};
void SetUniform(GLuint index,const Matrix4& m);
void SetUniform(const GLuint index,const Vec4& v);
void glSetTextureLayerEx(int layer, GLuint texName,GLuint type);
void SetUniformLight(int index, const Vec4& posRadius, const Vec4& color);
void glSetTextureLayer(int layer, GLuint texName);

enum // must sync with shader in ... a_pos etc
{	VertexAttrIndex_pos,VertexAttrIndex_color,VertexAttrIndex_norm,VertexAttrIndex_tex0,VertexAttrIndex_tex1,VertexAttrIndex_count
};

extern Vec4 g_FogColor;

const int SHADOW_SIZE=2048;
void InitShadowMap();
void InitRenderBuffers();
const int NUM_PASSES=2;
extern GLuint g_hdrFBO;
extern GLuint g_hdrDepthBuffer;
extern GLuint g_hdrColorBuffer;
extern GLuint g_tmpfbo[3];
extern GLuint g_ShadowMapFBO[SHADOWMAP_CASCADES];
extern GLuint g_ShadowMapDepth[SHADOWMAP_CASCADES];// 1 cascades.

struct Mesh;
struct DrawReq {
	const Mesh* msh;
	Matrix4 mat;
	DrawReq(const Mesh* _msh,const Matrix4& m) :msh(_msh),mat(m){}
	// todo - in practic it will be transparent *submesh*
};
struct RenderCtx{
	CamView cv; /* renderpass, commandbuffer object, lights..*/ 
	//Matrix4 worldToShadow;
	int currShaderProg=-1;
	int pass;	//0=standard, 1=shadow, TODO - z-prepass, ...
	bool transparent=false;
	//bool shadow=false;
	bool is_shadow()const {return pass==1;}
	void changeShader(GLuint shader);
	std::vector<PointLight> lights;
	std::vector<DrawReq> transparencies;
	DirLight sun=DirLight{Vec4(0.0f,0.7071f,0.7071f,0.f),Vec4(1.1f,0.8f,0.5f,0.f)};
	//  'hemisphere light'= subset of s.h.
	Vec4 fogColor=Vec4{0.5f,0.7f,0.8f,1.f};
	Vec4 fogFalloff=	Vec4{-0.75f,1.f/512.f,0.0f,0.f};	// todo - fog could actually blend into impostor skybox background..

	Vec4 ambient=Vec4{0.1f,0.22f,0.3f,1.f};
	Vec4 ambient_dz=Vec4{0.1f,0.22f,0.3f,0.f};//Vec4(0.7*0.7071,0.6*0.7071,0.40*0.7071,0.0);
	void setMainLight(const DirLight& sun, Vec4 ambient, Vec4 ambient_dz);
	void insertLight(const PointLight& lt);
	void setLightShaderConstants(const Matrix4& objectMat,const Sphere& boundingSphere) const;
};
enum PostProcess :int{
	None,SSAO,Bloom,Edges,DepthOfField, Num
};

void RenderAllPasses(const CamView& camView,PostProcess ppMode, std::function<void(RenderCtx&)> gatherLights, std::function<void(RenderCtx& c)> renderSceneIntoPass );

void UpdateShaders(bool forceRefresh);


SDL_Window* init_sdl_gl_window(int w,int h, SDL_GLContext* pGLC);
void InitRenderer();