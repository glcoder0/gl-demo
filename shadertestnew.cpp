#include <map>
#include <optional>
#include <deque>
#include "vecmathnew.h"
#include "triangulate.h"
#include "shaders.h"
#include "shadertestnew.h"
#include "mesh.h"
#include "drawprim.h"
#include "resourcemgr.h"
#include "landscape.h"

Vec2i g_CursorPos;
std::array<int,2> g_winsize;
float g_aspect;
bool g_Pause=false;
//TO BUILD ON LUNUX:
//	clang++ -DTESTBED ldemo.cpp -std=c++1y -lGLU -lGL -lSDL2 -lSDL2_image -o gldemo
// 'TESTBED' define enables the 'main' here; it's also disabled on android.
// just include the src in another project with it's own main to use otherwise.





template<typename T>
void print(T t) { printf("type unimplmented");};
template<>
void print<const char*>(const char* str){cout<<str;}
template<>
void print<int>(int x){printf("%d ",x);}
template<>void print<float>(float f){printf("%.5f ",f);}
void print(){};
template<typename A,typename B> void print(A&a,B& b){print(a);print(b);}
template<typename A,typename B,typename C> void print(A&a,B& b,C& c){print(a);print(b);print(c);}
template<typename A,typename B,typename C,typename D> void print(const A&a,const B& b,const C& c,const D& d){print(a);print(b);print(c);print(d);}
void print(const Vec4& v){ v.dump();}
void print(const Matrix4& m){ print("{");print(m.ax,m.ay,m.az,m.aw);print("}");};
template<typename T>
void print(T* p){printf("%x ",p);}


void	TestGl_Idle();

float	angle=0.f;
Mesh*	g_pHumanoid;
Mesh*	g_pBlenderCube;
Mesh*	g_pBlenderBuilding[16];
Mesh*	g_pBlenderCity;
Mesh* g_pTriangulationTest;


PostProcess g_PostProcess=PostProcess::Bloom;





float g_angle=0.f;
int	g_frame=0;

/*

// constructs an INVERSE of the specified heading-pitch-roll, translation , to tranaform from world to camera-local coords.
// todo parameterise z/y up
// todo - from rottrans-inverse of building the camera-to-world matrix, euler can be expanded out easily.
Matrix4 MatrixCameraView_ZUp_HeadingPitchRoll(Vec4 cam_centre, float heading,float pitch,float roll) {
	return Matrix4::z_is_up() *
	 Matrix4::rotY(-roll) * Matrix4::rotX(-pitch) * Matrix4::rotZ(-heading) 
	 * Matrix4::translate(Vec4(-cam_centre.x,-cam_centre.y,-cam_centre.z,1.f));

}
*/


int g_NumTorus = 512;
struct FlyCam {
	Vec4	pos;
	Vec4	vel;
	HeadingPitchRoll hpr;
	float	hv,pv,rv;
};
FlyCam g_Cam{{0.f,0.f,20.f,1.f},{0.f,0.f,0.f,0.f}, {0.f,0.f,0.f},0.f,0.f,0.f};

inline float keyIncDec(float* val,const uint8_t* keys, uint8_t dec,uint8_t inc,  float dv){
	float acc=0.f;
	if (keys[dec]) acc-=dv;
	if (keys[inc]) acc+=dv;
	*val+=acc;
	return acc;
}


//o  +  o
//+  x  +
//o  +  o




// todo - mode - multiple obj meshes - load as seperate elements or merge, give option


// becomes 'landscape+static objects' really.


#ifdef ANDROID
struct VirtualJoypad {
    float dx0,dy0;
    float dx1,dy1;
    int buttons;
};
extern VirtualJoypad g_Joypad;
#endif
uint8_t old_keys[512];

struct  UpdateCtx{float dt;};
#define RENDERABLE 0x0001
struct Actor {	
	uint64_t flags=RENDERABLE;	// universal property bits for sort/search (collideable,etc)
	// yucky virtuals but we can extrac the common cases later.
	Actor(){}
	virtual ~Actor(){}
	Matrix4 matrix = Matrix4::identity();
	Mesh* mesh=0;

	Vec4	vel	=Vec4(0.f);
	Vec4	angvel=Vec4(0.f);
	float radius=0.f;
	int	lifetime;
	virtual void update(){};
	virtual void gatherLights(RenderCtx& rc) const{}
	virtual void render(RenderCtx& rc)const {
		if (this->mesh) {
			rc.cv.cull_test_sphere(this->matrix,mesh->sphere);
			RenderMeshAt(rc,this->mesh,this->matrix);
		}
	}
	virtual void debugDraw()const{
		g_DebugDraw.drawAxes(this->matrix,2.0f);
		if (this->mesh){
			g_DebugDraw.drawBox(this->matrix,0xffffff,this->mesh->bounding_box);
		}
	}
};

struct WorldModel : Actor {
	WorldModel(Mesh* pmesh, const Matrix4& mat ){
		this->matrix = mat;
		this->mesh=pmesh;
		this->lifetime=0x7fffffff;
	}
};

struct Walker : Actor {
	void update() {
		float groundz = g_pLandscape->getHeight(matrix.aw);
		if (matrix.aw.z <groundz){
			matrix.aw.z=groundz;
			vel.z=0.f;
			//lifetime=0;
		}
		vel.z-=0.01f;
		vel*=1.0f-0.2f; // air resistance. todo - rockets.
		matrix.aw+=vel;
		auto acc=0.02f;
		vel+=Vec4(frands(acc),frands(acc),0.f,0.f);
	}
};
struct Projectile : Actor{
	Projectile(){};
	PackedARGB color=PackedARGB(0);
	void update() {
		vel.z-=0.005f;
		float groundz = g_pLandscape->getHeight(matrix.aw);
		vel*=1.0-0.005; // air resistance. todo - rockets.
		vel+=Vec4RandomXYZ(0.002f);
		matrix.aw+=vel;
		
		if (matrix.aw.z <groundz){

			matrix.aw-=vel;	// todo - propper normal etc
			float vl=vel.length();
			vel*=-0.125f;
			vel.z+=0.25f;
			vel+=Vec4RandomXYZ(0.25f);
			matrix.aw+=vel;
			//lifetime=0;
		}
		lifetime--;
		if (vel.lengthSquared()<0.001f) lifetime=0;
	}
};
struct Tracer : Projectile {
	void render(RenderCtx& rc) const {
		auto oldpos = matrix.aw-vel;
		//g_DebugDraw.line3d(oldpos+g_Cam.vel,matrix.aw,0xffffffff);
		// 2xbrite
		g_Sprites.drawSprite(rc.cv,lerp(matrix.aw,oldpos+g_Cam.vel,frands(0.5)+0.5f),0xffffff80,0.2f,g_angle);
		g_Sprites.lineSprite(rc.cv,matrix.aw,oldpos+g_Cam.vel,0xffffff80,0.1f,g_angle);
	}
	void gatherLights(RenderCtx& rc) const {
		// todo: aproximate by summing to a grid cell light
		// 4 
		if (color.v!=0){
			
			rc.insertLight(PointLight{this->matrix.aw,50.0f,Vec4(color)});
		}
	}
};

struct Lisajous : Actor {
	Lisajous(){
		lifetime=100000;
	}
	void update(){
		g_angle+=0.0025f;
	}
	void render(RenderCtx& rc) const {
		// 10 x 10metre lisajous
		float r0 = 50.0f;
		float r1 = 50.0f;
		float lisajous_z=25.f,l_zscale=0.5f;
		float sda=0.1f;
		float a0=g_angle*1.1f+0.1f, a1=g_angle*1.09f+1.5f;
		float a2=g_angle*1.05f+0.5f, a3=g_angle*1.11f;
		float a4=g_angle*1.11f+0.7f, a5=g_angle*1.105f;
		float da0=2*PI*0.071f*sda;
		float da1=2*PI*0.042f*sda;
		float da2=2*PI*0.081f*sda;
		float da3=2*PI*0.091f*sda;
		float da4=2*PI*0.153f*sda;
		float da5=2*PI*0.1621f*sda;
		float speed=0.02f;

		for (int i=0; i<g_NumTorus; i++,a0+=da0,a1+=da1,a2+=da2,a3+=da3,a4+=da4,a5+=da5) 
		{
			auto pos=Vec4(cos(a0)*r0+cos(a3)*r1 , cos(a1)*r0+cos(a4)*r1, (cos(a2)*r0+cos(a5)*r1)*l_zscale+lisajous_z, 1.0f);
			auto mat_m = 
					Matrix4::translate(pos
					)*
					Matrix4::rotY(a1*1.1245f)*
					Matrix4::rotX(a1*1.245f) *
					Matrix4::rotZ(a0*10.95f);
			SetUniform(uMatProj, rc.cv.proj_view);//mat_pv);


			int subi=i&15;
			auto mesh=g_pBlenderBuilding[subi];//((i&3)==0)?g_pBlenderCube:g_pTorus;
			//(subi==0)?g_pBlenderCube:(subi==1)?g_pBlenderBuilding:(subi==2)?g_pTorus:g_pHumanoid;
			if (!mesh) mesh=g_pTorus;
			rc.setLightShaderConstants(mat_m, mesh->sphere);
			if (g_ShowDebugLines) {
				auto centre=mat_m*mesh->sphere.centre;
				auto cull = rc.cv.cull_test_sphere_at(centre,mesh->sphere.radius);
				g_DebugDraw.drawSphere(cull==CullResult::ALL_VIS?0xff00ff00:0xffff0000, centre, mesh->sphere.radius);
			}
			int t0=g_Textures[1+(i&15)];
			int t1=g_Textures[1+((1+i)&15)];
			if ((i&7)==0) {
				t0=g_Textures[19];
				t1=g_Textures[19];
			}
			RenderMeshTextureOveride(rc,mesh,mat_m,t0,t1);
		}
	}
};


Vec2i g_viewport;	// size of main rendering window.
std::vector<Actor*> g_Actors;
int g_alternate=0;

uint32_t g_Buttons;

int g_recycle=0;
extern Vec2i g_viewport;
void ShaderTest_Update(const CamView& c){
	int	i;

	if (g_recycle>=0) g_recycle--;
	static int index=0;// inside trcer please!
	if (g_Buttons && g_recycle<=0) {
		g_alternate^=1;
		auto* na=new Tracer();
		// 'projectile(torus) = donut gun na->mesh=g_pTorus;
		na->lifetime=256; // will yield 256 vcalls.
		g_recycle=3;  
		na->matrix = c.to_world;
		if (0==(index&7)) // only 1 in 7 have light.
			na->color=PackedARGB(0.0f,1.0f,1.0f,0.0f);
		index++;
		float muzzle_speed = 4.0f;
		Vec4 target = c.raster_pos_to_world(g_CursorPos,1000.f);
		na->matrix.aw = c.fwd()*0.5f+c.to_world.aw+c.to_world.ax * (g_alternate?1.0f:-1.0f) - c.to_world.ay*0.5f +Vec4RandomXYZ(0.01f);
		na->vel = target.sub(na->matrix.aw).normalize() * muzzle_speed+c.vel();
		g_Actors.push_back((Actor*)na);
	} else if (!g_Buttons) index=0;

	
	for (i=0; i<g_Actors.size(); i++){
		auto a=g_Actors[i];
		a->update();
	}
	
	int endi=g_Actors.size();
	for (i=0; i<endi; i++){
		while (g_Actors[i]->lifetime<=0 && i<endi) {
			delete g_Actors[i];
			g_Actors[i]=g_Actors[--endi];
		}
	}
	g_Actors.resize(endi);
}
// caution 'cursor pos' - on a touchscreen, only exists for an actual press. 'cursorButton' needs to distinguish a state where it doesn't exist

void ShaderTest_Input(const uint8_t* keys, std::array<int,2> cursorPos,int buttons) {
	auto& cam=g_Cam;
	// TODO - this is easier extracted from the camera object matrix. when we have propper vmaths lib, its all easier.
	g_CursorPos = Vec2i(cursorPos);
#ifdef ANDROID
    float accel=1.f;
    float dx0=g_Joypad.dx0;
    float dy0=g_Joypad.dy0;
    float dx1=g_Joypad.dx1;
    float dy1=g_Joypad.dy1;
    ALOGV("vjp %.3f %.3f",dx0,dy0);
#else
    float accel=keys[SDL_SCANCODE_LSHIFT]?4.f:keys[SDL_SCANCODE_LCTRL]?0.25f:1.f;
    float dx0=0,dx1=0,dy0=0,dy1=0;
#endif
	float speed=0.01f*accel;
	float rollscale=1.0f;
	float rspeed=0.0025f;
	//float sh=sin(cam.hpr.heading),ch=cos(cam.hpr.heading);
	//float sp=-sin(cam.hpr.pitch),cp=cos(cam.hpr.pitch);
	cam.hpr.roll*=0.97f;
	
	

	float dx=0.f,dz=0.f,dy=0.f; // displacement in camera local coords i.e. x=horiz, z=in/out of screen. z is up in world.

#ifndef ANDROID
	if (keys[SDL_SCANCODE_H]){
		if (g_ShowDebugLines) g_ShowDebugLines=0; 
		else g_ShowDebugLines+=512;
	}
	if (keys[SDL_SCANCODE_P]){
	}

	
	UpdateShaders(keys[SDL_SCANCODE_R]?true:false);
	


	keyIncDec(&dx,keys,SDL_SCANCODE_A,SDL_SCANCODE_D,  speed);
	keyIncDec(&dz,keys,SDL_SCANCODE_S,SDL_SCANCODE_W, speed);
	keyIncDec(&dy,keys,SDL_SCANCODE_Q,SDL_SCANCODE_E, speed);
	keyIncDec(&cam.hv,keys,SDL_SCANCODE_LEFT, SDL_SCANCODE_RIGHT, -rspeed);
	keyIncDec(&cam.rv,keys,SDL_SCANCODE_LEFT, SDL_SCANCODE_RIGHT, 0.005f);
	cam.rv+=dx*0.05f;
	cam.rv*=0.9;
	cam.hpr.roll+=cam.rv; // this is not real angvel,just euler nonsense
	keyIncDec(&cam.pv,keys,SDL_SCANCODE_UP,SDL_SCANCODE_DOWN,  rspeed);
#endif
	cam.rv-=cam.hpr.roll*0.01f;	// spring back to upright
	cam.pv*=0.9f;
	cam.hv*=0.9f;
	cam.hpr.heading+=cam.hv - dx0*0.1f;
	cam.hpr.pitch+=cam.pv + dy0*0.1f;
	dx+=dx1*0.1f;
    dz+=-dy1*0.1f;
	// dx,dy,dz =local thrust . z=into screen, y=up screen
	

	auto cam_view=Matrix4::camera_view_z_up_heading_pitch_roll(cam.pos, cam.hpr.heading,cam.hpr.pitch,cam.hpr.roll);
	auto cam_object=cam_view.invertRT();
	
	//auto cam_fwd=Vec4(-sh*cp, ch*cp,-sp,0.f);
	
	// tend to oriuent back to  direction of travel, until 10degrees off..
	float driftx=cam_object.ax.dot(cam.vel);

	// and tend to cancel drift
	cam.vel+=cam_object.ax* clamps(-driftx,0.001f);

/*	cam_vel+=Vec4(
		dx * ch - (dz*cp+dy*sp)*sh,
		dx * sh + (dz*cp+dy*sp)*ch,
		-dz * sp + dy*cp,
		0.f);
		*/
	cam.vel+= cam_object.az * dz +cam_object.ax*dx+cam_object.ay*dy;
	if (cam.hpr.pitch<-1.4f && cam.pv<0.f) cam.pv+=rspeed*2.f;
	if (cam.hpr.pitch>1.4f && cam.pv>0.f) cam.pv-=rspeed*2.f;
	cam.hpr.pitch=clamps(cam.hpr.pitch,1.5f);
	
	g_Buttons=buttons;
#ifndef ANDROID
	if (keys[SDL_SCANCODE_RETURN]) {
		printf("restore camera");
		cam.pos=Vec4(0.f,0.f,0.f,1.f);
		cam.hpr.heading=0.f;
		cam.hpr.pitch=0.f;
		cam.hpr.roll=0.f;
		cam.hv=0.f;
		cam.pv=0.f;
	}
#endif
	float cam_air_resistance=0.03f;
	cam.vel*=1.0f-cam_air_resistance;
	cam.pos+=cam.vel;

	float cam_size=2.0f;
	float floorz = g_pLandscape->rayTraceDown(cam.pos).pos.z;
	if (cam.pos.z < floorz+cam_size) cam.pos.z=floorz+cam_size;;

	// fire
		
	static int recycle=0;
	if (recycle>0) recycle--;
}
void ShaderTest_Input(const uint8_t* keys) {
	ShaderTest_Input(keys, std::array<int,2>{0,0},0);
}


void ShaderTest_RenderDebug(Matrix4& projView){
	// mouse cursor into world via projection matrix.. how?
	g_DebugDraw.drawGridXY(Matrix4::identity(),0xff808080, 20.0f, -10,10);
	g_DebugDraw.drawGridXY(Matrix4::identity(),0xffc0c0c0, 5.0f, -10,10);
	for (auto a:g_Actors){
		a->debugDraw();
	}
}




void RenderActors(RenderCtx& rc){
	for (auto a:g_Actors){
		if (a)
			a->render(rc);
	}
}
void gatherActorLights(RenderCtx& rc){
	// todo - subest of actors-
	// actor grid cull vs frustum
	for (auto a:g_Actors){
		a->gatherLights(rc);
	}
}

void RenderEverything(RenderCtx& rc){
	SetUniform(uAnim, Vec4(g_angle,0.f,0.f,0.f));

	SetUniform(uMatProj, rc.cv.proj_view);
	g_pLandscape->render(rc);

	SetUniform(uMatProj, rc.cv.proj_view);
	RenderActors(rc);
}


float g_FarClipPlane=1024.0f;
void	ShaderTest_Render() {

//	glClearColor(g_FogColor.x,g_FogColor.y,g_FogColor.z,g_FogColor.w);
//	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glFlush();
    glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);


	//
	float fov=PI/369.0;
	//auto matP=Matrix4::projection(tan(fov/2.0),g_aspect,1.f,1024.f);
	int	i;

	auto& wc=g_Cam;
	Matrix4 cam_rot_trans=Matrix4::camera_view_z_up_heading_pitch_roll(wc.pos, wc.hpr.heading,wc.hpr.pitch,wc.hpr.roll);
	Matrix4 cam_obj_mat = cam_rot_trans.invertRT();
	Matrix4 check=cam_rot_trans * cam_obj_mat;

	CamView cam;
	cam.m_viewport_size=g_viewport;
	cam.m_vel=wc.vel;
	cam.set_look_at(cam_obj_mat.aw, cam_obj_mat.aw+cam_obj_mat.az,cam_obj_mat.ay, Frustum{1.0f,g_aspect,0.5f,g_FarClipPlane});
	auto matP = cam.proj_only;
	//g_DebugDraw.drawBox(Matrix4::translate(cam.to_world.aw + cam.to_world.az*5.0),0xff00ff00,Extents4f{Vec4(-0.5,-0.5,-0.5,1.f),Vec4(0.5,0.5,0.5,1.f)});
	//g_DebugDraw.drawBox(Matrix4::translate(cam.to_world.aw + cam.to_world.az*5.0- cam.to_world.ax*5.0),0xff00ff00,Extents4f{Vec4(-0.5,-0.5,-0.5,1.f),Vec4(0.5,0.5,0.5,1.f)});

	// todo: this calculation doesn't support orthographic views.
	// in ortho, the camera 'point' is at infinity, and 1/tanfov is also infinite.
	// figure out a formulation that handles isometric seemlesly - should be possible with homogenous coords.
	// eg put the quotient of 'pixel_screen_z' into 'w', allowing it to be zero here?
	auto pixel_screen_z = g_viewport.x/2 / tan(fov/2.0); // i.e. screen_width = 2 * pixel_screen_z * tan_half_fov
	auto cursor_screen_pos=Vec4((g_CursorPos.x-g_viewport.x/2)/(float)g_viewport.x,(g_CursorPos.y-g_viewport.y/2)/(float)g_viewport.y, 1.0f,1.f);
	auto aim_point = cam_obj_mat * Vec4(-0.5,-0.5,1.0,1.0);//cursor_screen_pos;

	auto mat_pv= matP*cam_rot_trans;//Matrix4(camRotTrans);
	cam.proj_view=mat_pv; // TODO - why does the internal calc not line up??

	// 'RenderAllPasses' - takes a function to draw a pass, applies it to shadow,main,<anything else>

	
//	g_DebugDraw.drawSphere(0xff00ff00,Vec4(0.0f,0.0f,0.0f,1.0f),10.0f);
//	g_DebugDraw.drawSphere(0xff00ff00,Vec4(0.0f,0.0f,0.0f,1.0f),10.0f);
//	g_DebugDraw.drawSphere(0xff00ff00,Vec4RandomXYZ(100.f)+Vec4(0.0f,0.0f,10.0f,1.0f),10.0f);
	g_Sprites.drawSprite(cam,Vec4(-70.0f,-20.0f,20.0f,1.0f),0xff804080,10.0f,g_angle);
	g_Sprites.drawSprite(cam,Vec4(70.0f,20.0f,20.0f,1.0f),0xff808080,10.0f,g_angle);
	g_Sprites.drawSprite(cam,Vec4(50.0f,50.0f,20.0f,1.0f),0xff808080,10.0f,g_angle);
	g_Sprites.drawSprite(cam,Vec4(-50.0f,-50.0f,20.0f,1.0f),0xff408040,10.0f,g_angle);
	
		// hardcoded 4 lights
	float lx=20.f;
	float ly=20.f;
	float lz=20.f;
	float lr=50.f;

	RenderAllPasses(cam,g_PostProcess, 
		[&](RenderCtx& rc)->void{
			// TODO - traverse the scene and find
			// all the lights in the frustum
			if (false){
			rc.insertLight(
				PointLight{
					Vec4(lx,ly,lz, lr),30.0f,
					Vec4(1.0, 0.0, 0.0, 0.0)});
			rc.insertLight(
				PointLight{
					Vec4(-lx,ly,lz, lr),30.0f,
					Vec4(0.0, 1.0, 0.0, 0.0)});
			rc.insertLight(
				PointLight{
					Vec4(lx,-ly,lz, lr),30.0f,
					Vec4(0.0, 0.0, 1.0, 0.0)});
			rc.insertLight(
				PointLight{
					Vec4(-lx,-ly,lz, lr),30.f,
					Vec4(1.0, 0.0, 1.0, 0.0)});
			}
			gatherActorLights(rc);
		},

		[&](RenderCtx& rc)->void{
			SetUniform(uAnim, Vec4(g_angle,0.f,0.f,0.f));

			SetUniform(uMatProj, rc.cv.proj_view);
			g_pLandscape->render(rc);

			SetUniform(uMatProj, rc.cv.proj_view);
			RenderActors(rc);
			if (0 && rc.transparent && !rc.is_shadow()){ // ALPHA PASS
				g_DebugDraw.drawSphere(0xffffff00,Vec4(0.0f,0.0f,0.0f,1.0f),20.0f);
				g_DebugDraw.flush3d(mat_pv);
			
				g_Sprites.render(rc.cv.proj_view,
					g_Textures.get("scrap_aluminum.jpg"),
					g_Textures.get("alphanoise.tga"),
					g_Textures.get("blob.tga")
				);
			}
			//g_DebugDraw.render(rc.cv.proj_view);
		}
	);

	if (g_ShowDebugLines) {
		ShaderTest_RenderDebug(mat_pv);

		#ifdef DEBUG_CURSOR
			auto cursor_from0=cam.raster_pos_to_world(Vec2i(0,g_winsize[1]),1.0f);
			auto cursor_from1=cam.raster_pos_to_world(Vec2i(g_winsize[0],g_winsize[1]),1.0f);
			auto cursor_at=cam.raster_pos_to_world(g_CursorPos,1000.0f);
			g_DebugDraw.line3d(cursor_from0,cursor_at,0xff00ffff);
			g_DebugDraw.line3d(cursor_from1,cursor_at,0xff00ffff);
		#endif
			

		g_DebugDraw.flush3d(mat_pv);
		DrawSomeDebugLines();
	}
	g_DebugDraw.flush3d(mat_pv);
	g_Sprites.discard();
	//g_DebugDraw.drawSphere(0xff00ff00,Vec4(0.0f,0.0f,0.0f,1.0f),10.0f);
	glBindFramebuffer(GL_FRAMEBUFFER,0);

	glFlush();



	g_frame++;

	g_AsyncLoader.update();
	if (!g_Pause) {
		ShaderTest_Update(cam);	// todo - figure out best flow between input-render-update-calculate-camera
	} else {
		g_Pause=g_Pause;
	}
}


void	LoadDemoTextures() {
	// hardcoded texture loading because we're drawing random objects with random textures,
	// as well as things properly textured by blender.
	const char* names[]={"gravel_midgrey.jpg","sand_gravel.jpg","stone_bricks.jpg","wood_grain.jpg","foilage4.jpg",
			"long_grass_use.jpg","irregular_brown_stone_wall.jpg","metal_bare.jpg","grey_rock.jpg",
			"scrap_aluminum.jpg","scrap_wire.jpg","soil_gravel.jpg","rough_rusted_metal.jpg",
			"castle_wall.jpg","wood_board.jpg","rock_strata.jpg","brick_07.jpg","tech_pipes.jpg",
			"industrial_pipes_gantries.jpg","engine_plumbing.jpg","metal_bolts.jpg",
			"sea_rock.jpg","sandy_stone_wall.jpg","marble.jpg","dead_coral.tga",nullptr};
	for (const char** pptn=names;*pptn;pptn++){
		g_Textures.get(*pptn);
	};
	TRACE
}


void ShaderTest_Resize(int w,int h){
	glViewport(0.f,0.f,w,h);
	g_viewport=Vec2i(w,h);
	g_aspect=(float) w/(float) h;
}

void Init_DemoActors(){
	for (int i=0; i<0; i++){
		auto a=new Walker;
		a->matrix.aw=Vec4(frands(100.f),frands(100.f),0.f,1.f);
		a->vel=Vec4(frands(0.2f),frands(0.2f),0.f,0.f);
		a->lifetime=1280;
		a->mesh=g_pHumanoid;
		g_Actors.push_back((Actor*) a);
	}
	auto a = new Actor;
	g_Actors.push_back(new WorldModel(g_pTriangulationTest, Matrix4::translate(Vec4(0.f,0.f,200.f,0.f)) ));

	g_Actors.push_back(new WorldModel(g_pBlenderCity, 
		Matrix4(
			Vec4(1.f,0.f,0.f,0.f),
			Vec4(0.f,0.f,1.f,0.f),
			Vec4(0.f,-1.f,0.f,0.f),
			Vec4(0.f,100.f,25.f,1.f))
		
	));
	g_Actors.push_back(new Lisajous());
}

void ShaderTest_Init(){
	InitRenderer();
	LoadDemoTextures();

	//glDrawBuffer(GL_BACK);

	// init HDR framebuffer
	//glGenRenderbuffers(1,&g_hdrDepthBuffer);
	//glBindRenderbuffer(GL_RENDERBUFFER,g_hdrDepthBuffer);
	//glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT32,g_viewport.x,g_viewport.y);
	printf("init fb %dx%d\n",g_viewport.x,g_viewport.y);

    //ALOGV("making meshes..");
	g_pBlenderBuilding[0] = LoadOBJAsync("building.obj");
	g_pBlenderCity = LoadOBJAsync("city.obj");
	//g_pTriangulationTest = makeSurfaceMesh();
	g_pHumanoid = LoadIQM("humanoid0.iqm");
	g_pBlenderCube = LoadOBJAsync("cube.obj");
	g_pBlenderBuilding[1] = LoadOBJAsync("building1.obj");
	g_pBlenderBuilding[2] = LoadOBJAsync("building2.obj");
	g_pBlenderBuilding[3] = LoadOBJAsync("building3.obj");
	g_pBlenderBuilding[4] = LoadOBJAsync("building4.obj");
	g_pBlenderBuilding[5] = LoadOBJAsync("building5.obj");
	g_pBlenderBuilding[6] = LoadOBJAsync("building6.obj");
	g_pBlenderBuilding[7] = LoadOBJAsync("building7.obj");
	g_pBlenderBuilding[8] = LoadOBJAsync("fastback_car.obj");
	g_pBlenderBuilding[9] = LoadOBJAsync("aircraft.obj");
	g_pBlenderBuilding[10] = LoadOBJAsync("tracked_vehicle.obj");
	g_pBlenderBuilding[11] = LoadOBJAsync("dinner_chair.obj");
	g_pBlenderBuilding[12] = LoadOBJAsync("office_chair.obj");
	g_pBlenderBuilding[13] = LoadOBJAsync("fighter_jet_f16.obj");

    const char* landscape_tex[]={
   		"gravel_midgrey.jpg", //microtexture horiz/vert
        "long_grass_use.jpg",
		"cliffs.jpg", // lowland horiz/vert
        "sand_gravel.jpg",
		"rocktile.jpg", // highland horiz/vert.
        // spare, unused,TODO , cell control..
        "rock_strata.jpg","gravel_midgrey.jpg","mid_grey_rock.jpg",nullptr
   	};

	g_pLandscape = MakeLandscape(1024,2.0f,1024*2, landscape_tex,nullptr);

	Init_DemoActors();

	//ALOGV("making meshes.. done");
}


int SDL_main(int argc, char* argv[])
{
	Vec4 v0=Vec4(2.0,3.0,4.0,1.0);
	Vec4 v2=v0.zero_elems<0,1,0,0>();
	printf("v2=%.3f %.3f %.3f %.3f\n", v2.x,v2.y,v2.z,v2.w);
	SDL_GLContext glc;
	auto wnd=init_sdl_gl_window(0,0,&glc);
	int w,h;
	SDL_GetWindowSize(wnd,&w,&h);
	SDL_ShowWindow(wnd);

	ShaderTest_Resize(w,h);
	ShaderTest_Init();

	int frame=0;
	std::array<int,2> cursorPos={0,0};

	while (1){
        SDL_Event e;
		
        while(SDL_PollEvent(&e))
        {
            if(e.type == SDL_QUIT) std::terminate();
			if(e.type== SDL_MOUSEMOTION){
				cursorPos={e.motion.x,e.motion.y};
			}
			if (e.type==SDL_KEYDOWN){
				switch (e.key.keysym.sym) {
				case SDLK_o:
					g_PostProcess = static_cast<PostProcess>( (int)(g_PostProcess+1)%(int)PostProcess::Num);
				break;
				case SDLK_h:
					g_ShowDebugLines^=1;
				break;
				case SDLK_p:
					g_Pause^=1;
					printf(g_Pause?"Paused\n":"Unpaused\n");
				break;
				}
			}
        }
		auto buttons=SDL_GetMouseState(&cursorPos[0],&cursorPos[1]);
		auto keys=SDL_GetKeyboardState(nullptr);
		if (keys[SDL_SCANCODE_SPACE]) buttons|=1;

  		if (keys[SDL_SCANCODE_ESCAPE]) std::terminate();
  		
		ShaderTest_Input(keys,cursorPos,buttons);

		// draw the torus lisajous
		//ShaderTest_Update();
		ShaderTest_Render();

		frame++;
		
        SDL_GL_SwapWindow(wnd);
	}
}
