#pragma once
#include <stdio.h>
#include <map>
#include <optional>
#include <deque>

#include "vecmathnew.h"
#include "triangulate.h"
#include "shaders.h"
#include "textures.h"
extern Vec2i g_CursorPos;
extern std::array<int,2> g_winsize;
extern float g_aspect;

void SetUniform(const GLuint index,const Vec4& v);
void SetUniform(GLuint index,const Matrix4& m);
void CreateTextures();
void CreateQuad();
void InitShadowMap();

// temporary globals.. TODO package them up..
extern GLuint g_QuadVAO;
extern GLuint g_QuadVBO;

extern GLuint g_hdrFBO;
extern GLuint g_hdrDepthBuffer;
extern GLuint g_hdrColorBuffer;
extern GLuint g_tmpfbo[3];
extern GLuint g_ShadowMapFBO[SHADOWMAP_CASCADES];
extern GLuint g_ShadowMapDepth[SHADOWMAP_CASCADES];// 1 cascades.
extern GLuint g_TextureArray;
extern Vec2i g_viewport;	// size of main rendering window.
struct Mesh;
extern Mesh*	g_pTorus;
extern Mesh*	g_pHumanoid;
extern Mesh*	g_pBlenderCube;
extern Mesh*	g_pBlenderBuilding[];
extern Mesh*	g_pBlenderCity;
extern Mesh* g_pTriangulationTest;
Mesh* LoadOBJ(const char* fname);

struct RenderCtx;
struct RenderNode;
struct Landscape;
Landscape* MakeLandscape(int side,float cellSize,int numObjects);
Mesh* LoadIQM(const char* f);
Mesh* MeshMakeTorus(int,int);
void Init_DemoActors();

extern Landscape* g_pLandscape; // cpu side representation only.



