#include "textures.h"

void glTexFilterWrap(GLuint target, GLuint filter, GLuint wrap){
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filter);

	glTexParameteri(target, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, wrap);
}

GLuint glGenTextureAndBind(GLuint type){
	GLuint ret; glGenTextures(1,&ret); 
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(type,ret); 
	return ret;
}
// framebuffers more related to textures.
GLuint glGenFramebufferAndBind(){
	GLuint fbo;
	glGenFramebuffers(1,&fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	return fbo;
}


GLuint GuessFormatForBytesPerPixel(int bpp){
	// todo: 16bit texture mode, and most importantly S3TC/DXT COPRESSION
	if (bpp==4) return GL_RGBA;
	else if (bpp==3) return GL_RGB;
	ERROR("pixel format not supported %d",bpp);

}

void ImageDownsampleByHalfInPlace(uint8_t* pixels, Vec2i size, int channels)
{
	int	i;
	int	j;
	int	k;
	auto d=pixels,s=pixels;
	auto c=channels;

	for (j=0; j<(size.y-1); j+=2) {
		for (i=0; i<(size.x-1); i+=2) {
			for (k=0; k<channels; k++) {
				d[k]=(unsigned char)(((int) s[k+c*0]+(int)s[k+c*1]+(int)s[k+c*size.x]+(int)s[k+c*(1+size.x)])/4);
			}
			d+=channels;
			s+=channels*2;
		}
		s+=c*size.x;
	}
}




uint8_t* tgaCreateImageFromMemfile(const uint8_t* data,int* pwidth,int* pheight,int* pbypp){
    auto    hdr = (TGA_Header*) data;
    auto    w = Combine(hdr->widthlo,hdr->widthhi);
    auto    h =Combine(hdr->heightlo,hdr->heighthi);
    auto    x0 =Combine(hdr->xstartlo,hdr->xstarthi);
    auto    y0 =Combine(hdr->ystartlo,hdr->ystarthi);
    int	ofs=0;
    //TX_LOGI("load TGA  w=%d h=%d type=%d x0,y0=%d,%d\n", w,h, (int)hdr->imagetype,x0,y0);

//    if (hdr->imagetype!=TGA_UncompressedRGB)
//    {
//        LOGI("Can't Load Image Type %d\n",(int)hdr->imagetype);
//    }

    if (hdr->imagetype==TGA_UncompressedRGB){
		int bypp=hdr->bits/8;
		auto sz=w*h*bypp;
        auto    srcBuffer = (hdr->identsize+(uint8_t*) (hdr+1));
		auto *dstbuffer = (uint8_t*) malloc(sz);
        //internalFormat, dataFormat);
		memcpy(dstbuffer,srcBuffer,sz);
		*pwidth=w;*pheight=h;*pbypp=bypp;
		return dstbuffer;
    } else if (hdr->imagetype==TGA_UncompressedClut){
		int bypp=hdr->colormapbits/8;
        auto numColors=Combine(hdr->colormaplenlo,hdr->colormaplenhi);
        LOGI("tga colormap colormapLen:%d paletteBits:%d imageBits:%d\n",numColors,hdr->colormapbits,hdr->bits);
		if (hdr->bits!=8) {
			LOGE("currently only support loading 256 color indexed color TGAs (got %d bpp)\n",hdr->bits);
			exit(0);
		}
		// depaletize..
		int j,i;
		auto sz = w*h*bypp;
		auto dstBuffer = (uint8_t*)malloc(sz);
		auto* colorMap = (uint8_t*)(hdr->identsize+(uint8_t*) (hdr+1));
		auto* srcPixels = colorMap+numColors*bypp;
		auto* src=srcPixels;
		auto* dst=dstBuffer;
		int	k;
		for (j=0; j<h; j++){
			for (i=0; i<w; i++){
				auto ci=*src++;
				auto* sp=&colorMap[ci*bypp];;
				if (bypp==3){
					dst[2]=sp[0];
					dst[1]=sp[1];
					dst[0]=sp[2];
				} else if (bypp==4){
					dst[2]=sp[0];
					dst[1]=sp[1];
					dst[0]=sp[2];
					dst[3]=sp[3];
				}
				dst+=bypp;
			}
		}
		*pwidth=w;*pheight=h;*pbypp=bypp;
		return dstBuffer;
    } else {
		LOGE("can't load TGA image type %d",hdr->imagetype)
		return nullptr;
	}
}


uint8_t* LoadTextureRaw(const char* fname,Vec2i* size,int* bytepp) {
	uint8_t* texBuffer=nullptr;
	int channels;
	size_t fileSize;
	// todo - fill more threads.... loader, decompressor on different threads..
	auto fileBuffer = loadAndAllocResourceFile(fname, &fileSize);
	if (!fileBuffer) {
		LOGI("could not load %s", fname);
		return nullptr;
	} else {
		auto ext=getFilenameExt(fname);
		printf("ext=%s\n",ext);
		if (cmpupr(ext,"JPG")) {
			texBuffer = jpgd::decompress_jpeg_image_from_memory(fileBuffer, fileSize, &size->x, &size->y, bytepp, 3);
		} else if (cmpupr(ext,"TGA")){
			texBuffer = tgaCreateImageFromMemfile(fileBuffer,&size->x,&size->y,bytepp);
		} else {
			LOGE("format not supported %s",ext);
			exit(0);
		}
	}
	free(fileBuffer);
	return texBuffer;
}
void  ReInitTextureFromRawMem(GLuint existingTexHandle,uint8_t* buffer,Vec2i size, int bytepp) {
	LOGI("InitTexFromRaw Mem: %dx%d (%d) %p\n",size.x,size.y,bytepp,buffer);
	auto mode=GuessFormatForBytesPerPixel(bytepp);
	glBindTexture(GL_TEXTURE_2D,existingTexHandle);
	
	int sx=size.x; int sy=size.y;
	int mipLevel=0;
	glTexImage2D(GL_TEXTURE_2D, mipLevel, mode, size.x,size.y, 0, mode, GL_UNSIGNED_BYTE, buffer);
	glTexFilterWrap(GL_TEXTURE_2D,GL_LINEAR_MIPMAP_LINEAR,GL_REPEAT);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,0);
	LOGI("INITIALIZED TEXTURE %d",existingTexHandle);
}
GLuint InitTextureFromRawMem(uint8_t* buffer, Vec2i size,int bytepp){
	auto tx=glGenTextureAndBind(GL_TEXTURE_2D);
	ReInitTextureFromRawMem(tx,buffer,size,bytepp);
	return tx;
}


GLuint LoadTextureNowMaybe(const char* fname){
	GLuint texId=0;
	Vec2i size; int bytepp;
	auto buffer=LoadTextureRaw(fname,&size,&bytepp);
	if (!buffer){
		return 0;
	}
	texId = InitTextureFromRawMem(buffer,size,bytepp);
	free(buffer);
	printf("loaded %d as %s\n",texId,fname);
	return texId;
};
GLuint LoadTextureNow(const char* fname){
	GLuint ret=LoadTextureNowMaybe(fname);
	if (!ret) {
		LOGE("could not load %s",fname);
	}
	return ret;
}


GLuint CreateTextureArray(Vec2i size, int numSlices,int bytepp, bool wrap, std::function<void(int slice, uint8_t* dst)> fillSliceTexels){
	LOGI("create tex array %d x %d (x%d)",size.x,size.y,bytepp);
	//auto tex=glGenTextureAndBind(GL_TEXTURE_2D_ARRAY);
	GLuint texid=glGenTextureAndBind(GL_TEXTURE_2D_ARRAY);
	auto wmode=wrap?GL_REPEAT:GL_CLAMP_TO_EDGE;
	GLCHECK
	TRACE;
	glTexFilterWrap(GL_TEXTURE_2D_ARRAY,GL_LINEAR_MIPMAP_LINEAR ,wmode);
	GLCHECK
	auto fmt=GuessFormatForBytesPerPixel(bytepp);
	int numMips = (size.x>16 && size.y>16)?4:1; // fallback 
	TRACE
	glTexImage3D(GL_TEXTURE_2D_ARRAY,0,fmt,size.x,size.y,numSlices,0,fmt,GL_UNSIGNED_BYTE,nullptr);
	//glTexStorage3D(GL_TEXTURE_2D_ARRAY, numMips,bytepp=4?GL_RGBA8:GL_RGB8,size.x,size.y,numSlices);
	GLCHECK
	TRACE

	LOGI("TEXARRAY=%d",texid);
	auto sz=size.area()*bytepp;
	LOGI("fill slices: %d bsize=%d",numSlices,sz);
	auto buffer=(uint8_t*)malloc(sz);
	for (int slice=0; slice<numSlices; slice++){
		fillSliceTexels(slice, buffer);
		glTexSubImage3D(
			GL_TEXTURE_2D_ARRAY,
			0, 
			0,0,slice,	// offset3d
			size.x,size.y,1, //size3d to copy
			fmt,GL_UNSIGNED_BYTE,buffer);
		GLCHECK
	}
	glGenerateMipmap(GL_TEXTURE_2D_ARRAY);

	free(buffer);
	return texid;
}

void scaleImage(uint8_t* dst,uint8_t* src,int channels, Vec2i dstSize, Vec2i srcSize){
	// todo - filter, min/max = different codepaths
	int dstStride = channels * dstSize.x;
	int srcStride = channels * srcSize.x;
	int i,j,k;
	for (j=0; j<dstSize.y; j++){
		auto srcj=(j*srcSize.y)/dstSize.y;
		for (i=0; i<dstSize.x; i++){
			auto srci=(i*srcSize.x)/dstSize.x;
			for (k=0; k<channels;k++){
				dst[j*dstStride+i*channels+k] = src[srci*channels+srcj*srcStride+k];
			}
		}
	}
}

GLuint LoadTextureArray(Vec2i size,int bytepp,const char **ztlsFilenames) {
	// todo - guess the size by loading them first. use median of batch.
	// as it stands we need to manually manage resolution
	const char **fptr=ztlsFilenames;
	int numTex=0;
	while (*fptr++) numTex++;
	
	ASSERT(bytepp==3 && "todo - support other formats for array textures")
	auto texid=CreateTextureArray(size, numTex, 3,true,
		[&](int slice, uint8_t* dstBuffer){ // lambda to initialize each slice..
			LOGI("load slice %d from %s",slice,ztlsFilenames[slice]);
			Vec2i imgSize; int imgByPP;
			auto srcBuffer = LoadTextureRaw(ztlsFilenames[slice],&imgSize,&imgByPP);
			ASSERT(bytepp == imgByPP)
			LOGI("got %d-%s =  %dx%d (%d bypp)",slice,ztlsFilenames[slice],imgSize.x,imgSize.y,imgByPP);
			if (srcBuffer){
				scaleImage(dstBuffer,srcBuffer,bytepp, size, imgSize);
				free(srcBuffer);
			}
		}
	);
	return texid;
}


void CreateDummyTextureSub(GLuint texId, int size, uint32_t maincol, uint32_t variance) {
	float imgr[256][256];
	float imgg[256][256];
	float imgb[256][256];
	midpoint_displacement_fractal(&imgr[0][0], 256,0, 0.f, 0, 1.0,0.55, 0xff532412);
	midpoint_displacement_fractal(&imgg[0][0], 256,0, 0.f, 0, 1.0,0.55, 0xff532412);
	midpoint_displacement_fractal(&imgb[0][0], 256,0, 0.f, 0, 1.0,0.55, 0xff532412);

	int	usize=256,vsize=256;
	auto buffer = (uint32_t*)malloc(usize*vsize*sizeof(uint32_t));
	int	i,j;
	auto dst= buffer;
	float s=10.0f;
	for (j=0; j<vsize; j++) {
		for (i=0; i<usize;i++) {
			int r=sin(imgr[j][i]*s)*127.0+127.f;
			int g=sin(imgg[j][i]*s)*127.0+127.f;
			int b=sin(imgb[j][i]*s)*127.0+127.f;
			dst[i+j*usize] = r+(g<<8)|(b<<16)|0xff000000;
		}
	}
	int	m;
	// todo -miplevels
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, usize,vsize, 0, GL_RGBA, GL_UNSIGNED_BYTE,buffer);
	glGenerateMipmap(GL_TEXTURE_2D);

	free(buffer);
}
GLuint CreateDummyTexture() {
	GLuint texId=glGenTextureAndBind(GL_TEXTURE_2D);
	//glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glTexFilterWrap(GL_TEXTURE_2D,GL_LINEAR_MIPMAP_LINEAR ,GL_REPEAT);
	CreateDummyTextureSub(texId,256,0x80808080,0x80808080);
	glBindTexture(GL_TEXTURE_2D,0);
	return texId;
}
