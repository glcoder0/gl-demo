#pragma once
#include "vecmathnew.h"
#define GL_GLEXT_PROTOTYPES
#include <SDL.h>
#include <SDL_opengl.h>


enum TGA_DataType {
    TGA_Nop=0,//  -	 No image data included.
    TGA_UncompressedClut=1,	   //Uncompressed, color-mapped images.
    TGA_UncompressedRGB=2,	  //Uncompressed, RGB images.
    TGA_UncompressedGreyScale=3,	//Uncompressed, black and white images.
    TGA_RLEClut=9,	  //Runlength encoded color-mapped images.
    TGA_RLERGB=10,	  //Runlength encoded RGB images.
    TGA_CompressedGreyScale=11,	   //Compressed, black and white images.
    TGA_CompressedClut=32,	  //Compressed color-mapped data, using Huffman, Delta, and
    //runlength encoding.
            TGA_CompressedClut2=33	//-	 Compressed color-mapped data, using Huffman, Delta, and
    //runlength encoding.  4-pass quadtree-type process.
};
inline int Combine(unsigned char lo,unsigned char hi) {return (((int)lo)+256*((int)hi));}

struct TGA_Header
{
    unsigned char	 identsize;
    unsigned char	 colourmaptype;
    uint8_t	imagetype;

    unsigned char	 colormapstartlo;
    unsigned char	 colormapstarthi;
    int colormapStart() { return Combine(colormapstartlo,colormapstarthi);}
    unsigned char	 colormaplenlo;
    unsigned char	 colormaplenhi;
    int colormapLenfth() { return Combine(colormaplenlo,colormaplenhi);}
    unsigned char	 colormapbits;

    unsigned char	 xstartlo;
    unsigned char	 xstarthi;
    unsigned char	 ystartlo;
    unsigned char	 ystarthi;
    unsigned char	 widthlo;
    unsigned char	 widthhi;
    unsigned char	 heightlo;
    unsigned char	 heighthi;
    unsigned char	 bits;
    unsigned char	 descriptor;
};

void glTexFilterWrap(GLuint target, GLuint filter, GLuint wrap);
GLuint glGenTextureAndBind(GLuint type);
GLuint glGenFramebufferAndBind();

GLuint GuessFormatForBytesPerPixel(int bpp);
void ImageDownsampleByHalfInPlace(uint8_t* pixels, Vec2i size, int channels);

#ifndef __ANDROID__
uint8_t* loadAndAllocResourceFile(const char* f, size_t* sizeOut);
#endif
uint8_t* tgaCreateImageFromMemfile(const uint8_t* data,int* pwidth,int* pheight,int* pbypp);
uint8_t* LoadTextureRaw(const char* fname,Vec2i* size,int* bytepp) ;
void  ReInitTextureFromRawMem(GLuint existingTexHandle,uint8_t* buffer,Vec2i size, int bytepp);
GLuint InitTextureFromRawMem(uint8_t* buffer, Vec2i size,int bytepp);
GLuint LoadTextureNowMaybe(const char* fname);
GLuint LoadTextureNow(const char* fname);
GLuint CreateTextureArray(Vec2i size, int numSlices,int bytepp, bool wrap, std::function<void(int slice, uint8_t* dst)> fillSliceTexels);
void scaleImage(uint8_t* dst,uint8_t* src,int channels, Vec2i dstSize, Vec2i srcSize);
GLuint LoadTextureArray(Vec2i size,int bytepp,const char **ztlsFilenames);
GLuint LoadTextureAsync(const char* fname);
GLuint glGenFramebufferAndBind();
void CreateDummyTextureSub(GLuint texId, int size, uint32_t maincol, uint32_t variance);
GLuint CreateDummyTexture();
