#include "SDL.h"
#include "triangulate.h"


void insertArcVertices(Triangulation& trm, Vec3f centre,uint32_t id, float radius, int steps,float a0,float a1){
    int i;float a=a0,da=(a1-a0)/steps;
    for (i=0,a=0; i<steps; i++,a+=da){
        trm.insertVertex(centre+Vec3(cos(a)*radius,sin(a)*radius,0.0f),id);
    }
}

void insertCircleVertices(Triangulation& trm, Vec3f centre,uint32_t id, float radius, int steps){
    float da = PI*2.f/(float) steps;


    
    insertArcVertices(trm,centre,id,radius,steps,0.f,2.0f*M_PI);
}

Triangulation* makeSurfaceDemo(int numCircles){
    auto trm = new Triangulation;
    trm->insertVertex(Vec3(512,512, 50.0),0);
    trm->insertVertex(Vec3(256,512, -50.0),0);
    for (int i=0; i<numCircles; i++) {
        // trm->insertVertex(Vec3f(rand()%1024,rand()%1024,0.f),0);
        float r0=(float)(rand()%128)+64;
        auto pt=Vec3f((float)(rand()%1024),float(rand()%1024),0.f);
        auto ht = float(rand()%32)+16.0f;
        insertCircleVertices(*trm,pt,0,r0,16);
        insertCircleVertices(*trm,pt+Vec3f(0.f,0.f,ht),0,r0*0.8f,16);
        //insertCircleVertices(*trm,pt+Vec3f(0.f,0.f,ht),0,r0*0.7f,16);
        //insertCircleVertices(*trm,pt,0,r0*0.6f,16);
    }
    return trm;
}





void SDL_DrawLine(SDL_Renderer* r, const Vec3f& a, const Vec3f& b){
    SDL_RenderDrawLine(r, (int)a.x,(int)a.y, (int)b.x,(int)b.y);
}
void drawTri(SDL_Renderer* sr,Triangulation* trm, Triangulation::Triangle* tri) {
    if (!tri->hasSplit()){
        for  (int i=0; i<3; i++){
            SDL_DrawLine(sr,tri->vertex[i]->pos,tri->vertex[(i+1)==3?0:i+1]->pos);
        }
    } else {
        for (int i=0; i<3; i++){
            if (auto* subt=tri->sub[i]) drawTri(sr,trm,subt);
        }
    }
}

#ifdef MAIN_triangulate
int main(int argc, const char** argv)
{
    std::vector<std::array<int,3>> pos;
    printf("sizeof elem %lu\n",sizeof(pos[0]));
    
    pos.push_back(std::array<int,3>{{1,2,3}});
    printf("%d %d %d\n",pos[0][0],pos[0][1],pos[0][2]);
    for (auto& v:pos[0]) {printf("%d\n",v );}
	SDL_Init(SDL_INIT_EVERYTHING);
    SDL_Window* win = NULL;
    SDL_Renderer* sr = NULL;

    SDL_CreateWindowAndRenderer(1024, 1024, 0, &win, &sr);
    Triangulation trm;
    int num=0;

    if (0)   {
        int total=1024*1024;
        for (int i=0; i<total; i++){
            trm.insertVertex(Vec3f(rand()%1024,rand()%1024,0.f),i);
//            if (1023==(i&1023)) 
//                printf("vertices=%d\n",i);
        }
        int no=0;
        for (auto* p=trm.vertices; p; p=p->next){
            no++;
        }
        printf("done %d/%d total\n",no,total);
        exit(0);

    }
    //trm.subdivRootQuad(3,0,0,1024,1024, 0);
   	while (1){
        SDL_Event e;
        while(SDL_PollEvent(&e)){
            if(e.type == SDL_QUIT) std::terminate();
        }
        if (num<64) {
            //trm.insertVertex(Vec3f(rand()%1024,rand()%1024,0.f),num);
            float r0=(float)(rand()%128)+64;
            auto pt=Vec3f((float)(rand()%1024),float(rand()%1024),0.f);
            auto ht = float(rand()%16)+8.0f;
            insertCircleVertices(trm,pt,0,r0,16);
            insertCircleVertices(trm,pt+Vec3f(0.f,0.f,ht*0.25f),0,r0*0.8f,16);
            insertCircleVertices(trm,pt+Vec3f(0.f,0.f,0),0,r0*0.6f,16);
        }
        printf("num vertices=%d\n",num);
        SDL_SetRenderDrawColor(sr,0,0,0,255);
        SDL_RenderClear(sr);
        SDL_SetRenderDrawColor(sr,128,128,128,255);
        
        //if (num>=256)
        //drawTri(sr,&trm,trm.root_triangle);
        for (auto tri=trm.all_triangles; tri; tri=tri->next){
            if (tri->hasSplit()) continue;
            for (int i=0; i<3; i++) {
                auto ii=(i+1)%3;
                SDL_DrawLine(sr,tri->vertex[i]->pos,tri->vertex[ii]->pos);
            }

        }
/*        trm.for_each_leaf_tri(trm.root_triangle,[&](auto trm,auto tri){
            for (int i=0; i<3; i++) {
                auto ii=(i+1)%3;
//                SDL_DrawLine(sr,tri->vertex[i]->pos,tri->vertex[ii]->pos);
            }
            
        });
  */      
        
        SDL_SetRenderDrawColor(sr,255,255,255,255);
        for (auto v=trm.vertices; v; v=v->next){
            int dr=1;
            auto p=SDL_Rect{(int)v->pos.x-dr,(int)v->pos.y-dr,1+dr*2,1+dr*2};
            SDL_RenderDrawRect(sr,&p);
            // SDL_DrawLine(sr,Vec3f(0.0,0.0,0.0),v->pos);
        }
        num++;

        SDL_RenderPresent(sr);
    }
}
#endif
