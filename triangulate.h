#pragma once
#include "vecmathnew.h"

#define TRI_WARN(X,...) 
struct Triangulation{
    struct Triangle;
    struct Vertex;
    struct Triangle {
        Vertex* vertex[3]={nullptr,nullptr,nullptr};
        Triangle* sub[3]={nullptr,nullptr,nullptr}; // divides into up to 3.
        Triangle* edge[3]={nullptr,nullptr,nullptr};
        // todo - hard-edge flags.
        Triangle(Vertex* v0,Vertex* v1,Vertex* v2){vertex[0]=v0; vertex[1]=v1;vertex[2]=v2;};
        Triangle* next=nullptr;// all tris.
        int index=0;
        bool hasVertexID(uint32_t id){
            for (int k=0; k<3; k++){if (vertex[k]->id==id) return true;}
            return false;
        }
        bool hasGotVertex(Vertex* vs){
            return (vertex[0]==vs||vertex[1]==vs||vertex[2]==vs);
        }
        Vertex* nextVertex(Vertex* vs){
            if (vertex[0]==vs) return vertex[1];
            if (vertex[1]==vs) return vertex[2];
            if (vertex[2]==vs) return vertex[0];
            return nullptr;

        }
        bool hasVertex(Vertex* v)const {
            return (vertex[0]==v || vertex[1]==v || vertex[2]==v);
            
        }
        void verifyHasEdge(Vertex* vs,Vertex *ve)const{
            if (!(hasVertex(vs)&&hasVertex(ve))){
                printf("requesting edge that doesn't exist");
                exit(0);
            }
        }
        Vertex* getOtherVertex(Vertex* vs,Vertex* ve){
            verifyHasEdge(vs,ve);
            // TODO - vertify vs,ve are both here
            if (vertex[0]!= vs && vertex[0]!=ve) {return vertex[0];}
            if (vertex[1]!= vs && vertex[1]!=ve) {return vertex[1];}
            if (vertex[2]!= vs && vertex[2]!=ve) {return vertex[2];}
            printf("wtf tri %p %p %p  (asked %p %p) degerate tri?\n",vertex[0],vertex[1],vertex[2], vs,ve);
            return nullptr;
        }
        Triangle* getOtherTriangle(Vertex* vs,Vertex* ve){
            verifyHasEdge(vs,ve);
            for (int i=0; i<2; i++) {
                if (vertex[0]== vs && vertex[1]==ve) {return edge[0];}
                if (vertex[1]== vs && vertex[2]==ve) {return edge[1];}
                if (vertex[2]== vs && vertex[0]==ve) {return edge[2];}
                auto swp=vs;vs=ve;ve=swp;
            }
            return nullptr;

        }
        bool hasSplit()const{
            return this->sub[0]||this->sub[1]||this->sub[2];
        }
        int getTriEdgeI(Vertex* vs,Vertex* ve){
            for (int i=0; i<3; i++) {
                int ii=(i+1)%3;
                if ((vertex[i]==vs && vertex[ii]==ve) ||(vertex[i]==ve && vertex[ii]==vs)){
                    return i;
                }
            }
            return -1;
        }
        Triangle* getTriEdge(Vertex* vs,Vertex* ve){
            auto i=getTriEdgeI(vs,ve); 
            if (i>=0){return this->edge[i];}
            else return nullptr;
        }
/*        Triangle* replaceTriEdge(Vertex* vs,Vertex* ve, Triangle* other){
            auto i=getTriEdgeI(vs,ve); 
            if (i>=0){auto prev=this->edge[i]; this->edge[i]=other; return prev;}
            else return nullptr;    // should be error!
        }
*/
        bool setTriEdge(Vertex* vs,Vertex* ve, Triangle* other){
            for (int i=0; i<3; i++){
                int ii=(i+1)%3;
                if ((vertex[i]==vs && vertex[ii]==ve) ||
                    (vertex[i]==ve && vertex[ii]==vs)) {
                    edge[i]=other;
                    return true;
                }
            }
            return false;
        //    this->replaceTriEdge(vs,ve,other);
        }
        void setTriEdges(Vertex* v0,Vertex* v1,Vertex* v2, Triangle* t0,Triangle* t1,Triangle* t2){
            ASSERT(this->setTriEdge(v0,v1, t0)); 
            ASSERT(this->setTriEdge(v1,v2,t1)); 
            ASSERT(this->setTriEdge(v2,v0,t2));
        }
    };
    void connectTriEdges(Vertex* v0, Vertex* v1, Triangle* inside, Triangle* outside) {
        if (inside)
            inside->setTriEdge(v0,v1,outside);
        if (outside)
            outside->setTriEdge(v1,v0,inside);
    }
    struct Vertex{
        Vec3f pos;
        uint32_t id;
        Vertex* next=nullptr;
        int index=0;
    };

    // should include "hard edges", variation of classic delaunay
    Triangle* root_triangle=nullptr;
    Triangle* all_triangles=nullptr;
    Vertex* vertices=nullptr;
    int numVertices=0;
    void subdivRootQuadSub(int maxDepth, Vec3f v0,Vec3f v1, Vec3f v2,Vec3f v3,uint32_t id){
        if (maxDepth<=0)
            return;
        auto v01=(v0+v1)*0.5f;
        auto v23=(v2+v3)*0.5f;
        auto v02=(v0+v2)*0.5f;
        auto v13=(v1+v3)*0.5f;

        auto centre=(v01+v23)*0.5f;
        insertVertex(centre,id);
        maxDepth--;
        //  v0    v01   v1
        //
        //  v02  ce    v13
        // 
        //  v2    v23    v3
        subdivRootQuadSub(maxDepth, v0,v01, v02,centre, id);
        subdivRootQuadSub(maxDepth, v01,v1, v02,centre, id);
        subdivRootQuadSub(maxDepth, v02,centre, v2,v23, id);
        subdivRootQuadSub(maxDepth, centre,v13, v23,v3, id);
    }
    void subdivRootQuad(int maxDepth, float x0,float y0, float x1,float y1,uint32_t id){
        subdivRootQuadSub(maxDepth, Vec3f(x0,y0,0.f),Vec3f(x1,y0,0.f),Vec3f(x0,y1,0.f),Vec3f(x1,y1,0.f),id);
    }
    Triangulation(){
        float s=1024000.0f;
        float cx=512.0,cy=512.0;
        auto v0=new Vertex{Vec3f(cx-s,cy-s,0.f),0xffffffff,nullptr};
        auto v1=new Vertex{Vec3f(cx+s,cy-s,0.f),0xffffffff,v0};
        auto v2=new Vertex{Vec3f(cx,cy+s,0.f),0xffffffff,v1};
        //auto v3=new Vertex{Vec3f(s,s,0.f),v2};
        this->vertices=v2;
        root_triangle=MakeTriangle(v0,v1,v2);
//        tr=new Triangle(v2,v1,v3);
        //tr->next=triangles;
        //triangles=tr;
    }
    Triangle *MakeTriangle(Vertex* v0,Vertex* v1,Vertex* v2){
        if (v0==v1 || v0==v2 || v1==v2){
            printf("rying to create degenerate tri\n");
            return nullptr;
        }
        auto tri=new Triangle(v0,v1,v2);
        tri->next=all_triangles;
        all_triangles=tri;
        return tri;
    }
    Vec3 getValueAt(Vec3 pt){
        auto tri=find_tri_at(root_triangle, pt, 0);
        // barycentric..
        // HACK :(
        auto v0=tri->vertex[0];
        auto v1=tri->vertex[1];
        auto v2=tri->vertex[2];
        float s01=signedAreaXY(pt,v0->pos,v1->pos);
        float s12=signedAreaXY(pt,v1->pos,v2->pos);
        float s20=signedAreaXY(pt,v2->pos,v0->pos);
        float area=(s01+s12+s20);
        // TODO - generalize whats being interpolated. change it from Vec3 pt tp {Vec2 xy, T surfaceValue} which can be hieght, parameters,with indexed compression (==ID..)
        //  1
        //    x
        //  0    2

        float z = v0->pos.z +
                (s20*(v1->pos.z-v0->pos.z) + s01*(v2->pos.z-v0->pos.z))/area;
        return Vec3(pt.x,pt.y,z);
    }

    Triangle* find_tri_at(Triangle* tri,Vec3 pt,int depth){
        const float epsilon=0.00001f;
        
        float s0=signedAreaXY(pt,tri->vertex[0]->pos,tri->vertex[1]->pos);
        float s1=signedAreaXY(pt,tri->vertex[1]->pos,tri->vertex[2]->pos);
        float s2=signedAreaXY(pt,tri->vertex[2]->pos,tri->vertex[0]->pos);
        if ((s0>=-epsilon && s1>=-epsilon && s2>=-epsilon) ||    // todo- supposed to be one or the other..
            (s0<=epsilon && s1<=epsilon && s2<=epsilon) )     {
            // here!
            if (!tri->hasSplit())
                return tri;

            
            for (int i=0; i<3; i++) {
                if (auto * subt=tri->sub[i]){
                    if (auto* found=find_tri_at(subt,pt,depth+1)){
                        return found;
                    }
                }
            }

            
            //printf("error, shouldnt be here could not find tri - in split parent without having recursed. try again with bigger epsilon? use tri-dist instead of inside test..");
            // try again with bigger epsilon?

            return nullptr;

        } else
            return nullptr;
    }
    void tryFlip(Vertex* v0, Vertex* v1,Triangle* ta, int depth=0){
        //          vb
        //e0b  /     ?     \  eb1
        //    /     tb      \  
        //  v0--t0ba-+-t1ab--v1
        //    \     ta      / 
        //ea0  \     ?     /  e1b
        //          va
        //
        // vb  _
        //   \   \   
        //    v0---v1   unflipable.
        //    /  /  
        //   av
        if (!ta) return;
        if (ta->sub[0]){
            TRI_WARN("already flipped - need recurse here\n");
            //exit(0);
            return ;
        }

        // asked to split this, but it might have been already split at the point we asked.
        // so - check ..



        Triangle* tb = ta->getOtherTriangle(v0,v1);
        if (!tb) return;
        if (tb->sub[0]){
            TRI_WARN("other side was flipped - need recurse or fix\n");
            return;
        }

        Vertex* vb=tb->getOtherVertex(v0,v1);
        Vertex* va=ta->getOtherVertex(v0,v1);

        // Use angles opposite the edge to determine flip
        // "maximize  min angle"
        // TODO - method NOT using trigonometry. it is possible
        float angle_a0_a1 = triangleAngleXY(va->pos.xy(), v1->pos.xy(), v0->pos.xy());
        float angle_b1_b0 = triangleAngleXY(vb->pos.xy(), v0->pos.xy(), v1->pos.xy());
        
        if (angle_a0_a1 + angle_b1_b0 < M_PI)
            return;

        auto z_a01=signedAreaXY(va->pos,v0->pos,v1->pos);
        auto z_b10=signedAreaXY(vb->pos,v1->pos,v0->pos);
        if (z_a01*z_b10 <0.f){
            //printf("winding order broken %.5f %.5f\n",z_a01,z_b10);
            //exit(0);
            //return;
        }


        auto z_a0b=signedAreaXY(va->pos,v0->pos,vb->pos);
        auto z_ab1=signedAreaXY(va->pos,vb->pos,v1->pos);
        if (z_a0b*z_ab1 <0.f){
            TRI_WARN("unflipable (result nonconvex,flip flips order)\n");
            return;
        };
        //return;


        // we dont *delete* the triangles. we replace them with ptrs to the new ones.

        auto e0b = tb->getOtherTriangle(v0,vb); // others needed to reconnect.
        auto eb1 = tb->getOtherTriangle(vb,v1);
        auto e1b = ta->getOtherTriangle(v1,va);
        auto ea0 = ta->getOtherTriangle(va,v0);

        Triangle* tri_0ba=MakeTriangle(v0,vb,va);
        Triangle* tri_1ab=MakeTriangle(v1,va,vb);
        if (!tri_0ba) return;
        if (!tri_1ab) return;
        ta->sub[0]=tri_0ba; // both a and b have these new triangles, cutting accross the old edge.
        ta->sub[1]=tri_1ab;
        tb->sub[0]=tri_0ba;
        tb->sub[1]=tri_1ab;
        connectTriEdges(va,vb, tri_1ab,tri_0ba);
        connectTriEdges(v0,vb, tri_0ba, e0b);
        connectTriEdges(vb,v1, tri_1ab, eb1);
        connectTriEdges(v1,va, tri_1ab, e1b);
        connectTriEdges(va,v0, tri_0ba, ea0);

        // verify all pointers to ta,tb are gone..
        if (0)
        for (auto t=this->all_triangles; t; t=t->next){
            for (int i=0; i<3; i++){
                if (t->edge[i]==ta || t->edge[i]==tb){
                    printf("dangling edge to non leaf tri");
                    exit(0);
                }
            }
        }

        // verify all connections are to leaf tris?
/*
        for (int i=0; i<2; i++){
            auto *t=i?tri_0ba:tri_1ab;
            if (t->hasSplit()) {
                printf("erroneous split\n");
                exit(0);
            }
            for (int j=0; j<3; j++){
                if (t->edge[j])
                    if (t->edge[j]->hasSplit()){
                        printf("pointing at erroneous split\n");
                        exit(0);

                    }
            }
        }   
        */

//        tri_0ba->setTriEdge(v0,vb, tb->getOtherTriangle(v0,vb));
//        tri_0ba->setTriEdge(va,v0, ta->getOtherTriangle(va,v0));
//        tri_1ab->setTriEdge(vb,v1, tb->getOtherTriangle(vb,v1));
//        tri_1ab->setTriEdge(v1,va, ta->getOtherTriangle(v1,va));
        


        // now need to check flips of the 4 outer edges..
        //return;// notworking no
        if(depth>10){
            TRI_WARN("recursion %d?!",depth);
            return;
        }

        this->tryFlip(v0,vb,tri_0ba,depth+1);
        this->tryFlip(va,v0,tri_0ba,depth+1);
        this->tryFlip(vb,v1,tri_1ab,depth+1);
        this->tryFlip(v1,va,tri_1ab,depth+1);

        //tryFlip()
//        this->tryFlip(va,vb,tri_0ba,depth+1);


    }
    void insertVertex(Vec3 pt,uint32_t id){
        // Find which triangle it's inside..
        auto tri=find_tri_at(this->root_triangle,pt,0);
        // we insert into the triangle, splitting it.
        //     e0
        // 0  ----  1
        //   \'vn'/
        // e2 \| / e1
        //     2
        if (tri) {
            auto vn=new Vertex;
            vn->pos=pt;
            vn->id=id;
            vn->next=vertices;
            vertices=vn;

            Vertex* v0=tri->vertex[0];
            Vertex* v1=tri->vertex[1];
            Vertex* v2=tri->vertex[2];
            Triangle* e0=tri->edge[0];
            Triangle* e1=tri->edge[1];
            Triangle* e2=tri->edge[2];
            Triangle* tri01n=MakeTriangle(v0,v1,vn);
            Triangle* tri12n=MakeTriangle(v1,v2,vn);
            Triangle* tri20n=MakeTriangle(v2,v0,vn);
            tri->sub[0]=tri01n;
            tri->sub[1]=tri12n;
            tri->sub[2]=tri20n;

            // TODO - vertify both options below are equivalent.
            // see order above..
            if (false) {
                if (e0)
                    e0->setTriEdge(v0,v1, tri01n);
                if (e1)
                    e1->setTriEdge(v1,v2, tri12n);
                if (e2)
                    e2->setTriEdge(v2,v0, tri20n);
                tri01n->setTriEdges(v0,v1,vn, e0, tri12n, tri20n);
                tri12n->setTriEdges(v1,v2,vn, e1, tri20n, tri01n);
                tri20n->setTriEdges(v2,v0,vn, e2, tri01n, tri12n);
            } else {
                connectTriEdges(v0,v1, tri01n, e0);
                connectTriEdges(v1,v2, tri12n, e1);
                connectTriEdges(v2,v0, tri20n, e2);
                connectTriEdges(v1,vn, tri01n, tri12n);
                connectTriEdges(v2,vn, tri12n, tri20n);
                connectTriEdges(v0,vn, tri20n, tri01n);
            }
            // now we have 3 flips to try..
            tryFlip(v0,v1,tri01n);
            tryFlip(v1,v2,tri12n);
            tryFlip(v2,v0,tri20n);
        }
        else {    // not inside any triangle - bug. only support dividing up the initial condition
            TRI_WARN("pt outside of mesh");
            //exit(0);
        }
    }
    // triangles held in heirachy - walk them.
    void for_each_tri(Triangle* curr, std::function<void(Triangulation*,Triangle*)> f) {
            f(this,curr);
            for (int i=0; i<3; i++){
                if (auto *p=curr->sub[i])
                    for_each_tri(p, f);
            }
    }
    void for_each_leaf_tri(Triangle* tri, std::function<void(Triangulation*,Triangle*)> f){
        if (!tri->hasSplit()) {
            f(this,tri);
        } else {
            for (int i=0; i<3; i++){
                if (auto *p=tri->sub[i])
                    for_each_leaf_tri(p, f);
            }
        }
    }
};

Triangulation* makeSurfaceDemo(int numCircles);
