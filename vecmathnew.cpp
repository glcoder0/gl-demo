#include "vecmathnew.h"


const char* getFilenameExt(const char* fname){
	auto *cc=fname;
	while (*cc && *cc!='.') cc++;
	if (*cc=='.') return cc+1;
	else return cc;	// it'l be zero length.
}
bool cmpupr(const char* a, const char* b){
	for (;*a && *b;a++,b++){
		auto c=*a; if (c>='a' && c<='z') c+='A'-'a';
		if (c!=*b) return false;
	}
	if (*a==0 && *b==0)
		return true;
	return false;
}

// sohuld be in fileutil
#ifndef __ANDROID__
uint8_t* loadAndAllocResourceFile(const char* f, size_t* sizeOut){
	char full_name[256];
	snprintf(full_name,256,"%s/%s","assets",f);
	printf("\nload %s\n",full_name);
	FILE* fp=fopen(full_name,"rb");
	if (!fp) {
		LOGI("could not load %s",f);
		return nullptr;
	}
	fseek(fp,0,SEEK_END);
	size_t len=ftell(fp);
	fseek(fp,0,SEEK_SET);
	auto* buffer=(uint8_t*)malloc(len);
	fread(buffer,1,len,fp);
	fclose(fp);
	if (sizeOut) *sizeOut=len;
	LOGI("loaded %s %zu bytes",f,len);
	return buffer;
}
#endif


void midpoint_displacement_fractal(float* dst, int size, int size_supress, float s_amp, int step, float amp,float dim, int seed) {
	if (step==0) step=size;
	int mask=size-1,i,j;
	if (step==size) dst[0]=0.f;
	printf("step %d/%d amp=%.3f\n",step,size,amp);
	if (step==size_supress) amp=s_amp;
	
	// X
	int halfstep=step/2;
	for (j=0; j<size; j+=step){
		for (int i=0; i<size; i+=step){
			auto val=(
				*addr2d(dst,size, i,j)+
				*addr2d(dst,size, i+step,j)+
				*addr2d(dst,size, i,j+step)+
				*addr2d(dst,size, i+step,j+step))*0.25f;
			*addr2d(dst,size,i+halfstep,j+halfstep) = val+gaussRand(amp);
		}
	}
	amp*=sqrt(dim);
	bool swap=false;
	for (j=0; j<size; j+=halfstep){
		swap^=true;
		for (int i=(swap)?halfstep:0; i<size; i+=step){
			auto val=(
				*addr2d(dst,size, i-halfstep,j)+
				*addr2d(dst,size, i+halfstep,j)+
				*addr2d(dst,size, i,j-halfstep)+
				*addr2d(dst,size, i,j+halfstep))*0.25f;
			*addr2d(dst,size,i,j) = val+gaussRand(amp);
		}
	}
	if (halfstep>1) {
		midpoint_displacement_fractal(dst,size,size_supress,s_amp,halfstep,amp*sqrt(dim), dim, seed);
	}
}

extern Vec2i g_viewport;
void CamView::set_orthographic(Vec4 fwd, Vec4 up, const Vec4* pointsToEnclose, int numPoints,float expand_points_by_radius){
	auto points=pointsToEnclose;
	// cut-pasted from shadowbuffer intiialization code which was written adhoc
	// todo -rename in a neutral way. we extracted this because this setup is useful for other things
	auto shadow_dir=-fwd.normalize();
	Vec4 shadow_v = up.perp(shadow_dir).normalize();
	Vec4 shadow_u = shadow_dir.cross(shadow_v);
	Extents4f shadowExt;
	shadowExt.init();
	for (int i=0; i<numPoints; i++){
		auto fp=points[i];
		shadowExt.include(Vec4(fp.dot(shadow_u),fp.dot(shadow_v),fp.dot(shadow_dir),1.f));
	}
	shadowExt.min-=Vec3(expand_points_by_radius).xyz0();
	shadowExt.min+=Vec3(expand_points_by_radius).xyz0();
	auto extCentre=shadowExt.centre(); // this centre frames the shadow render rect around vis objects
	Vec4 shadow_centre = (shadow_u*extCentre.x+shadow_v*extCentre.y+shadow_dir*extCentre.z).xyz1();
	Vec4 hsize=shadowExt.halfSize();
	Matrix4 shadowToWorld=Matrix4(shadow_u,shadow_v,shadow_dir, shadow_centre);
	Matrix4 worldToShadow= shadowToWorld.invertSRT();

	Matrix4 shadowProj = Matrix4(
		Vec4(1.0f/hsize.x,0.f,0.f,0.f),
		Vec4(0.f,1.0f/hsize.y,0.f,0.f),
		Vec4(0.f,0.f,1.0f/hsize.z,0.f),
		Vec4(0.f,0.f,0.f,1.f)
	);
	// reset extents.
	shadowExt.init();
	float radius=0.f;

	for (int i=0; i<numPoints; i++){
		auto ofs=points[i]-shadow_centre;
		if (auto d=ofs.length(); d>radius){radius=d;}
		shadowExt.include(Vec4(ofs.dot(shadow_u),ofs.dot(shadow_v),ofs.dot(shadow_dir),1.f));
		// todo - should this just end up with min,max = -/+ halfSize from above?!
	};


	auto &shcam=*this;
	shcam.is_orthographic=true;

	shcam.to_world=shadowToWorld;
	shcam.from_world=shadowToWorld;
	shcam.proj_view=shadowProj * worldToShadow;
	shcam.proj_only=shadowProj;//Matrix4::identity(); TODO FIX MISNOMER. it's proj_view. uMatProj=uMatProjView; uMatModelView=uMatModel.
	shcam.m_viewport_size=g_viewport;
	shcam.frustum.tan_half_fov=0.f;
	shcam.frustum.aspect=1.f;	// TODO WE MIGHT NEED
	shcam.m_vel=Vec4(0.f,0.f,0.f,0.f);//cam.m_vel;
	shcam.frustum.znear = -hsize.z;//shadowExt.min.z; //-1000000000.f;//
	shcam.frustum.zfar = hsize.z;//shadowExt.max.z; //1000000000.f;//
	shcam.set_bounds();
	shcam.sphere.radius=radius;
	shcam.sphere.centre=shadow_centre;
	shcam.cull_planes[0]=shadow_u;//to_world * Vec4(1.0f,0.0f,0.0f,0.0f);
	shcam.cull_planes[1]=-shadow_u;//Vec4(-1.0f,0.0f,0.0f,0.0f);
	shcam.cull_planes[2]=shadow_v;//Vec4(0.0f,1.0f,0.0f,0.0f);
	shcam.cull_planes[3]=-shadow_v;//Vec4(0.0f,-1.0f,0.0f,0.0f);
	shcam.cull_values[0] =+hsize.x;//shadowExt.min.x ;
	shcam.cull_values[1] =+hsize.x;//shadowExt.max.x ;
	shcam.cull_values[2] =+hsize.y;//shadowExt.min.y ;
	shcam.cull_values[3] =+hsize.y;//shadowExt.max.y ;
}



#ifdef MAIN_vecmathnew

void math_test() {
	auto v0 = Vec4(1.0,0.1,-2.0,0.f);
	auto v1 = Vec4(-1.0,0.3,0.6,0.f);
	v0.cross(v1).dump(); v0.cross(v1).dump();

	auto tmp=Matrix4(1.0f);
	tmp.dump();
	tmp.orthonormalize_zyx().dump();
	tmp.orthonormalize_yzx().dump();
	auto m=Matrix4::heading_pitch_roll_zup_yfwd(0.5f,0.f,0.0f);
	printf("asin y.z=%.3f\n", tmp.ay.z);
	m.dump();

	m.heading_pitch_roll_zup_yfwd().dump();
	clock_t t0 = clock();
	printf("timing cpu matmul..\n");
	printf("");
	Matrix4 m1 = Matrix4::identity();
	Matrix4 m2 = Matrix4::rotX(5.f);
	Matrix4 m3 = Matrix4::rotY(0.01f);
	Vec4 acc=Vec4::unitW();
	int i;
	m1=m2;
	int numToCount=1000000;
	for (i=0; i<numToCount; i++)  {
		m1=m1 * m3;
	}
	m1.dump();
	auto t1 = clock();
	auto msec=(1000*(t1-t0))/CLOCKS_PER_SEC;
	printf("timing cpu matmul.. %ld %ld %ld %ld sec = %ld matmuls per'frame\n",
			t0,t1,t1-t0, msec, numToCount*16/(msec) );


	float f0=0.1f,f1=-0.02f,f2=100.0f;
	Half h0=f0,h1=f1,h2=f2;
	printf("float->half->float %.5f %.5f %.5f\n", (float) h0,(float) h1,(float) h2);

	printf("angle test 1 %.3f\n", triangleAngleXY(Vec2(0.0f,0.0f), Vec2(1.0f,0.0f),Vec2(0.0f,1.0f)  ));
	printf("angle test 2 %.3f\n", triangleAngleXY(Vec2(1.0f,1.0f), Vec2(1.0f,2.0f),Vec2(1.0f,2.0f)  ));
	printf("angle test 2 %.3f\n", triangleAngleXY(Vec2(1.0f,1.0f), Vec2(1.0f,2.0f),Vec2(2.0f,2.0f)  ));
	printf("angle test 3 %.3f\n", triangleAngleXY(Vec2(0.0f,0.0f), Vec2(1.0f,1.0f),Vec2(0.0f,1.0f)  ));
	for (int i=0; i<64; i++){
		printf("\tvec2(%.5f,%.5f),",gaussRand(1.0f),gaussRand(1.0f));
	}
}



int main(int argc, const char** argv)
{
    math_test();
}

#endif




