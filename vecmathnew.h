#pragma once

#define _CRT_SECURE_NO_WARNINGS
#include <math.h>
#include <string.h>
#define PI M_PI

#include <vector>
#include <array>
#include <limits>
#include <functional>
#include <algorithm>
#include <map>
#include  <initializer_list>
#include <iostream>
#include "jpgd.h"
#include <time.h>

using namespace std;
#define fbx_printf printf
//#define ASSERT(X)
//#include "texture.h"

#define GL_GLEXT_PROTOTYPES


#ifndef __ANDROID__
#include <xmmintrin.h>
//#include <immintrin.h>
#include <SDL.h>
#ifdef _MSC_VER
#include <glew.h>
#include <GL/GLU.h>
#include <GL/GL.h>
#else
inline void glewInit(){};
#define SDL_main main
#endif

#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>
#define USE_SSE

#else
#include <android/log.h>

uint8_t* loadAndAllocResourceFile(const char* f, size_t* sizeOut);

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "native-activity", __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "native-activity", __VA_ARGS__))
#define ALOGW LOGW
#define ALOGV LOGI

// Include the latest possible header file( GL version header )
#if __ANDROID_API__ >= 24
#include <GLES3/gl32.h>
#elif __ANDROID_API__ >= 21
#include <GLES3/gl31.h>
#else
#include <GLES3/gl3.h>
#endif

#endif

#ifndef ERROR
#define ERROR(X,...) {printf("%s:%d ERROR:",__FILE__,__LINE__);printf(X,__VA_ARGS__);printf("\n");exit(0);}
#endif
#ifndef LOGI
#define LOGI(X,...) printf("%s:%d INFO:",__FILE__,__LINE__);printf(X,__VA_ARGS__);printf("\n");
#endif
#ifndef LOGI
#define LOGV(X,...) printf("%s:%d VERBOSE:",__FILE__,__LINE__);printf(X,__VA_ARGS__);printf("\n");
#endif
#ifndef LOGE
#define LOGE(X,...) printf("%s:%d ERROR:",__FILE__,__LINE__);printf(X,__VA_ARGS__);printf("\n");
#endif
#ifndef ASSERT
#define ASSERT(X) if (!(X)) {printf("%s:%d ASSERT FAILED: %s",__FILE__,__LINE__,#X);}
#endif
#define ALOGW LOGW
#define ALOGV LOGI

#ifndef TRACE
#define TRACE  printf("%s:%d\n",__FILE__,__LINE__);
#endif

#ifndef DUMPI
#define DUMPI(X) LOGI("%s=%d",#X,X);
#endif


#ifndef HALF_T
struct Half {
    short value;
    Half() { }
    //inline auto	operator=(const float & src)->Half&
	Half(const float src)
    {
        volatile static const unsigned int integer = 0x52000000;

        // todo - fixed point numbers are better for models, they have known bounding boxes
        ASSERT(sizeof(int)==sizeof(float))
        int x= (int&) src;

        // cut-paste from web..
        short sign = (x >> 16) & 0x8000;
        volatile int absolute = x & 0x7FFFFFFF;
        int X = absolute | (absolute << 10) | (absolute << 13);
        int Y = 0x0F7FE000;
        int Z = absolute + 0xC8000000;
        volatile float F = (float&)absolute * (float&)integer;
        int W = (int)F;

        int result;
        if(absolute >= 0x7F800000)   // NaN
        {
            result = X;
        }
        else if(absolute > 0x477FE000)   // Infinity
        {
            result = Y;
        }
        else if(absolute >= 0x38800000)   // Normal
        {
            result = Z;
        }
        else   // Denormal
        {
            result = W;
        }
        value= sign | ((result >> 13) & 0x7FFF);
        // end cut-paste
    }
	operator float() const {
		uint32_t sign = ((uint32_t)(value&0x8000))<<16;
		uint32_t exp = (((value&0x7fff)>>10)-15 + 127 )<<23;
		uint32_t mant = ((uint32_t)(value  & ((1<<10)-1) ))<<(23-10);
		uint32_t result = sign|exp|mant;
		return *(float*)&result;
	}
};
#endif

namespace glm {
	// fwd decl's of GLM stuff for interop
	template<typename T> class tmat4x4;
	template<typename T> class tvec4;
	template<typename T> class tvec3;
	template<typename T> class tvec2;

}

template<typename T> struct TVec4;
template<typename T> struct TVec3;
template<typename T> struct TVec2 {
	T x,y; 
	TVec2(){}
	template<typename Y>
	inline TVec2(const TVec2<Y>& src):x(T(src.x)),y(T(src.y)){}
	inline TVec2(const T& a,const T& b):x(a),y(b){}
	template<typename Y>
	explicit inline TVec2(const std::array<Y,2>& s):x(Y(s[0])),y(Y(s[1])){}
	inline auto area()const {return x*y;}
	inline TVec2<T> operator-(const TVec2<T>& b)const {return TVec2<T>(x-b.x,y-b.y);}
	inline TVec3<T> operator-()const {return TVec2<T>(-x,-y);}
	inline TVec2<T> operator+(const TVec2<T>& b)const {return TVec2<T>(x+b.x,y+b.y);}
	inline TVec2<T> operator&(const TVec2<T>& b)const {return TVec2<T>(x&b.x,y&b.y);}
	inline TVec2<T> operator&(const T& b)const {return TVec2<T>(x&b,y&b);}
	inline float crossZ(const TVec2<T>&b) const { return x*b.y -y*b.x; } // todo -verify winding. z component of 3d cross

	operator std::array<T,2>()const{return std::array<T,2>{{x,y}}; }


	template<typename B>
	inline auto operator*(const TVec2<B>& b)const {return TVec2<decltype(x*b.x)>(x*b.x,y*b.y);}

	template<typename F>
	inline auto operator*(const F& b)const {return TVec2<decltype(x*b)>(x*b,y*b);}
	template<typename B>
	inline auto operator/(const TVec2<B>& b)const {return TVec2<decltype(x/b.x)>(x/b.x,y/b.y);}
	inline TVec2<T>& operator+=(const TVec2<T>& src) {x+=src.x; y+=src.y;}
	inline TVec2<T>& operator-=(const TVec2<T>& src) {x-=src.x; y-=src.y;}
	// scalar comparison allows slotting into std::map<>
	inline bool operator<(const TVec2<T>& b)const {return x<b.x;}
	inline auto dot(const TVec2<T>& b) const{return x*b.x+y*b.y;}
	inline auto crossz(const TVec2<T>& b) const{return x*b.y - y*b.x;} // z component of cross((x0,y0,0),(x1,y1,0))
	inline TVec2<T> min(const TVec2<T>& b)const{return TVec2<T>(min(x,b.x),min(y,b.y)); }
	inline TVec2<T> max(const TVec2<T>& b)const{return TVec2<T>(max(x,b.x),max(y,b.y)); }
	inline auto sqr()const{return this->dot(*this);}	// TODO .. actually not sure if this should be 'per elem'
	auto lengthSquared()const{return this->dot(*this);}
	auto length()const{return sqrt(this->lengthSquared());}
	auto dist(const TVec2<T>& b) const {return sqrt((b-*this).sqr());}
	auto distSquared(const TVec2<T>& b) const{return (b-*this).sqr();}
};// todo - ops
template<typename T> 
struct TVec3 {T x,y,z;
	TVec3(){}
	TVec3(const T& a,const T&b,const T&c) :x(a),y(b),z(c){};
	TVec3(const T& a) :x(a),y(a),z(a){};
	TVec3(const TVec2<T>& ab,const T&c) :x(ab.x),y(ab.y),z(c){};
	template<typename B>
	explicit TVec3(const std::array<B,3>& src) :x(B(src[0])),y(B(src[1])),z(B(src[2])){};
	inline TVec3<T> operator-(const TVec3<T>& b)const {return TVec3<T>(x-b.x,y-b.y,z-b.z);}
	inline TVec3<T> operator-()const {return TVec3<T>(-x,-y,-z);}
	inline TVec3<T> operator+(const TVec3<T>& b)const {return TVec3<T>(x+b.x,y+b.y,z+b.z);}
	inline TVec3<T> operator&(const TVec3<T>& b)const {return TVec3<T>(x&b.x,y&b.y,z&b.z);}
	inline TVec3<T> operator&(const T& b)const {return TVec3<T>(x&b,y&b,z&b);}
	
	template<typename B>
	operator std::array<B,3>()const{return std::array<B,3>{{B(x),B(y),B(z)}}; }
	template<typename B>
	inline auto operator*(const TVec3<B>& b)const {return TVec3<decltype(x*b.x)>(x*b.x,y*b.y,z*b.z);}
	template<typename F>
	inline auto operator*(const F& f)const {return TVec3<decltype(x*f)>(x*f,y*f,z*f);}
	inline TVec3<T>& operator+=(const TVec3<T>& b) {x+=b.x;y+=b.y;z+=b.z;return *this;}
	inline TVec3<T> min(const TVec3<T>& b)const{return TVec3<T>(std::min(x,b.x),std::min(y,b.y),std::min(z,b.z)); }
	inline TVec3<T> max(const TVec3<T>& b)const{return TVec3<T>(std::max(x,b.x),std::max(y,b.y),std::max(z,b.z)); }
	// scalar comparison allows slotting into std::map<>. 
	bool operator<(const TVec3& b)const {return this->x<b.x; /* todo - hash of all bits.*/}
	inline auto dot(const TVec3<T>& b) const{return x*b.x+y*b.y+z*b.z;}
	inline auto sqr()const {return this->dot(*this);}
	auto distSquared(const TVec3<T>& b) const{return (b-*this).sqr();}
	auto dist(const TVec3<T>& b) const{return sqrt(this->distSquared(b));}
	inline TVec2<T> xy()const{ return TVec2<T>(x,y);}
	inline TVec2<T> yz()const{ return TVec2<T>(y,z);}
	inline TVec2<T> xz()const{ return TVec2<T>(x,z);}
	inline TVec4<T> xyz0()const{ return TVec4<T>(x,y,z,0.0f);}
	inline TVec4<T> xyz1()const{ return TVec4<T>(x,y,z,1.0f);}
	inline TVec3<T> yzx()const{ return TVec3<T>(y,z,x);}
	inline TVec3<T> zxy()const{ return TVec3<T>(z,x,y);}
	inline auto length()const{return sqrt(this->sqr());}
	T maxElem() const {return std::max(x,std::max(y,z));}
	inline TVec3<T> normalize()const{ return *this * (1.0f/this->length());}
	TVec3 cross(const TVec3& b) const{
		// via permutes - 4 shuffles , 2muls, 1 sub
		// todo - clear the W or assert it's zero on inputs.
		return this->yzx()*b.zxy() - this->zxy()*b.yzx();
	}

	inline auto volume()const{return x*y*z;};
}; // todo -ops
typedef TVec3<float> Vec3f;
inline float signedAreaXY(Vec3f a,Vec3f b,Vec3f c){
    auto dx0 = b.x-a.x;
    auto dy0 = b.y-a.y;
    auto dx1 = c.x-b.x;
    auto dy1 = c.y-b.y;
    return (dx0*dy1-dy0*dx1)*0.5f;
}

typedef TVec2<int32_t> Vec2i;
typedef TVec2<float> Vec2;
typedef TVec3<float> Vec3;


template<typename V> auto triangleCrossZ(const V& a, const V& b,const V& c){ //un normalised normal.
	return (c-a).crossZ(b-a);
}

template<typename V> auto triangleAngleXY(const V& origin, const V& pos0, const V& pos1){
	auto v0 = pos0-origin;
	auto v1 = pos1-origin;
	float len0 = v0.length();
	float len1 = v1.length();
	// v0 dot v1 = len v0 * len v1 * cos angle
	return acosf(v0.dot(v1)/(len0*len1));
}

template<typename V> auto triangleCross(const V& a, const V& b,const V& c){ //un normalised normal.
	return (c-a).cross(b-a);
}
template<typename V> auto triangleNormal(const V& a, const V& b,const V& c){
	return triangleCross(a,b,c).normalize();
}
struct PackedARGB{
	uint32_t v;
	PackedARGB(const PackedARGB& b):v(b.v){}
	explicit PackedARGB(uint32_t a):v(a){}
	void to_f32(float* r,float* g,float* b,float* a)const{
		float f=(1.0f/128.0f);
		*r=((v)&0xff)*f;
		*g=((v>>8)&0xff)*f;
		*b=((v>>16)&0xff)*f;
		*a=((v>>24)&0xff)*f;
	}
	PackedARGB(float r,float g,float b,float a){
		float f=128.0f;
		v=std::clamp((int)(r*f),0,255)|
			(std::clamp(int(g*f),0,255)<<8)|
			(std::clamp(int(b*f),0,255)<<16)|
			(std::clamp(int(a*f),0,255)>>24);
	}
};

struct HeadingPitch {float heading;float pitch;void dump(){printf("{%f,%f}",heading,pitch);}};
struct HeadingPitchRoll {float heading;float pitch;float roll;void dump(){printf("{%f,%f,%f}",heading,pitch,roll);}};
template<typename T>
struct TVec4 {
	friend struct Matrix4;

	T x,y,z,w;
public:
	TVec4(){}
	TVec4(const glm::tvec4<T>& src);	// compatible with glm vector

	operator glm::tvec4<T> () const;
	// todo SIMDize
	static auto unitX(){return TVec4(1.0,0.0,0.0,0.0);}
	static auto unitY(){return TVec4(0.0,1.0,0.0,0.0);}
	static auto unitZ(){return TVec4(0.0,0.0,1.0,0.0);}
	static auto unitW(){return TVec4(0.0,0.0,0.0,1.0);}
	inline auto xy()const {return TVec2<T>(x,y);}
	inline auto xz()const {return TVec2<T>(x,z);}
	inline auto yz()const {return TVec2<T>(y,z);}
	inline auto zy()const {return TVec2<T>(z,y);}
	inline auto xyz()const {return TVec3<T>(x,y,z);}
	inline auto xzy()const {return TVec3<T>(x,z,y);}
	inline auto getx()const {return this->x;}
	inline auto gety()const {return this->x;}
	inline auto getz()const {return this->z;}
	inline auto getw()const {return this->w;}


	TVec4(const TVec3<T>& a,const T&  _w): x(a.x),y(a.y),z(a.z),w(_w){}
	TVec4(const TVec2<T>& a,const T& _z,const T&  _w): x(a.x),y(a.y),z(_z),w(_w){}
	explicit TVec4(const T& a):x(a),y(a),z(a),w(a){} // splat single value
	template<typename B>
	explicit TVec4(B src[4]):x(T(src[0])),y(T(src[1])),z(T(src[2])),w(T(src[3])){};
	template<typename B>
	explicit TVec4(B src[3],B src3):x(T(src[0])),y(T(src[1])),z(T(src[2])),w(T(src3)){};
	template<typename B>
	explicit TVec4(const std::array<B,4>& src):x(src[0]),y(src[1]),z(src[2]),w(src[3]){};
	template<typename B>
	explicit TVec4(const std::array<B,3>& src,float _w):x(src[0]),y(src[1]),z(src[2]),w(_w){};
	explicit TVec4(const PackedARGB& src){
		float f=(1.0f/128.0f);
		x=((src.v)&0xff)*f;
		y=((src.v>>8)&0xff)*f;
		z=((src.v>>16)&0xff)*f;
		w=((src.v>>24)&0xff)*f;
	}
	//operator PackedARGB() const{return PackedARGB(x,y,z,w);}

	operator std::array<T,4>()const{return std::array<T,4>{{x,y,z,w}}; }

	TVec4(T v[3],T _w){x=v[0],y=v[1],z=v[2],w=_w;}
	TVec4(const T& a,const T& b,const T& c,const T& d):x(a),y(b),z(c),w(d){}
	template<typename B>
	explicit TVec4(const TVec4<B>& src) : x(T(src[0])),y(T(src[1])),z(T(src[2])),w(T(src[3])){}
	TVec4 min(const TVec4<T>& b)const{ return TVec4(std::min(x,b.x),std::min(y,b.y),std::min(z,b.z),std::min(w,b.w));}
	TVec4 max(const TVec4<T>& b)const{ return TVec4(std::max(x,b.x),std::max(y,b.y),std::max(z,b.z),std::max(w,b.w));}
	auto xxxx()const{return TVec4<T>(x);}	// broadcasts in glsl-esque syntax. see also 'splat'
	auto yyyy()const{return TVec4<T>(y);}
	auto zzzz()const{return TVec4<T>(z);}
	auto wwww()const{return TVec4<T>(w);}

	HeadingPitch heading_pitch_zup()const{return HeadingPitch{atan2(x,y), asin(z)};}
	HeadingPitch heading_pitch_yup()const{return HeadingPitch{atan2(x,z), asin(y)};}

	std::pair<TVec4<T>,TVec4<T>> subadd(const TVec4& ofs)const{return std::pair<TVec4<T>,TVec4<T>>(*this-ofs,*this+ofs);}// useful for gen boxes
	TVec4 avr(const TVec4& b)const{return ((*this)+b)*0.5f;}
	TVec4 sub(const TVec4& b)const{return *this-b;}	// for sequence 
	TVec4 sub_from(const TVec4& b)const{return b-*this;}	// for sequence, reversed ("subtract this from..") 
	TVec4 add(const TVec4& b)const{return b+*this;}
	TVec4 scale(float f)const{return *this * f;}
	TVec4 madd(const TVec4& b,float f)const{return this->add(b*f);}
	const void operator*=(T f){
		x*=f; y*=f; z*=f;w*=f;
	}
	const void operator+=(const TVec4& b){
		x+=b.x; y+=b.y; z+=b.z; w+=b.w;
	}
	const void operator-=(const TVec4& b){
		x-=b.x; y-=b.y; z-=b.z; w-=b.w;
	}
	TVec4 operator*(T f)const{
		return TVec4(x*f,y*f,z*f,w*f);
	}
	TVec4 operator*(const TVec4& b)const{	// per elem mul (aka "hadamard product")
		return TVec4(x*b.x,y*b.y,z*b.z,w*b.w);
	}
	TVec4 operator+(const TVec4& b)const {
		return TVec4(x+b.x,y+b.y,z+b.z,w+b.w);
	}
	TVec4 operator-(const TVec4& b)const {
		return TVec4(x-b.x,y-b.y,z-b.z,w-b.w);
	}
	T dist(const TVec4& b)const {return (b-*this).length();}
	T distSquared(const TVec4& b)const {return (b-*this).lengthSquared();}
	T invLength() const {return 1.0/this->length();}// dedicated opt in SSE exists }
	float  dot(const TVec4& b) const{return x*b.x+y*b.y+z*b.z+w*b.w;}
	// component wise comparisons.. select mask abstraction
	// need to specialise, keep as Vec4<u32> for float compares.
	//TVec4<bool> operator>(const TVec4& b)const {return TVec4<bool>(x>b.x,y>b.y,z>b.z(),w>b.w());}
	// change: bool operator< allows slotting into 'std::map<>'
	bool operator<(const TVec4<T>& b)const {return this->x<b.x; /* todo - hash of all bits.*/}
	//TVec4<bool> operator==(const TVec4& b)const {return TVec4<bool>(x==b.x,y==b.y,z==b.z(),w==b.w());}
	//TVec4<bool> operator<=(const TVec4& b)const {return TVec4<bool>(x<=b.x,y<=b.y,z<=b.z(),w<=b.w());}
	//TVec4<bool> operator>=(const TVec4& b)const {return TVec4<bool>(x>=b.x,y>=b.y,z>=b.z(),w>=b.w());}
	T horizSum()const{return x+y+z+w;}; // dot = component wise mul.horizSum
	bool allGE(const TVec4& b) const{return x>=b.x && y>=b.y && z>=b.z && w>=b.w;}
	bool allLE(const TVec4& b) const{return x<=b.x && y<=b.y && z<=b.z && w<=b.w;}
	bool anyLT(const TVec4&b) const {return !this->allGE(b);}
	bool anyGT(const TVec4&b) const {return !this->allLE(b);}
	const T& operator[](int i)const{return ((const T*)&x)[i];}
	T& operator[](int i){return ((T*)&x)[i];}
	// some common permutes
	inline void store(T f[4]){f[0]=x;f[1]=y;f[2]=z;f[3]=w;}
	TVec4 xyz0() const {return TVec4(x,y,z,0.f);}
	TVec4 xyz1() const {return TVec4(x,y,z,1.f);}
	TVec4 xzyw() const {return TVec4(x,z,y,w);}	// y/z up swaps
	TVec4 zyxw() const {return TVec4(z,y,x,w);}	// argb rgba swap
	TVec4 yzxw() const {return TVec4(y,z,x,w);}	// parts of cross-product
	TVec4 zxyw() const {return TVec4(z,x,y,w);}	// parts of cross-product
	TVec4 neg_xyz() const {return TVec4(-x,-y,-z,w);}
	TVec4 cross_manual(const TVec4& b) const {	// todo -cross product as elemwise dual permute for SIMD
		auto&a=*this;
		return TVec4{
			a.y*b.z-a.z*b.y,
			a.z*b.x-a.x*b.z,
			a.x*b.y-a.y*b.x,
			0.f,
		};
	}
	TVec4 cross(const TVec4& b) const{
		// via permutes - 4 shuffles , 2muls, 1 sub
		// todo - clear the W or assert it's zero on inputs.
		return this->yzxw()*b.zxyw() - this->zxyw()*b.yzxw();
	}
	TVec4 operator-() const{
		return TVec4{-x,-y,-z,-w};
	}
	auto normalize() const {
		return *this * (1.f/this->length());
	}
	TVec4<T> normalize_or(float epsilon,const TVec4<T>& elseVal) const {
		float len=this->length();
		if (len > epsilon) {return *this * (1.0f/len);}
		else return elseVal;
	}
	T length()const{ return sqrt(this->lengthSquared());}
	T lengthSquared()const{ return this->dot(*this);}

	TVec4 lerp(const TVec4& b,float f)const{
		return (b-*this)*f+*this;
	}
	TVec4 perp(const TVec4& axis) const {
		return *this - axis * this->dot(axis);
	}
	void dump()const {printf("{%.5f,%.5f,%.5f,%.5f}",x,y,z,w);}

	int maxAxisIndexXYZ()const{	// ignore 'w'
		float xx=x*x;
		float yy=y*y;
		float zz=z*z;

		if (xx>yy && xx>zz) return 0;
		if (yy>xx && yy>zz) return 1;
		return 2;
	}

};

#ifdef USE_FMA
#define mm_mul_add(A,B,C) _mm_fmadd_ps(A,B,C)
#define mm_mul_add(A,B,C) _mm_fmsub_ps(A,B,C)
#else
#define mm_mul_add(A,B,C) _mm_add_ps( _mm_mul_ps(A,B), C )
#define mm_mul_sub(A,B,C) _mm_sub_ps( _mm_mul_ps(A,B), C )
#endif

// Vec4 is explicitely float4 and the type we operate on
// other types are for storage.
#ifndef USE_SSE
typedef TVec4<float> Vec4;

#else
#define SHUFFLE(x,y,z,w) (x|(y<<2)|(z<<4)|(w<<6))
struct SplattedFloat {	// i.e. from dot product result.assume x=y=z=w
	__m128 m;
	operator float()const{float f; _mm_store_ss(&f,m);return f;}
	inline operator __m128()const{return m;}
	explicit SplattedFloat(__m128 s):m(s){}
	SplattedFloat(float f){
		m=_mm_load1_ps(&f);
//		m=_mm_broadcast_ss(&f);
	}
};
struct Half;
struct alignas(16) Vec4{
	float x,y,z,w;
	Vec4(){}
	template<typename Y>
	Vec4(Y a,Y b,Y c,Y d):x(float(a)),y(float(b)),z(float(c)),w(float(d)){}
	Vec4(SplattedFloat a,SplattedFloat b,SplattedFloat c,float d):x(float(a)),y(float(b)),z(float(c)),w(float(d)){}
	template<typename Y>
	Vec4(TVec2<Y> ab,Y c,Y d):x(Y(ab.x)),y(Y(ab.y)),z(Y(c)),w(Y(d)){}
	template<typename Y>	// todo - this is simd-able surely
	Vec4(TVec2<Y> ab,TVec2<Y> cd):x(Y(ab.x)),y(Y(ab.y)),z(Y(cd.x)),w(Y(cd.y)){}

	template<typename Y>
	Vec4(const TVec3<Y>& v,Y w):x(float(v.x)),y(float(v.y)),z(float(v.z)),w(float(w)){}

	template<typename Y>
	Vec4(const std::array<Y,3>& v,Y w):x(float(v[0])),y(float(v[1])),z(float(v[2])),w(float(w)){}
	template<typename Y>
	Vec4(const std::array<Y,4>& v,Y w):x(float(v[0])),y(float(v[1])),z(float(v[2])),w(float(v[3])){}
	template<typename Y>
	explicit Vec4(Y v[4]): x(float(v[0])),y(float(v[1])),z(float(v[2])),w(float(v[3])){}
	template<typename Y>
	explicit Vec4(Y v[3], Y _w): x(float(v[0])),y(float(v[1])),z(float(v[2])),w(float(_w)){}

	explicit Vec4(const PackedARGB& src){
		src.to_f32(&x,&y,&z,&w);
	}
	//operator PackedARGB() const{return PackedARGB(x,y,z,w);}


	inline operator __m128()const{return _mm_load_ps(&x);}
	inline explicit Vec4(__m128 xyzw){_mm_store_ps(&x,xyzw);}
	/*
		a.y*b.z - a.z*b.y,
		a.z*b.x - a.x*b.z,
		a.x*b.y - a.y*b.x,
	*/

	// todo - we just wanted specialization of one path
	// nonetheless we can implement SIMD optimized versions of these..
	template<typename B>
	Vec4(const TVec4<B>& src):x(B(src.x)),y(B(src.y)),z(B(src.z)),w(B(src.w)){}
	//explicit Vec4(const TVec4<int32_t>& src):Vec4(_mm_cvtepi32_ps(*(__m128*)(&src.x))){}

	float operator[](int i)const{return i[&x];}
	template<typename B>
	operator TVec4<B>()const{	return TVec4<B>(B(x),B(y),B(z),B(w));}

	operator TVec4<int32_t>()const{
		TVec4<int32_t> r;
		//(__m64&)r.x=_mm_cvt_ps2pi(*this);
		//(__m64&)r.z=_mm_cvt_ps2pi(this->zwxy());
		r.x=(int)this->x;
		r.y=(int)this->y;
		r.z=(int)this->z;
		r.w=(int)this->w;
		return r;
	}

	// to allow insertiion into std::map;
	bool operator<(const Vec4& b)const {
		return this->x<b.x; 
	}
	// todo - bitwise op converts to i32? or its temporary eg selmask
	inline void store(float f[4])const {f[0]=x;f[1]=y;f[2]=z;f[3]=w;}
	inline Vec4 operator&(const Vec4& b)const{	return Vec4(_mm_and_ps(*this,b));}
	inline Vec4 operator|(const Vec4& b)const{	return Vec4(_mm_or_ps(*this,b));	}
		inline Vec4 andnot(const Vec4& b)const{	return Vec4(_mm_andnot_ps(*this,b));	}
	inline Vec4 operator^(const Vec4& b)const{	return Vec4(_mm_xor_ps(*this,b));	}
	explicit Vec4(SplattedFloat f){_mm_store_ps(&x,f);};
	inline Vec4 operator+(const Vec4 b)const{
		return Vec4(_mm_add_ps(*this,b));
	}
	inline Vec4& operator+=(const Vec4 b){
		*this= Vec4(_mm_add_ps(*this,b));
		return *this;
	}
	inline Vec4& operator-=(const Vec4 b){
		*this= Vec4(_mm_sub_ps(*this,b));
		return *this;
	}
	int maxAxisIndexXYZ()const{	// ignore 'w'
		float xx=x*x;
		float yy=y*y;
		float zz=z*z;

		if (xx>yy && xx>zz) return 0;
		if (yy>xx && yy>zz) return 1;
		return 2;
	}
	inline Vec4 add(const Vec4& b)const{return *this+b;}
	inline Vec4 sub(const Vec4& b)const{return *this-b;}
	inline Vec4 sub_from(const Vec4& b)const{return *this-b;}
	inline Vec4 operator-(const Vec4 b)const{
		return Vec4(_mm_sub_ps(*this,b));
	}
	inline std::pair<Vec4,Vec4> subadd(const Vec4 b)const{return std::pair<Vec4,Vec4>(*this-b,*this+b);}
	inline Vec4 operator*(const Vec4 b)const{
		return Vec4(_mm_mul_ps(*this,b));
	}
	inline Vec4& operator*=(const SplattedFloat f){
		*this=Vec4(_mm_mul_ps(*this,f));
		return *this;
	}
	inline Vec4 operator*(const SplattedFloat f)const {
		return Vec4(_mm_mul_ps(*this,f));
	}
	inline Vec4 operator/(const Vec4 b)const{
		return Vec4(_mm_div_ps(*this,b));
	}
	inline Vec4 operator/(float f)const{
		// mm_load_ps .. why not?!
		//return Vec4(_mm_div_ps(*this,Vec4(f,f,f,f)));
		return Vec4(x/f,y/f,z/f,w/f);

	}
	template<int ix,int iy,int iz,int iw>
	inline Vec4 permute()const {return Vec4(_mm_shuffle_ps(*this,*this,ix|(iy<<2)|(iz<<4)|(iw<<6)));}

	inline Vec4 yzxw()const{ return permute<1,2,0,3>();}
	inline Vec4 zxyw()const{ return permute<2,0,1,3>();}
	inline Vec4 max(Vec4 b) const{
		return Vec4(_mm_max_ps(*this,b));
	}
	inline Vec4 min(Vec4 b) const{
		return Vec4(_mm_min_ps(*this,b));
	}
	inline static Vec4 unitX(){return Vec4(1.0f,0.0f,0.0f,0.0f);}
	inline static Vec4 unitY(){return Vec4(0.0f,1.0f,0.0f,0.0f);}
	inline static Vec4 unitZ(){return Vec4(0.0f,0.0f,1.0f,0.0f);}
	inline static Vec4 unitW(){return Vec4(0.0f,1.0f,0.0f,1.0f);}
	inline Vec4 xxyw()const{ return permute<0,0,1,3>(); } // for quat conversion
	inline Vec4 yzzw()const{ return permute<1,2,2,3>(); }
	inline Vec4 xzyw()const{ return permute<0,2,1,3>(); } // for crossprod
	inline Vec4 zyxw()const{ return permute<2,1,0,3>(); }
	inline Vec4 xxxx()const{ return permute<0,0,0,0>();}
	inline Vec4 yyyy()const{ return permute<1,1,1,1>();}
	inline Vec4 zzzz()const{ return permute<2,2,2,2>();}
	inline Vec4 yxwz()const{ return permute<1,0,3,2>();}
	inline Vec4 zwxy()const{ return permute<2,3,0,1>();}
	inline Vec4 wwww()const{ return permute<3,3,3,3>();}
	template<int ix,int iy,int iz,int iw>
	inline Vec4 zero_elems()const{
		auto m=__m128(*this);
		auto zero=_mm_sub_ps(m,m);
		return Vec4(_mm_blend_ps(m,zero,ix|(iy<<1)|(iz<<2)|(iw<<3)));
	}
	inline Vec4 neg_xyz()const {return Vec4(-x,-y,-z,w);}
	inline Vec4 xyz1()const{Vec4 aw{0.f,0.f,0.f,1.f};return zero_elems<0,0,0,1>()+aw;}
	inline Vec4 xyz0()const{return zero_elems<0,0,0,1>();}
	inline Vec4 xy00()const{return zero_elems<0,0,1,1>();}
	inline Vec4 x0z0()const{return zero_elems<0,1,0,1>();}
	inline Vec4 x000()const{return zero_elems<0,1,1,1>();}
	inline Vec4 _0y00()const{return zero_elems<1,0,1,1>();}
	inline Vec4 _00z0()const{return zero_elems<1,1,0,1>();}
	inline SplattedFloat distSquared(const Vec4& b)const {return (b-*this).lengthSquared();}
	inline SplattedFloat dist(const Vec4& b)const {return SplattedFloat(_mm_sqrt_ss(distSquared(b)));}
	inline SplattedFloat lengthSquared()const {return this->dot(*this);}
	inline SplattedFloat length()const {return SplattedFloat(_mm_sqrt_ss(this->dot(*this)));}
	inline Vec4 normalize() const{
		SplattedFloat len2 = this->length();
		float invlen=1.0f/len2;
		Vec4 ret=*this * invlen;
		return ret;
		//return *this * (Vec4(this->length()).rcp());
	}
	inline Vec4 perp(Vec4 axis) const{
		return this->msub(axis, axis.dot(*this));
	}
	inline Vec4 cross(Vec4 b)const{
		return yzxw()*b.zxyw() - zxyw()*b.yzxw();
	}
	Vec4 floor()const{
		return Vec4(_mm_floor_ps(*this));
	}
	Vec4 ceil()const{
		return Vec4(_mm_ceil_ps(*this));
	}
	HeadingPitch heading_pitch_zup()const{return HeadingPitch{atan2(x,y), asin(z)};}
	HeadingPitch heading_pitch_yup()const{return HeadingPitch{atan2(x,z), asin(y)};}
	TVec2<float> xy()const {return TVec2<float>(x,y);}
	TVec2<float> xz()const {return TVec2<float>(x,z);}
	TVec2<float> yz()const {return TVec2<float>(y,z);}

	Vec4 rcp()const{return Vec4(_mm_rcp_ps(*this));}
	Vec4 rsqrt()const{return Vec4(_mm_rsqrt_ps(*this));}
	Vec4 sqrt()const{return Vec4(_mm_sqrt_ps(*this));}
	Vec4 operator-() const{
		auto mthis=*this;
		auto zero=_mm_sub_ps(mthis,mthis);
		return Vec4(_mm_sub_ps(zero,mthis));
	}
	TVec3<float> xyz()const{return Vec3(x,y,z);}
	
	inline SplattedFloat dot(const Vec4& b)const {
		__m128 dp = _mm_dp_ps(*this, b, 0xff);
		return SplattedFloat(dp);
		//auto tmp=*this * b;
		//return SplattedFloat(x*b.x+y*b.y+z*b.z+w*b.w);
		//return SplattedFloat(_mm_dp_ps(*this,b,0xff));
	}
	inline Vec4 lerp(const Vec4& b,SplattedFloat f)const{
		auto a=_mm_load_ps(&x);
		return Vec4(mm_mul_add(_mm_sub_ps(_mm_load_ps(&b.x),a), f, a));
	}
	// multiply-accumulate , same as 'fmadd', 1st arg is the accumulator.
	inline Vec4 macc(const Vec4& b,SplattedFloat f)const{
		return Vec4(mm_mul_add(_mm_load_ps(&b.x), f, _mm_load_ps(&this->x)));
	}
	inline Vec4 msub(const Vec4& b,SplattedFloat f)const{
		return Vec4(mm_mul_sub(_mm_load_ps(&b.x), f, _mm_load_ps(&this->x)));
	}
	void dump()const {printf("{%.5f,%.5f,%.5f,%.5f}",x,y,z,w);}
};

#endif


// classic 2d brownian noise
inline float frands(float amp){ return amp*((float)(((rand()&0xffff)-0x8000)) * (1.0f/(float)0x8000));}
inline float gaussRand(float a){ return (frands(1.0f)+frands(1.0f))*0.5f * a; }



template<typename T,typename F>
inline T lerp(const T& a,const T& b,F f){return (b-a)*f+a;};

inline Vec4 lerp(const Vec4& a,const Vec4& b,float f){return a.lerp(b,f);};

template<typename T>
inline auto invlerp(const T& a,const T& lo,const T& hi){return (a-lo)/(hi-lo);};

template<typename T>
T bilerp(const T& a, const T& b, const T& c, const T& d, float u,float v){
	return lerp( lerp(a,b,u), lerp(c,d,u),v);
};

template<typename T,typename F>
T bilerp(const T& a, const T& b, const T& c, const T& d, const TVec2<F>& factors){
	return lerp( lerp(a,b,factors.x), lerp(c,d,factors.x),factors.y);
};


inline Vec4 Vec4RandomXYZ(float amp){
	return Vec4(gaussRand(amp),gaussRand(amp),gaussRand(amp),0.0f);
}
inline Vec2 Vec2Random(float amp){
	return Vec2(gaussRand(amp),gaussRand(amp));
}


template<typename T>
const T& clamp_between(const T& v, const T& lo, const T& hi){return v<lo?lo:v>hi?hi:v;}

template<typename T>
inline const T deadzone(T val, T deadzone, T max) {
	// at val==max, return max
	// at val==+/- deadzone , return 0
	//at |val|<deadzone, return 0
	auto scale = max/(max-deadzone);
	if (val<-deadzone){ return (val+deadzone)*scale;}
	else if (val>deadzone){return (val-deadzone)*scale;}
	else return 0.f;
}

template<typename T>
void updatemax(T& dst, const T& cmp){
	if (cmp>dst) dst=cmp;
}

template<typename T> auto clamp01(const T&a ){return clamp_between(a,0.0f,1.0f);}
template<typename T> auto clamps(const T&a,const T& r){return clamp_between(a,-r,r);}
template<typename T> auto sqr(const T& a){return a*a;}

// packed color - 32bits- knows how to convert to vector types.
struct RGBA8{
	uint32_t data;
	operator uint32_t()const{return data;};
	RGBA8(uint32_t src){data=src;}
	RGBA8(const TVec4<float>& src){
		int r=clamp_between((int)(src.x*255.f),0,255);
		int g=clamp_between((int)(src.y*255.f),0,255);
		int b=clamp_between((int)(src.z*255.f),0,255);
		int a=clamp_between((int)(src.w*255.f),0,255);
		data=r|(g<<8)|(b<<16)|(a<<24);
	}

	operator TVec4<uint8_t>()const{
		auto v=TVec4<uint8_t>(data&0xff,(data>>8)&0xff,(data>>16)&0xff,(data>>24)&0xff);
		return v;
	}
	operator TVec4<float>()const{
		float s=(1.f/255.f);
		return TVec4<float>(s*(float)(data&0xff),s*(float)((data>>8)&0xff),s*(float)((data>>16)&0xff),s*(float)((data>>24)&0xff));
	}

};

// extents AKA bounding box. 
template<typename V=TVec4<float> >
struct Extents{
	V min,max;
	// expand the extends to include this point
	void init(){
		min=V(FLT_MAX);
		max=V(-FLT_MAX);
		} // max per generic val
	V centre()const{return (min+max)*0.5f;}
	V size()const {return max-min;}
	V halfSize()const {return (max-min)*0.5f;}
	
	void include(const V& pt){min=min.min(pt); max=max.max(pt);}
	void include(const V& pt,float r){Vec4 ofs(r,r,r,0.f);min=min.min(pt-ofs); max=max.max(pt+ofs);}
	bool contains(const V& pt){return pt.allGE(min) && pt.allLT(max);}
	bool overlaps(const Extents& other)const{
		// any axial plane seperates? then no
		return  !(max.anyLT(other.min) || min.anyGT(other.max));
	}
	void dump(){printf("{{%.3f,%.3f,%.3f},{%.3f,%.3f,%.3f\n}}", this->min.x,this->min.y,this->min.z, this->max.x,this->max.y,this->max.z);}
};
typedef Extents<Vec4> Extents4f;
struct Sphere{Vec4 centre;float radius;
	void includePoint(Vec4 pt){
		if (radius<0.0000f){ // initialize with first.. negative radius means not initialized.
			centre=pt;
			radius=0.f;
		}
		float len=centre.dist(pt);
		if (radius<0.00001f){ // 1st point - radius=0.0
			centre=(centre+pt)*0.5f;
			radius=len*0.5f;
		} else
		if (len > radius){
			// [----X----]
			// [-----X------]
			// factor increase in radius is:
			radius=(len+radius)*0.5f;
			float alpha = (len-radius)/len;
			centre+=(pt-centre)*alpha;
		}
	}
};

struct Quaternion {
	Vec4 v;
	explicit Quaternion(const Vec4& xyzw):v(xyzw){}
	float dot(const Quaternion& b)const {return v.dot(b.v);}
	Quaternion lerp(const Quaternion& b,float f){return Quaternion(v.lerp(b.v,f));}
	Quaternion normalize()const {return Quaternion(v.normalize());}
	Quaternion(float x,float y,float z):v(x,y,z,sqrt(1.0f-x*x-y*y-z*z)){};

	// TODO slerp..
};
// todo is it better to embed
// very like Vec4<Vec4<T>> BUT Matrix4<T> would be a row, Vec<T> a column.
// eventually make TMatix4<V>
template<typename V>
struct TMatrix3 {
	V ax,ay,az;
};

typedef TMatrix3<TVec3<float>> Matrix3;

struct Matrix4{	
	Matrix4(const glm::tmat4x4<float>&);	// compatible with glm type.
	operator glm::tmat4x4<float>()const;
	Vec4 ax,ay,az,aw;
	Matrix4(){*this=identity();}
	explicit Matrix4(const decltype(ax.x)& v): Matrix4(Vec4(v)){} // -> default cons spats diag.
	explicit Matrix4(Vec4 v):	// create diagonal matrix (eg for scaling, or 1111 = identity)
		ax(v[0],0.0f,0.0f,0.0f),ay(0.0f,v[1],0.0f,0.0f),az(0.0f,0.0f,v[2],0.0f),aw(0.0f,0.0f,0.0f,v[3])
	{}
	Matrix4 operator*(const Matrix4& b)const{
		return Matrix4{*this * b.ax, *this * b.ay, *this * b.az, *this * b.aw};
	}
	explicit Matrix4(const Quaternion& q) : Matrix4(q,Vec4(0.0f,0.0f,0.0f,1.0f)){}
	Matrix4(const Quaternion& q, const Vec4& translation){
		Vec4 qq = q.v*q.v; 
		float qxy = q.v.x*q.v.y;
		float qxz = q.v.x*q.v.z;
		float qyz = q.v.y*q.v.z;
		float qxw = q.v.x*q.v.w;
		float qyw = q.v.y*q.v.w;
		float qzw = q.v.z*q.v.w;
		// per element multiples.
		// probably a fancy way to do this with cross product swizzles.
		ax=Vec4(1.0f - 2.0f*(qq.y+qq.z), 2.0f*(qxy+qzw), 2.0f*(qxz-qyw), 0.0f);
		ay=Vec4(2.0f*(qxy-qzw), 1.0f- 2.0f*(qq.x+qq.z), 2.0f*(qyz+qxw), 0.0f);
		az=Vec4(2.0f*(qxz+qyw), 2.0f*(qyz-qxw), 1.0f-2.0f*(qq.x+qq.y), 0.0f);
		aw=translation;
	}
	// TODO - quaternion from matrix..
	void store(float dst[16]) const{
		ax.store(&dst[0]);
		ay.store(&dst[4]);
		az.store(&dst[8]);
		aw.store(&dst[12]);
	}

	Matrix4(const Matrix4& src):ax(src.ax),ay(src.ay),az(src.az),aw(src.aw){};
	Matrix4& operator=(const Matrix4& src){ax=src.ax;ay=src.ay;az=src.az;aw=src.aw; return *this;}
	const Vec4& operator[](int i)const{return ((const Vec4*)&ax)[i];}
	Vec4& operator[](int i){return ((Vec4*)&ax)[i];}
	// matrix x vector formulated as macc of components.
	Vec4 operator*(const Vec4& b)const{
		// todo - figure out the best define for x86 vs ARM platforms , we just do linux vs android for the moment.
		// TODO - PS2VU inspired abstraction for "madd.x" would allow hiding the SIMD broadcast.
		#ifndef USE_SSE
		// ensure consitent ordering vs SIMD version , see below
		return Vec4( (this->ax*b.x+this->ay*b.y)+(this->az*b.z+this->aw*b.w));
		#else
		// TODO - can you accelrate this with AVX256 .. stuff xxxxyyyy  yyyyzzzz into 2 reg
		__m128 xyzw=_mm_load_ps(&b.x);
		__m128 xxxx=_mm_shuffle_ps(xyzw,xyzw, SHUFFLE(0,0,0,0));
		__m128 yyyy=_mm_shuffle_ps(xyzw,xyzw, SHUFFLE(1,1,1,1));
		__m128 zzzz=_mm_shuffle_ps(xyzw,xyzw, SHUFFLE(2,2,2,2));
		__m128 wwww=_mm_shuffle_ps(xyzw,xyzw, SHUFFLE(3,3,3,3));
		// matrix multiply in 4 instructions using FMA. However, it's latency of 4 steps
		// non FMA would be 4 mults, 2 adds, 1 add - latency of 3 steps. 
		// which is better?
		//__m128 acc = _mm_mul_ps(ax,xxxx);

		// strangely this times at 6secs for 1bil, vs 7 for the fma version, WTF.
		__m128 acc = _mm_add_ps(
			_mm_add_ps(_mm_mul_ps(ax,xxxx),
				_mm_mul_ps(ay,yyyy)),
			_mm_add_ps(_mm_mul_ps(az,zzzz),
				_mm_mul_ps(aw,wwww)));
			/*
			_mm_fmadd_ps(aw,wwww,
				_mm_fmadd_ps(az,zzzz,
					_mm_fmadd_ps(ay, yyyy,
						_mm_mul_ps(ax,xxxx))));
						*/

		return Vec4(acc);
		#endif
	}
	explicit Matrix4(float m[3][3], float trans[3]) :ax(Vec4(m[0],0.0f)),ay(Vec4(m[1],0.0f)),az(Vec4(m[2],0.0f)), aw(Vec4(trans,1.0f)){}

	explicit Matrix4(float m[4][4]) :ax(Vec4(m[0])),ay(Vec4(m[1])),az(Vec4(m[2])), aw(Vec4(m[3])){}
	explicit Matrix4(float m[16]):
		ax(m[0],m[1],m[2],m[3]),
		ay(m[4],m[5],m[6],m[7]),
		az(m[8],m[9],m[10],m[11]),
		aw(m[12],m[13],m[14],m[15]){}
	Matrix4(const Vec4& a,const Vec4& b,const Vec4& c,const Vec4& d): ax(a),ay(b),az(c),aw(d){}
	static Matrix4 identity() {return Matrix4(Vec4(1.0f));}
	Matrix4 transpose()const{	// TODO - efficient SIMD swizzle
		return Matrix4(
			Vec4(ax.x,ay.x,az.x,aw.x),
			Vec4(ax.y,ay.y,az.y,aw.y),
			Vec4(ax.z,ay.z,az.z,aw.z),
			Vec4(ax.w,ay.w,az.w,aw.w)
		);
	}
	Matrix4 invertRT()const{ // assumption it's orthonormal
		// todo-vectorized

		Matrix4 tmp=Matrix4(
			Vec4(ax.x,ay.x,az.x,0.f),
			Vec4(ax.y,ay.y,az.y,0.f),
			Vec4(ax.z,ay.z,az.z,0.f),
			Vec4(0.f,0.f,0.f,1.f)
		);
		// todo - check translation is combined correctly for SRT.
		Vec4 invTrans=tmp*(-this->aw.xyz0());
		tmp.aw=Vec4(invTrans.xyz(),1.0f);
		
		return tmp;
	}
	Matrix4 transpose_xyz()const{ // inverses the rotation part only.
		return Matrix4(
			Vec4(ax.x,ay.x,az.x,   ax.w),
			Vec4(ax.y,ay.y,az.y,   ay.w),
			Vec4(ax.z,ay.z,az.z,   az.w),
			Vec4(aw.x,aw.y,aw.z,   aw.w)
		);
	}
	Matrix4 invertSRT()const {
		// axes will have length invertex
		// so it's divide by length to normalze, and again
		// i.e divide by len squared.
		float invlx2=1.0f/ax.lengthSquared();
		float invly2=1.0f/ay.lengthSquared();
		float invlz2=1.0f/az.lengthSquared();
		auto tmp=Matrix4(
			ax*invlx2,
			ay*invly2,
			az*invlz2,
			Vec4(0.f,0.f,0.f,1.f)).transpose();
		Vec4 invTrans=tmp*(-this->aw.xyz0());
		tmp.aw=invTrans.xyz1();
		return tmp;
	}
	const float* as_array() const{return &ax.x;}
	// inverse multiply this matrix by vec, assuming this is a rot-trans matrix
	Vec4 invMulRT(const Vec4& b) const{ // assumption its pure rotation-translation
		if (b.w>0.5f) {	// assume w is 1 or 0
			auto ofs=b-aw;
			auto x=ofs.dot(this->ax);
			auto y=ofs.dot(this->ay);
			auto z=ofs.dot(this->az);
			return Vec4(x,y,z,1.f);
		} else {
			return Vec4(b.dot(this->ax),b.dot(this->ay),b.dot(this->az),0.f);
		}
	}
	float maxAxisLength()const {
		return axisLengths().maxElem();
	}
	TVec3<float> axisLengths() const {	// xyz scalings if it's orthogonal
		return TVec3(
			(float)ax.length(),
			(float)ay.length(),
			(float)az.length());
	}
	static Matrix4 scale(float s){
		return Matrix4(Vec4(s,s,s,1.0));
	}
	static Matrix4 rotX(float f){
		float s=sin(f),c=cos(f);
		return Matrix4(
			Vec4::unitX(),
			Vec4(0.0f,c, s, 0.0f),
			Vec4(0.0f,-s, c, 0.0f),
			Vec4::unitW()
		);
	}
	static Matrix4 rotY(float f){
		float s=sin(f),c=cos(f);
		return Matrix4(
			Vec4(c,0.0f,-s,0.0f),
			Vec4::unitY(),
			Vec4(s,0.0f, c, 0.0f),
			Vec4::unitW()
		);
	}
	static Matrix4 rotZ(float f){
		float s=sin(f),c=cos(f);
		return Matrix4{
			Vec4(c,s, 0.0f,0.0f),
			Vec4(-s,c, 0.0f, 0.0f),
			Vec4::unitZ(),
			Vec4::unitW()
		};
	}
	Matrix4 orthonormalize_zyx() const{
		auto newz=az.normalize();
		auto newy=ay.perp(az).normalize();
		auto newx=newy.cross(newz);
		return Matrix4(newx,newy,newz,aw);
	}
	Matrix4 orthonormalize_yzx() const{
		auto newy=ay.normalize();
		auto newz=az.perp(ay).normalize();
		auto newx=newy.cross(newz);
		return Matrix4(newx,newy,newz,aw);
	}
	static Matrix4 translate(const Vec4& tv){
		return Matrix4(Vec4::unitX(),Vec4::unitY(),Vec4::unitZ(),tv.xyz1());
	};
	static Matrix4 z_is_up(){
		return Matrix4{Vec4::unitX(),Vec4::unitZ(),Vec4::unitY(),Vec4::unitW()};
	}

	Matrix4	xyz1() const {return Matrix4(ax,ay,az,Vec4{0.f,0.f,0.f,1.0f});} // copy orientation only

	static Matrix4 projection(float tan_half_fov,float aspect, float znear,float zfar){
		// assume centred. 
		float xmax = znear*tan_half_fov;
		float xmin = -xmax;
		float ymin = xmin /aspect;
		float ymax = xmax / aspect;
		return frustum(xmin,xmax,ymin,ymax, znear,zfar);
	}
	static Matrix4 lookAtAlong(const Vec4& centre, const Vec4& at,const Vec4& up,int alongAxis){
		auto fwd=(at-centre).normalize();
		auto horiz=up.cross(fwd).normalize();
		if (alongAxis==2)	// default 'object' matrix has it's Y up, like the camera. the *world* is Z=up.
			return Matrix4(horiz, up.normalize(),fwd,centre);
		else 	// pass 'along==1' to make Z=up matrices look like world objets
			return Matrix4(horiz, fwd, up.normalize(),centre);
	}
	inline static auto lookAtZFwd(const Vec4& centre, const Vec4& at,const Vec4& up){
		return lookAtAlong(centre,at,up,2);
	}
	inline static  auto lookAtYFwd(const Vec4& centre, const Vec4& at,const Vec4& up){
		return lookAtAlong(centre,at,up,1);
	}

	static Matrix4 frustum(float left, float right, float bottom, float top, float fnear, float ffar){
    	auto a=(right+left)/(right-left);
    	auto b=(top+bottom)/(top-bottom);
    	auto c=-(ffar+fnear)/(ffar-fnear);
    	auto d=-(2*ffar*fnear/(ffar-fnear));
		return Matrix4(
			Vec4(2.f*fnear/(right-left),	0.f,					0.0f,		0.f),
			Vec4(0.f,						2.f*fnear/(top-bottom), 0.0f, 		0.f),
			Vec4(-a,						-b,						-c,			1.f),
			Vec4(0.f,						0.f,					d,			0.f))	;
	};
	
	static Matrix4 heading_pitch_roll_zup_yfwd(float heading,float pitch,float roll){
		return rotZ(heading)*rotX(pitch)*rotY(roll);
	}
	// argh. 2 conventions mixing x-y in the world , 'z=up', 'y=forward' with z forward.
	HeadingPitchRoll heading_pitch_roll_zup_yfwd() const{	// converts *into*
		auto hp=ay.heading_pitch_zup();
		float roll = atan2( ax.z, ax.xy().length());
		return HeadingPitchRoll{hp.heading,hp.pitch,roll};
	}
	// this is an INVERSE of the specified description of the camera 'object' , i.e transforms world->cam
	static Matrix4 camera_view_z_up_heading_pitch_roll(const Vec4& pos,float heading,float pitch,float roll){
		return z_is_up()*rotY(-roll) * rotX(-pitch) * rotZ(-heading)* translate(Vec4(-pos.x,-pos.y,-pos.z,1.0f));
	};
	//todo - 'to-str' like rust instead?
	void dump() {
		printf("{\t");ax.dump();printf(",\n\t"); ay.dump(); printf(",\n\t"); az.dump(); printf(",\n\t"); aw.dump(); printf("\n}\n");
	}
};


struct uchar255is1 {	// encoded 8888 is NOT '8bits fraction' . 1.0 = 255 not 256.
	uint8_t val;
	operator float()const{return (float)val * (1.0f/255.0f); }
	explicit uchar255is1(uint8_t src):val(src){}	// avoid autoconversion confusion.
	uchar255is1(float f){ val = (uint8_t) (clamp_between(f,0.0f,1.0f) * 255.0f); }
};

template<typename INT,int SHIFT>
struct FixedPt {
	INT value;
	operator float() const {return 1.0f/(float)(1<<SHIFT);}
	explicit FixedPt(float f){value = INT (f * (float)(1<<SHIFT) );}
	auto operator+(const FixedPt<INT,SHIFT>& b){return FixedPt(value+b.value);}
	auto operator-(const FixedPt<INT,SHIFT>& b){return FixedPt(value-b.value);}
	// todo - could flesh out all the arithmetic but ultimately this is a STORAGE format.
	// convert to & from FP for real work.
	auto operator*(const FixedPt<INT,SHIFT>& b){return FixedPt((value*b.value))>>SHIFT;}
};

typedef FixedPt<int16_t,12> int16pt12;	// named to be discoverable from the  storage type
typedef FixedPt<int16_t,15> int16pt15;	// e.g. "its like an i16 but its got 15bits fraction"
typedef FixedPt<int16_t,8> int16pt8;
typedef FixedPt<int8_t,7> int8pt7;
typedef FixedPt<int8_t,4> iint8pt4;
typedef FixedPt<uint8_t,7> uint8pt7;

bool cmpupr(const char* a, const char* b);
const char* getFilenameExt(const char* fname);

#define GLCHECK\
	{auto x=glGetError(); \
		if (x!=GL_NO_ERROR){\
			LOGE("gl error %d",x ); \
		} \
	}

// TODO: split to 'camera.h'
// how to resolve shadowmap cascades ..?

struct Frustum{float tan_half_fov; float aspect; float znear;float zfar;}; // centred frustrum, no shear. we may need windows to the sides.
enum CullResult {INVIS, PART_VIS,ALL_VIS};
const int SHADOWMAP_CASCADES=2;

struct CamView {	// camera combined with viewport. orientation & frustum.
	// setup with Z IS FORWARD as per usual camera logic. 
	// doesn't matter which way is 'up' in the world, if orientation is defined as a matrix or quat.
	Vec2i m_viewport_size;
	Frustum frustum;
	Matrix4 to_world; // to_world * from_world == IDENTITY
	Matrix4 from_world;	// == aka 'view' matrix in GL parlance.
	Matrix4 proj_only;		// 3d cam relative points -> screen
	Matrix4	proj_view;	// inverse-proj - how?
	Matrix4 worldToShadowBuffer[SHADOWMAP_CASCADES];
	Vec4 	m_vel; // needed for some motion blur stuff.. (TODO - really need complete old matrix)
	inline Vec4 pos()const{return to_world.aw;}
	inline Vec4 vel()const {return m_vel;}
	Vec4 fwd()const {return to_world.az;}
	Vec4 up()const {return to_world.ay;}
	Vec4 horiz()const {return to_world.ax;}
	Vec2	min,max;	// TOOD 2d rectangle within the frustum (future - portals)
	Vec4	cull_planes[4];	// left,right,up,down - +ve values inside, -ve outside. like z=+ve inside.
	float cull_values[4];
	Sphere	sphere;		// encloses the frustum.
	bool is_shadow=false;
	bool is_orthographic=false;
	//float cxx, cxz;
	//float cyy, cyz;

	void frustum_vertices(Vec4 pos[8],float z0rel=0.0f, float z1rel=1.0f)const{
		auto& f=frustum;
		float znear = lerp(f.znear,f.zfar, z0rel);
		float zfar = lerp(f.znear,f.zfar, z1rel);
		for (int i=0; i<8; i++){
			float z=i&4?znear:zfar;
			float x=z*f.tan_half_fov;
			float y=z*f.tan_half_fov/f.aspect;
			pos[i] = to_world*Vec4(i&1?x:-x,i&2?y:-y,z,1.f);
		};
	}
	void set_bounds() {
		//  screen pyramid coords = (+/-thfov, .., 1.0) etc.
		// as such cull planes are 90deg rotations of those vectors
		// as thfov gets smaller, these planes tend more toward +/- x,y axis - correct
		// as thfov these planes tend toward z axis - correct.
		auto& f=this->frustum;
		auto thfov = f.tan_half_fov;
		cull_planes[0] = (to_world * Vec4(1.0f,0.f,thfov,0.f)).normalize();
		cull_planes[1] = (to_world * Vec4(-1.0f,0.f,thfov,0.f)).normalize();
		cull_planes[2] = (to_world * Vec4(0.0f,-1.f,thfov/f.aspect,0.f)).normalize();
		cull_planes[3] = (to_world * Vec4(0.0f,1.f,thfov/f.aspect,0.f)).normalize();
		// TODO based on tanhalf fov. sort ou thte maths for finite/infinite frustums identically using homogeneous magic
		cull_values[0]=0.0f;
		cull_values[1]=0.0f;
		cull_values[2]=0.0f;
		cull_values[3]=0.0f;
		Vec4 corners[8];
		frustum_vertices(corners);
		sphere.centre = pos()+fwd()*(frustum.znear+frustum.zfar)*0.5f;
		float maxr=0.f;
		for (int i=0; i<8; i++){
			sphere.radius=std::max(sphere.radius,(float)corners[i].dist(sphere.centre));
		}
	}
	void set_orthographic(Vec4 fwd, Vec4 up, const Vec4* pointsToEnclose, 
					int numPoints,float expand_point_radius);
	void set_look_at(const Vec4& pos,const Vec4 interest_point, Vec4 up_vector, const Frustum& f){
		frustum = f;
		this->is_orthographic=false;
		to_world = Matrix4::lookAtZFwd(pos,interest_point,up_vector);
		from_world = to_world.invertRT();
		auto thfov = f.tan_half_fov;
		proj_only= 
			Matrix4::projection(thfov,f.aspect,f.znear,f.zfar)*
			Matrix4(Vec4(1.0,0.f,0.f,0.f),Vec4(0.f,1.f,0.f,0.f),Vec4(0.f,0.f,1.f,0.f),Vec4(0.f,0.f,0.f,1.f));
		
		proj_view=proj_only*from_world;
		set_bounds();
	}
	//
	CullResult	cull_test_sphere(const Matrix4& objMat, Sphere& lcSphere)const{
		return cull_test_sphere_at(objMat * sphere.centre, sphere.radius);
	}
	CullResult	cull_test_sphere_at(const Vec4& pt, float radius,float distFactor=1.f)const{
		// todo - use bounding box of the frustrum aswell!
		// .. or record the slab clip values for the horiz/vert planes like we use with z..
		// TODO- is it more efficient to transform x/y/z into frustrum space
		// and use 2d logic for horiz/vert tests (it's certainly less data to store)
		//if (is_shadow){return CullResult::PART_VIS;}	//todo: shadow frustum cull is differnt.
		auto ofs=pt - this->pos();
		auto tz = ofs.dot(this->fwd());	// todo multi cull planes in matrix?
			
		float centre_dist2=pt.distSquared(this->sphere.centre);
		if (centre_dist2 > sqr(radius + this->sphere.radius)) {
			return CullResult::INVIS;
		}

		if (((tz+radius) < (this->frustum.znear)) ||
			((tz-radius) > (this->frustum.zfar*distFactor)))
			return CullResult::INVIS;
		

		auto tx0 = ofs.dot(this->cull_planes[0])+this->cull_values[0];	// todo multi cull planes in matrix?
		auto tx1 = ofs.dot(this->cull_planes[1])+this->cull_values[1];	// todo multi cull planes in matrix?
		if (tx0+radius < 0.f) return CullResult::INVIS;
		if (tx1+radius < 0.f) return CullResult::INVIS;


		auto ty0 = ofs.dot(this->cull_planes[2])+this->cull_values[2];	// todo multi cull planes in matrix?
		auto ty1 = ofs.dot(this->cull_planes[3])+this->cull_values[3];	// todo multi cull planes in matrix?
		if (ty0+radius < 0.f) return CullResult::INVIS;
		if (ty1+radius < 0.f) return CullResult::INVIS;
		if (
			tz+radius <this->frustum.zfar && tz-radius > this->frustum.znear  &&
			tx0 > radius && tx1 > radius && ty0>radius && ty1>radius)
			return CullResult::ALL_VIS;	
		return CullResult::PART_VIS; // todo - all-vis
	}
	Vec4 raster_pos_to_world(Vec2i pos,float distz)const{
		// eg to raytrace into the distance, trace from camera origin to fn(screenx,screeny,very_large_z)
		float thfov = this->frustum.tan_half_fov;
		float aspect=this->frustum.aspect;

		float cx = (2.0f*((float)pos.x)/(float)(m_viewport_size.x) - 1.0f)*thfov;
		float cy = (2.0f*((float)pos.y)/(float)(m_viewport_size.y) - 1.0f)*thfov/aspect;
		float cz = 0.f;
		// unhappy with that negation :()
		auto ofs_pt=Vec4(-cx*distz,-cy*distz,distz,1.0f); // seems wrong :( should be +distz why is Z inverted?
		//return to_world * ofs_pt;
		return to_world.ax*ofs_pt.x  + to_world.ay*ofs_pt.y  + to_world.az*ofs_pt.z  + to_world.aw;//*ofs_pt.w  +
	}
	//		g_DebugDraw.line3d(left_gun, aim_point,0xff00ff00).line3d(right_gun,aim_point,0xff00ff00);
//		float thfov = tan(fov/2.0f);
//		float aspect=g_aspect;
//		float cx = (2.0f*(g_CursorPos.x)/(float)(g_winsize[0])-1.0f)*thfov*2.0f;
//		float cy = (2.0f*(g_CursorPos.y)/(float)(g_winsize[1]) - 1.0f)*thfov*2.0f/aspect;
//		float cz = 0.f;
//		float dist=1000.f;
//		auto cam_ofs_pt=Vec4(cx*dist,-cy*dist,-dist,1.0);

//		g_DebugDraw.line3d(cam_obj*Vec4(-thfov*2.0,-thfov/aspect*2.0,-1.0,1.0), cam_obj*cam_ofs_pt,0xff00ff00);
//		g_DebugDraw.line3d(cam_obj*Vec4(thfov*2.0,-thfov/aspect*2.0,-1.0,1.0), cam_obj*cam_ofs_pt,0xff00ff00);

};
#define STORE3(D,X,Y,Z) {D[0]=X;D[1]=Y;D[2]=Z;}
#define UPDATE_MINMAX(MIN,MAX,V) {MIN=std::min(MIN,V); MAX=std::max(MAX,V);}
uint8_t* loadAndAllocResourceFile(const char* f, size_t* sizeOut);
void midpoint_displacement_fractal(float* dst, int size, int size_supress, float s_amp, int step, float amp,float dim, int seed);
inline float* addr2d(float* corner,int size, int x,int y){ int mask=size-1; return corner+(x&mask)+(y&mask)*size;}


struct	TestVertex 
{	// TODO: 8888 color, packed normal, 16bit texture-coords.
	// option for just repeating the second texture-coord (uv0=uv1 and it's all about blend)
	array<float,3>	pos;
	array<uint8_t,4>	color;
	array<Half,3>	norm;
	array<array<Half,2>,2>	texLayers;
};
struct VertexC {
	Vec3	pos;
	TVec4<uint8_t>	color;
};
struct VertexCT {
	Vec3 pos;
	TVec4<uint8_t> color;
	Vec2 tex;
};
struct VertexCT2 {
	Vec3 pos;
	TVec4<uint8_t> color;
	Vec2 tex0;
	Vec2 tex1;
};

struct HemisphereLight{Vec4 ambient; Vec4 ambient_dz;};
struct DirLight{Vec4 dir;Vec4 color;};
struct PosNorm {Vec4 pos;Vec4 norm;};
struct PointLight {Vec4 pos;float radius; Vec4 color;};//too compact

enum ShadingMode {
	VertexAlpha,Multiply2x,PsOverlay,TextureAlpha,LandscapeShader, WaterShader,CarbodyShader,Metallic,Foilage
};


